<?php

function zest_migration_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'content' => array(
        'title' => t('Content'),
      ),
      'taxonomy' => array(
        'title' => t('Taxonomy'),
      ),
      'entity' => array(
        'title' => t('Entity'),
      ),
    ),
    'migrations' => array(
      'ZestTermWhare' => array (
        'class_name' =>'ZestTermWhareMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermWharekai' => array (
        'class_name' =>'ZestTermWharekaiMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermWhareKarakia' => array (
        'class_name' =>'ZestTermWhareKarakiaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermHapu' => array (
        'class_name' =>'ZestTermHapuMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermWaka' => array (
        'class_name' =>'ZestTermWakaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermMaunga' => array (
        'class_name' =>'ZestTermMaungaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermPuke' => array (
        'class_name' =>'ZestTermPukeMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermAwa' => array (
        'class_name' =>'ZestTermAwaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermRoto' => array (
        'class_name' =>'ZestTermRotoMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermMoana' => array (
        'class_name' =>'ZestTermMoanaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermKeyTupunaDepicted' => array (
        'class_name' =>'ZestTermKeyTupunaDepictedMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermTaonga' => array (
        'class_name' =>'ZestTermTaongaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermUrupa' => array (
        'class_name' =>'ZestTermUrupaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermWhakapapa' => array (
        'class_name' =>'ZestTermWhakapapaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermPapakainga' => array (
        'class_name' =>'ZestTermPapakaingaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermAncestralPapakainga' => array (
        'class_name' =>'ZestTermAncestralPapakaingaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermNgahere' => array (
        'class_name' =>'ZestTermNgahereMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermHuiTikangaKawa' => array (
        'class_name' =>'ZestTermHuiTikangaKawaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermMaraeCondition' => array (
        'class_name' =>'ZestTermMaraeConditionMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermTangiTikangaKawa' => array (
        'class_name' =>'ZestTermTangiTikangaKawaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermCommunityTrust' => array (
        'class_name' =>'ZestTermCommunityTrustMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermBattalion' => array (
        'class_name' =>'ZestTermBattalionMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermRohe' => array (
        'class_name' =>'ZestTermRoheMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermRegion' => array (
        'class_name' =>'ZestTermRegionMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestTermIwiRunanga' => array (
        'class_name' =>'ZestTermIwiRunangaMigration',
        'group_name' => 'taxonomy',
      ),
      'ZestNodetypeMarae' => array (
        'class_name' =>'ZestNodetypeMaraeMigration',
        'group_name' => 'content',
      ),
      'ZestNodetypeTest1' => array (
        'class_name' =>'ZestNodetypeTest1Migration',
        'group_name' => 'content',
      ),
      'ZestEntitytypeDescription' => array (
        'class_name' =>'ZestEntitytypeDescriptionMigration',
        'group_name' => 'entity',
      ),
      'ZestEntitytypeMarae' => array (
        'class_name' =>'ZestEntitytypeMaraeMigration',
        'group_name' => 'entity',
      ),
    ),
  );
  return $api;
}
