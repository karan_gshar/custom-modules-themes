<?php

define('SOURCE_DIR_ENTITY', 'public://taonga_images');
//define('SOURCE_DIR_ENTITY', '/srv/www/maorimaps.dev.zestmedia.co.nz/httpdocs/sites/default/files/taonga_images');
define('DEST_DIR_ENTITY', 'public://taonga_images');

class ZestEntitytypeDescriptionMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrates nodetype-Marae');

    //These fields will be migrated later

    $source_fields = array(
      'description_language' => t('Language'),
    );

    $query = db_select('maorimaps.node', 'n')
      ->fields('n', array(
        'nid',
        'language',
        'title',
        'uid',
        'status',
        'created',
        'changed',
        'comment',
        'promote',
        'sticky',
        'tnid',
        'translate'
      ))
      ->fields('body', array('body_value'))
      ->condition('n.type', 'marae', '=');

    $query->leftjoin('maorimaps.field_data_body', 'body', 'n.nid = body.entity_id');

    $query->orderBy('n.title');

    $this->source = new MigrateSourceSQL($query, $source_fields);

    $this->destination = new MigrateDestinationEntityAPI(
      'poi_description',  // Entity type
      'poi_description'   // bundle
    );

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          //'description' => 'D7 Unique Node ID',
          'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Make the mappings
    $this->addFieldMapping('id')->issueGroup(t('DNM'));
    $this->addFieldMapping('type')->defaultValue('poi_description');
    $this->addFieldMapping('field_poi_description_language', 'description_language');
    $this->addFieldMapping('field_poi_description_body', 'body_value');
    $this->addFieldMapping('field_poi_description_source_id', 'nid');
    if (module_exists('path')) {
      $this->addFieldMapping('path')
        ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
          ->issueGroup(t('DNM'));

      }
    }
  }

  public function prepareRow($row) {

    $result = db_select('maorimaps.node', 'n')
      ->fields('n', array('language'))
      ->condition('nid', $row->nid)
      ->execute()
      ->fetchField();

    Switch ($result) {

      case 'en' :
        $lang = 'English';
        break;

      case 'mi':
        $lang = 'Te Reo';
        break;

      default:
        $lang = 'English';
    }

    $row->description_language = $lang;

    return TRUE;
  }

}


// Marae

class ZestEntitytypeMaraeMigration extends Migration {

  /**
   * @param array $arguments
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrates nodetype-Marae');

    $source_fields = array(
      'whare' => t('Whare'),
      'wharekai' => t('Wharekai'),
      'whare_karakia' => t('Whare Karakia'),
      'hapu' => t('Hapu'),
      'waka' => t('Waka'),
      'maunga' => t('Maunga'),
      'puke' => t('Puke'),
      'awa' => t('Awa'),
      'roto' => t('Roto'),
      'moana' => t('Moana'),
      'key_tupuna_depicted' => t('Key Tupuna Depicted'),
      'taonga' => t('Taonga'),
      'urupa' => t('Urupa'),
      'whakapapa' => t('Whakapapa'),
      'papakainga' => t('Papakainga'),
      'ancestral_papakainga' => t('Ancestral Papakainga'),
      'ngahere' => t('Ngahere'),
      'hui_tikanga_kawa' => t('Hui Tikanga Kawa'),
      'marae_condition' => t('Marae Condition'),
      'tangi_tikanga_kawa' => t('Tangi Tikanga Kawa'),
      'community_trust' => t('Community Trust'),
      'battalion' => t('Battalion'),
      'rohe' => t('Rohe'),
      'region' => t('Region'),
      'runanga' => t('Runanga'),
      'taonga_images' => t('Taonga Images'),
    );

    //To merge both English and Te Roe versions under one node.

    $result = db_query('SELECT nid, COUNT(title) c FROM {maorimaps.node} GROUP BY title HAVING c > 1')->fetchAll();
    $node_arr = array();
    foreach ($result as $value) {
      $node_arr[] = $value->nid;
    }

    $result2 = db_query('SELECT nid, COUNT(title) c FROM {maorimaps.node} GROUP BY title HAVING c = 1')->fetchAll();
    foreach ($result2 as $value) {
      $node_arr[] = $value->nid;
    }

    $query = db_select('maorimaps.node', 'n')
      ->fields('n', array(
        'nid',
        'language',
        'title',
        'uid',
        'status',
        'created',
        'changed',
        'comment',
        'promote',
        'sticky',
        'tnid',
        'translate'
      ))
      ->fields('taonga_information', array('field_taonga_information_value'))
      ->fields('uri_home', array('field_uri_home_value'))
      ->fields('uri_away', array('field_uri_away_value'))
      ->fields('harvesting', array('field_customary_harvesting_value'))
      ->fields('anniversary', array('field_key_dates_anniversaries_value'))
      ->fields('matteress', array('field_mattress_info_value'))
      ->fields('settlement', array('field_nearest_settlement_value'))
      ->fields('schools', array('field_local_schools_value'))
      ->fields('frequency', array('field_use_frequency_type_value'))
      ->fields('governance', array('field_governance_value'))
      ->fields('electricity', array('field_electricity_value'))
      ->fields('rios', array('field_rios_value'))
      ->fields('nz_police', array('field_nz_police_value'))
      ->fields('fire_service', array('field_fire_service_value'))
      ->fields('medical', array('field_medical_value'))
      ->fields('insurance', array('field_insurance_value'))
      ->fields('sewerage', array('field_sewerage_value'))
      ->fields('tax', array('field_tax_value'))
      ->fields('rates', array('field_rates_value'))
      //->condition('n.type', 'marae', '=');
      ->condition('n.nid', $node_arr, 'IN');

    $query->leftjoin('maorimaps.field_data_field_taonga_information', 'taonga_information', 'n.nid = taonga_information.entity_id');
    $query->leftjoin('maorimaps.field_data_field_uri_home', 'uri_home', 'n.nid = uri_home.entity_id');
    $query->leftjoin('maorimaps.field_data_field_uri_away', 'uri_away', 'n.nid = uri_away.entity_id');
    $query->leftjoin('maorimaps.field_data_field_customary_harvesting', 'harvesting', 'n.nid = harvesting.entity_id');
    $query->leftjoin('maorimaps.field_data_field_key_dates_anniversaries', 'anniversary', 'n.nid = anniversary.entity_id');
    $query->leftjoin('maorimaps.field_data_field_mattress_info', 'matteress', 'n.nid = matteress.entity_id');
    $query->leftjoin('maorimaps.field_data_field_nearest_settlement', 'settlement', 'n.nid = settlement.entity_id');
    $query->leftjoin('maorimaps.field_data_field_local_schools', 'schools', 'n.nid = schools.entity_id');
    $query->leftjoin('maorimaps.field_data_field_use_frequency_type', 'frequency', 'n.nid = frequency.entity_id');
    $query->leftjoin('maorimaps.field_data_field_governance', 'governance', 'n.nid = governance.entity_id');
    $query->leftjoin('maorimaps.field_data_field_electricity', 'electricity', 'n.nid = electricity.entity_id');
    $query->leftjoin('maorimaps.field_data_field_rios', 'rios', 'n.nid = rios.entity_id');
    $query->leftjoin('maorimaps.field_data_field_nz_police', 'nz_police', 'n.nid = nz_police.entity_id');
    $query->leftjoin('maorimaps.field_data_field_fire_service', 'fire_service', 'n.nid = fire_service.entity_id');
    $query->leftjoin('maorimaps.field_data_field_medical', 'medical', 'n.nid = medical.entity_id');
    $query->leftjoin('maorimaps.field_data_field_insurance', 'insurance', 'n.nid = insurance.entity_id');
    $query->leftjoin('maorimaps.field_data_field_sewerage', 'sewerage', 'n.nid = sewerage.entity_id');
    $query->leftjoin('maorimaps.field_data_field_tax', 'tax', 'n.nid = tax.entity_id');
    $query->leftjoin('maorimaps.field_data_field_rates', 'rates', 'n.nid = rates.entity_id');

    $query->orderBy('n.title');

    $this->source = new MigrateSourceSQL($query, $source_fields);

    $this->destination = new MigrateDestinationEntityAPI(
      'poi_types',  // Entity type
      'marae'   // bundle
    );

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          //'description' => 'D7 Unique Node ID',
          'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Make the mappings
    $this->addFieldMapping('id')->issueGroup(t('DNM'));
    $this->addFieldMapping('type')->defaultValue('marae');
    $this->addFieldMapping('field_marae_taonga_information', 'field_taonga_information_value');
    $this->addFieldMapping('field_marae_uri_home', 'field_uri_home_value');
    $this->addFieldMapping('field_marae_uri_away', 'field_uri_away_value');
    $this->addFieldMapping('field_marae_customary_harvesting', 'field_customary_harvesting_value');
    $this->addFieldMapping('field_marae_key_dates_anniversar', 'field_key_dates_anniversaries_value');
    $this->addFieldMapping('field_marae_mattress_info', 'field_mattress_info_value');
    $this->addFieldMapping('field_marae_nearest_settlement', 'field_nearest_settlement_value');
    $this->addFieldMapping('field_marae_local_schools', 'field_nearest_settlement_value');
    $this->addFieldMapping('field_marae_use_frequency_type', 'field_use_frequency_type_value');
    $this->addFieldMapping('field_marae_governance', 'field_governance_value');
    $this->addFieldMapping('field_marae_electricity', 'field_electricity_value');
    $this->addFieldMapping('field_marae_rios', 'field_rios_value');
    $this->addFieldMapping('field_marae_nz_police', 'field_nz_police_value');
    $this->addFieldMapping('field_marae_fire_service', 'field_fire_service_value');
    $this->addFieldMapping('field_marae_medical', 'field_medical_value');
    $this->addFieldMapping('field_marae_insurance', 'field_insurance_value');
    $this->addFieldMapping('field_marae_sewerage', 'field_sewerage_value');
    $this->addFieldMapping('field_marae_tax', 'field_tax_value');
    $this->addFieldMapping('field_marae_rates', 'field_rates_value');
    $this->addFieldMapping('field_poi_type_marae_source_id', 'nid');

    //Whare
    $this->addFieldMapping('field_marae_whare', 'whare');
    $this->addFieldMapping('field_marae_whare:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_whare:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_whare:ignore_case')->defaultValue(TRUE);

    //Wharekai
    $this->addFieldMapping('field_marae_wharekai', 'wharekai');
    $this->addFieldMapping('field_marae_wharekai:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_wharekai:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_wharekai:ignore_case')
      ->defaultValue(TRUE);

    //Whare karakai
    $this->addFieldMapping('field_marae_whare_karakia', 'whare_karakia');
    $this->addFieldMapping('field_marae_whare_karakia:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_whare_karakia:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_whare_karakia:ignore_case')
      ->defaultValue(TRUE);

    //Hapu
    $this->addFieldMapping('field_marae_hapu', 'hapu');
    $this->addFieldMapping('field_marae_hapu:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_hapu:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_hapu:ignore_case')->defaultValue(TRUE);

    //Waka
    $this->addFieldMapping('field_marae_waka', 'waka');
    $this->addFieldMapping('field_marae_waka:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_waka:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_waka:ignore_case')->defaultValue(TRUE);

    //Maunga
    $this->addFieldMapping('field_marae_maunga', 'maunga');
    $this->addFieldMapping('field_marae_maunga:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_maunga:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_maunga:ignore_case')
      ->defaultValue(TRUE);

    //Puke
    $this->addFieldMapping('field_marae_puke', 'puke');
    $this->addFieldMapping('field_marae_puke:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_puke:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_puke:ignore_case')->defaultValue(TRUE);

    //Awa
    $this->addFieldMapping('field_marae_awa', 'awa');
    $this->addFieldMapping('field_marae_awa:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_awa:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_awa:ignore_case')->defaultValue(TRUE);

    //Roto
    $this->addFieldMapping('field_marae_roto', 'roto');
    $this->addFieldMapping('field_marae_roto:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_roto:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_roto:ignore_case')->defaultValue(TRUE);

    //Moana
    $this->addFieldMapping('field_marae_moana', 'moana');
    $this->addFieldMapping('field_marae_moana:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_moana:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_moana:ignore_case')->defaultValue(TRUE);

    //Key Tupuna Depicted
    $this->addFieldMapping('field_marae_key_tupuna_depicted', 'key_tupuna_depicted');
    $this->addFieldMapping('field_marae_key_tupuna_depicted:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_key_tupuna_depicted:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_key_tupuna_depicted:ignore_case')
      ->defaultValue(TRUE);

    //Taonga
    $this->addFieldMapping('field_marae_taonga', 'taonga');
    $this->addFieldMapping('field_marae_taonga:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_taonga:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_taonga:ignore_case')
      ->defaultValue(TRUE);

    //Urupa
    $this->addFieldMapping('field_marae_urupa', 'urupa');
    $this->addFieldMapping('field_marae_urupa:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_urupa:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_urupa:ignore_case')->defaultValue(TRUE);

    //Whakapapa
    $this->addFieldMapping('field_marae_whakapapa', 'whakapapa');
    $this->addFieldMapping('field_marae_whakapapa:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_whakapapa:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_whakapapa:ignore_case')
      ->defaultValue(TRUE);

    //Papakainga
    $this->addFieldMapping('field_marae_papakainga', 'papakainga');
    $this->addFieldMapping('field_marae_papakainga:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_papakainga:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_papakainga:ignore_case')
      ->defaultValue(TRUE);

    //Ancestral Papakainga
    $this->addFieldMapping('field_marae_ancestral_papakainga', 'ancestral_papakainga');
    $this->addFieldMapping('field_marae_ancestral_papakainga:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_ancestral_papakainga:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_ancestral_papakainga:ignore_case')
      ->defaultValue(TRUE);

    //Ngahere
    $this->addFieldMapping('field_marae_ngahere', 'ngahere');
    $this->addFieldMapping('field_marae_ngahere:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_ngahere:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_ngahere:ignore_case')
      ->defaultValue(TRUE);

    //Hui Tikanga Kawa
    $this->addFieldMapping('field_marae_hui_tikanga_kawa', 'hui_tikanga_kawa');
    $this->addFieldMapping('field_marae_hui_tikanga_kawa:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_hui_tikanga_kawa:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_hui_tikanga_kawa:ignore_case')
      ->defaultValue(TRUE);

    //marae_condition
    $this->addFieldMapping('field_marae_marae_condition', 'marae_condition');
    $this->addFieldMapping('field_marae_marae_condition:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_marae_condition:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_marae_condition:ignore_case')
      ->defaultValue(TRUE);

    //Tangi Tikanga Kawa
    $this->addFieldMapping('field_marae_tangi_tikanga_kawa', 'tangi_tikanga_kawa');
    $this->addFieldMapping('field_marae_tangi_tikanga_kawa:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_tangi_tikanga_kawa:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_tangi_tikanga_kawa:ignore_case')
      ->defaultValue(TRUE);

    //Community Trust
    $this->addFieldMapping('field_marae_community_trust', 'community_trust');
    $this->addFieldMapping('field_marae_community_trust:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_community_trust:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_community_trust:ignore_case')
      ->defaultValue(TRUE);

    //Battalion
    $this->addFieldMapping('field_marae_battalion', 'battalion');
    $this->addFieldMapping('field_marae_battalion:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_battalion:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_battalion:ignore_case')
      ->defaultValue(TRUE);

    //Rohe
    $this->addFieldMapping('field_marae_rohe', 'rohe');
    $this->addFieldMapping('field_marae_rohe:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_rohe:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_rohe:ignore_case')->defaultValue(TRUE);

    //Region
    $this->addFieldMapping('field_marae_region', 'region');
    $this->addFieldMapping('field_marae_region:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_region:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_region:ignore_case')
      ->defaultValue(TRUE);

    //Runanga
    $this->addFieldMapping('field_marae_runanga', 'runanga');
    $this->addFieldMapping('field_marae_runanga:source_type')
      ->defaultValue(array('tname'));
    $this->addFieldMapping('field_marae_runanga:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_runanga:ignore_case')
      ->defaultValue(TRUE);

    //Mapping of Image Fields
    $this->addFieldMapping('field_marae_taonga_images', 'taonga_images');
    $this->addFieldMapping('field_marae_taonga_images:file_class')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_marae_taonga_images:preserve_files')
      ->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_marae_taonga_images:destination_dir')
      ->defaultValue(DEST_DIR_ENTITY);
    $this->addFieldMapping('field_marae_taonga_images:file_replace')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_marae_taonga_images:destination_file')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_marae_taonga_images:urlencode')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_marae_taonga_images:alt')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_marae_taonga_images:title')
      ->issueGroup(t('DNM'));;
    $this->addFieldMapping('field_marae_taonga_images:source_dir')
      ->defaultValue(SOURCE_DIR_ENTITY);


    if (module_exists('path')) {
      $this->addFieldMapping('path')
        ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
          ->issueGroup(t('DNM'));

      }
    }
  }

  public function prepareRow($row) {

    $maraes = db_select('maorimaps.node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.title', '%' . $row->title . '%', 'LIKE')
      ->execute()
      ->fetchAll();


    $term_arr1 = $term_arr2 = $term_arr3 = $term_arr4 = $term_arr5 = $term_arr6 = $term_arr7 = $term_arr8 = $term_arr9 = $term_arr10 = $term_arr11 = array();
    $term_arr12 = $term_arr13 = $term_arr14 = $term_arr15 = $term_arr16 = $term_arr17 = $term_arr18 = $term_arr19 = $term_arr20 = $term_arr21 = array();
    $term_arr22 = $term_arr23 = $term_arr24 = $term_arr25 = $image_arr = array();

    foreach ($maraes as $marae) {
      //Whare
      $query1 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '6');

      $query1->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query1
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr1)) {
          $term_arr1[] = $value->name;
        }
      }

      //Wharekai
      $query1 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '7');

      $query1->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query1
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr2)) {
          $term_arr2[] = $value->name;
        }
      }

      //Whare Karakia
      $query2 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '8');

      $query2->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query2
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr3)) {
          $term_arr3[] = $value->name;
        }
      }


      //Hapu
      $query3 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '9');

      $query3->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query3
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr4)) {
          $term_arr4[] = $value->name;
        }
      }

      //Waka
      $query4 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '10');

      $query4->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query4
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr5)) {
          $term_arr5[] = $value->name;
        }
      }

      //Maunga
      $query5 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '11');

      $query5->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query5
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr6)) {
          $term_arr6[] = $value->name;
        }
      }


      //Puke
      $query6 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '12');

      $query6->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query6
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr7)) {
          $term_arr7[] = $value->name;
        }
      }


      //Awa
      $query7 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '13');

      $query7->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query7
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr8)) {
          $term_arr8[] = $value->name;
        }
      }


      //Roto
      $query8 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '14');

      $query8->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query8
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr9)) {
          $term_arr9[] = $value->name;
        }
      }


      //Moana
      $query9 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '16');

      $query9->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query9
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr10)) {
          $term_arr10[] = $value->name;
        }
      }


      //Key Tupuna Depicted
      $query10 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '17');

      $query10->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query10
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr11)) {
          $term_arr11[] = $value->name;
        }
      }


      //Taonga
      $query11 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '18');

      $query11->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query11
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr12)) {
          $term_arr12[] = $value->name;
        }
      }


      //Urupa
      $query12 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '19');

      $query12->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query12
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr13)) {
          $term_arr13[] = $value->name;
        }
      }


      //Whakapapa
      $query13 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '20');

      $query13->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query13
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr14)) {
          $term_arr14[] = $value->name;
        }
      }


      //Papakainga
      $query14 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '21');

      $query14->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query14
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr15)) {
          $term_arr15[] = $value->name;
        }
      }


      //Ancestral Papakainga
      $query15 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '22');

      $query15->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query15
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr16)) {
          $term_arr16[] = $value->name;
        }
      }


      //Ngahere
      $query16 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '23');

      $query16->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query16
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr17)) {
          $term_arr17[] = $value->name;
        }
      }


      //Hui Tikanga Kawa
      $query17 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '24');

      $query17->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query17
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr18)) {
          $term_arr18[] = $value->name;
        }
      }


      //Marae Condition
      $query18 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '25');

      $query18->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query18
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr19)) {
          $term_arr19[] = $value->name;
        }
      }


      //Tangi Tikanga Kawa
      $query19 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '27');

      $query19->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query19
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr20)) {
          $term_arr20[] = $value->name;
        }
      }

      //Community Trust
      $query20 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '4');

      $query20->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query20
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr21)) {
          $term_arr21[] = $value->name;
        }
      }

      //Battalion
      $query21 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '3');

      $query21->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query21
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr22)) {
          $term_arr22[] = $value->name;
        }
      }

      //Rohe
      $query22 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '1');

      $query22->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query22
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr23)) {
          $term_arr23[] = $value->name;
        }
      }


      //Region
      $query23 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '2');

      $query23->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query23
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr24)) {
          $term_arr24[] = $value->name;
        }
      }

      //Runanga
      $query24 = db_select('maorimaps.taxonomy_index', 'ti')
        ->fields('ttd', array('name'))
        ->condition('ti.nid', $marae->nid)
        ->condition('ttd.vid', '5');

      $query24->leftjoin('maorimaps.taxonomy_term_data', 'ttd', 'ti.tid = ttd.tid');
      $result = $query24
        ->execute()
        ->fetchAll();

      foreach ($result as $value) {
        if (!in_array($value->name, $term_arr25)) {
          $term_arr25[] = $value->name;
        }
      }
    }

    //Toanga Images

    $query = db_select('maorimaps.field_data_field_taonga_images', 'ti')
      ->fields('ti', array('field_taonga_images_fid'))
      ->fields('fm', array('filename'))
      ->condition('ti.entity_id', $row->nid)
      ->condition('ti.bundle', '%marae%', 'LIKE');

    $query->leftjoin('maorimaps.file_managed', 'fm', 'ti.field_taonga_images_fid = fm.fid');

    $result = $query
      ->execute()
      ->fetchAll();

    foreach ($result as $value) {
      if (!in_array($value->name, $image_arr)) {
        $image_arr[] = $value->filename;
      }
    }

    $row->whare = $term_arr1;
    $row->wharekai = $term_arr2;
    $row->whare_karakia = $term_arr3;
    $row->hapu = $term_arr4;
    $row->waka = $term_arr5;
    $row->maunga = $term_arr6;
    $row->puke = $term_arr7;
    $row->awa = $term_arr8;
    $row->roto = $term_arr9;
    $row->moana = $term_arr10;
    $row->key_tupuna_depicted = $term_arr11;
    $row->taonga = $term_arr12;
    $row->urupa = $term_arr13;
    $row->whakapapa = $term_arr14;
    $row->papakainga = $term_arr15;
    $row->ancestral_papakainga = $term_arr16;
    $row->ngahere = $term_arr17;
    $row->hui_tikanga_kawa = $term_arr18;
    $row->marae_condition = $term_arr19;
    $row->tangi_tikanga_kawa = $term_arr20;
    $row->community_trust = $term_arr21;
    $row->battalion = $term_arr22;
    $row->rohe = $term_arr23;
    $row->region = $term_arr24;
    $row->runanga = $term_arr25;
    $row->taonga_images = $image_arr;

    return TRUE;
  }

}
