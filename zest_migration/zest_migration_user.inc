<?php

define('STORE_DIR', 'public://pictures');

class ZestUserMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);

    // Remove non-existing nids from comment statistics
    //db_query ("delete from node_comment_statistics where nid not in (select nid from node)");

    $this->description = t('Migrates Staff or User');

    //These fields will be migrated later

    $source_fields = array(
      'profile_picture' => t('Image file path'),
      'user_name' => t('user name'),
      //  'institution_name' => t('name of institution term'),
      //  'people_name' => t('name of people term'),
      //  'tag_name' => t('name of tag term'),
      //'path_alias' => t('node alias'),
      //  'feature_image_title' => t('Feature image title'),
    );


    $query = db_select('users', 'u')
      ->fields('u', array('uid', 'status', 'mail'))
      ->fields('userfullname', array('field_full_name_value'))
      ->fields('userddi', array('field_user_ddi_value'))
      ->fields('usermobile', array('field_user_mobile_value'))
      ->fields('userphysicaladdress', array('field_user_physical_address_value'))
      ->fields('userlocation', array('field_user_location_value'))
      ->fields('userfirstname', array('field_user_first_name_value'))
      ->fields('usersurname', array('field_user_surname_value'))
      ->condition('u.status', '1', '=');

    $query->rightjoin('field_data_field_full_name', 'userfullname', 'u.uid = userfullname.entity_id');
    $query->leftjoin('field_data_field_user_ddi', 'userddi', 'u.uid = userddi.entity_id');
    $query->leftjoin('field_data_field_user_mobile', 'usermobile', 'u.uid = usermobile.entity_id');
    $query->leftjoin('field_data_field_user_physical_address', 'userphysicaladdress', 'u.uid = userphysicaladdress.entity_id');
    $query->leftjoin('field_data_field_user_postal_address', 'userpostaladdress', 'u.uid = userpostaladdress.entity_id');
    $query->leftjoin('field_data_field_user_location', 'userlocation', 'u.uid = userlocation.entity_id');
    $query->leftjoin('field_data_field_user_first_name', 'userfirstname', 'u.uid = userfirstname.entity_id');
    $query->leftjoin('field_data_field_user_surname', 'usersurname', 'u.uid = usersurname.entity_id');


    $this->source = new MigrateSourceSQL($query, $source_fields);

    $this->destination = new MigrateDestinationNode('contact', array('text_format' => 'full_html'));

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'uid' => array(
          'type' => 'int',
          //'unsigned' => TRUE,
          'not null' => TRUE,
          //'description' => 'D6 Unique Node ID',
          //'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Make the mappings
    //$this->addFieldMapping('nid', 'nid');
    $this->addFieldMapping('title', 'user_name');

    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    $this->addFieldMapping('uid')->issueGroup(t('DNM'));
    $this->addFieldMapping('created')->issueGroup(t('DNM'));
    $this->addFieldMapping('changed')->issueGroup(t('DNM'));
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('promote')->defaultValue(0);
    $this->addFieldMapping('sticky')->issueGroup(t('DNM'));
    $this->addFieldMapping('revision')->issueGroup(t('DNM'));
    $this->addFieldMapping('log')->issueGroup(t('DNM'));
    $this->addFieldMapping('language')->issueGroup(t('DNM'));
    $this->addFieldMapping('tnid')->issueGroup(t('DNM'));
    $this->addFieldMapping('translate')->issueGroup(t('DNM'));
    $this->addFieldMapping('revision_uid')->issueGroup(t('DNM'));

    $this->addFieldMapping('field_zest_contact_location', 'field_user_location_value');
    $this->addFieldMapping('field_zest_contact_email', 'mail');

    //Mapping of image fields
    $this->addFieldMapping('field_zest_contact_picture', 'profile_picture');
    $this->addFieldMapping('field_zest_contact_picture:file_class')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_picture:preserve_files')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_zest_contact_picture:source_dir')
      ->defaultValue(STORE_DIR);
    $this->addFieldMapping('field_zest_contact_picture:file_replace')
      ->defaultValue(FILE_EXISTS_REUSE);
    $this->addFieldMapping('field_zest_contact_picture:destination_file')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_picture:urlencode')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_picture:alt')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_picture:title')
      ->issueGroup(t('DNM'));;
    $this->addFieldMapping('field_zest_contact_picture:destination_dir')
      ->defaultValue(STORE_DIR);

    $this->addFieldMapping('field_zest_contact_tags')
      ->defaultValue(179); // Term Id for Staff
    $this->addFieldMapping('field_zest_contact_tags:source_type')
      ->defaultValue('tid');
    $this->addFieldMapping('field_zest_contact_tags:create_term')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_zest_contact_tags:ignore_case')
      ->defaultValue(TRUE);

    $this->addFieldMapping('field_zest_contact_source', 'uid');

    //Physical address field mapping
    $this->addFieldMapping('field_zest_contact_address')->defaultValue('NZ');
    $this->addFieldMapping('field_zest_contact_address:administrative_area')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:sub_administrative_area')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:locality')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:dependent_locality')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:postal_code')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:thoroughfare', 'field_user_physical_address_value');
    $this->addFieldMapping('field_zest_contact_address:premise')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:sub_premise')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:organisation_name')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:name_line')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:first_name')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:last_name')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_address:data')
      ->issueGroup(t('DNM'));

    //Mapping of Member Since field
    $this->addFieldMapping('field_zest_contact_member_since')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_member_since:timezone')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_member_since:rrule')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_zest_contact_member_since:to')
      ->issueGroup(t('DNM'));

    //Mapping of Staff fields
    //$this->addFieldMapping('field_user_job_title', 'field_user_job_title_value');
    $this->addFieldMapping('field_zest_contact_phone', 'field_user_ddi_value');
    $this->addFieldMapping('field_zest_contact_mobile', 'field_user_mobile_value');


    $this->addFieldMapping('comment')->issueGroup(t('DNM'));
    $this->addFieldMapping('path')->issueGroup(t('DNM'));
    $this->addFieldMapping('pathauto')->issueGroup(t('DNM'));

  }

  // massage user data before saving

  public function prepareRow($current_row) {
    $source_id = $current_row->uid;

    $query = db_select('file_usage', 'fu')
      ->fields('fu', array('fid'))
      ->fields('fm', array('filename'))
      ->condition('fu.id', $source_id)
      ->condition('fu.type', '%user%', 'LIKE');

    $query->leftjoin('file_managed', 'fm', 'fm.fid = fu.fid');

    $result = $query
      ->execute()
      ->fetch();

    if (!empty($result)) {
      $current_row->profile_picture = $result->filename;
    }

    else {
      $current_row->profile_picture = 'img-holding.png';  //default-image
    }


    if (!empty($current_row->field_full_name_value)) {
      $current_row->user_name = $current_row->field_full_name_value;
    }
    else {
      $current_row->user_name = $current_row->field_user_first_name_value . ' ' . $current_row->field_user_surname_value;
    }
    return TRUE;
  }
}

