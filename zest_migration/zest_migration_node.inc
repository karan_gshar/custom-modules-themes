<?php

define('SOURCE_DIR_POI', '/srv/www/maorimaps.dev.zestmedia.co.nz/httpdocs/sites/default/files/gallery_images');
define('DEST_DIR_POI', 'public://gallery_images');

class ZestNodetypeMaraeMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrates nodetype-Marae');

    //All the Taxonomies should be migrated first

    $this->dependencies = array(
      'ZestTermWhare',
      'ZestTermWharekai',
      'ZestTermWhareKarakia',
      'ZestTermHapu',
      'ZestTermWaka',
      'ZestTermMaunga',
      'ZestTermPuke',
      'ZestTermAwa',
      'ZestTermRoto',
      'ZestTermMoana',
      'ZestTermKeyTupunaDepicted',
      'ZestTermTaonga',
      'ZestTermUrupa',
      'ZestTermWhakapapa',
      'ZestTermPapakainga',
      'ZestTermAncestralPapakainga',
      'ZestTermNgahere',
      'ZestTermHuiTikangaKawa',
      'ZestTermMaraeCondition',
      'ZestTermTangiTikangaKawa',
      'ZestTermCommunityTrust',
      'ZestTermBattalion',
      'ZestTermRohe',
      'ZestTermRegion',
      'ZestTermIwiRunanga'
    );

    //These fields will be migrated later

    $source_fields = array(
      'poi_title' => t('POI title'),
      'poi_description_id' => t('POI description ID'),
      'contact_entity_id' => t('POI description ID'),
      'coords_lat' => t('Latitude'),
      'coords_lng' => t('Longitude'),
      'gallery_images' => t('Gallery Image'),
      'poi_marae_id' => t('POI Marae ID'),
    );


    //To merge both English and Te Roe versions under one node.

    $result = db_query('SELECT nid, COUNT(title) c FROM {maorimaps.node} GROUP BY title HAVING c > 1')->fetchAll();
    $node_arr = array();
    foreach ($result as $value) {
      $node_arr[] = $value->nid;
    }

    $result2 = db_query('SELECT nid, COUNT(title) c FROM {maorimaps.node} GROUP BY title HAVING c = 1')->fetchAll();
    foreach ($result2 as $value) {
      $node_arr[] = $value->nid;
    }

    //To merge both English and Te Roe versions under one node.


    $query = db_select('maorimaps.node', 'n')
      ->fields('n', array(
        'nid',
        'language',
        'title',
        'uid',
        'status',
        'created',
        'changed',
        'comment',
        'promote',
        'sticky',
        'tnid',
        'translate'
      ))
      ->fields('street', array('field_street_value'))
      ->fields('suburb', array('field_suburb_value'))
      ->fields('city', array('field_city_value'))
      ->fields('postcode', array('field_postcode_value'))
      ->fields('caretaker', array('field_caretaker_value'))
      ->fields('caretaker_email', array('field_caretaker_email_email'))
      ->fields('caretaker_phone', array('field_caretaker_phone_value'))
      ->fields('chairperson', array('field_chairperson_value'))
      ->fields('chairperson_email', array('field_chairperson_email_email'))
      ->fields('chairperson_phone', array('field_chairperson_phone_value'))
      ->fields('secretary', array('field_secretary_value'))
      ->fields('secretary_email', array('field_secretary_email_email'))
      ->fields('secretary_phone', array('field_secretary_phone_value'))
      ->fields('members', array('field_members_value'))
      ->fields('members_email', array('field_members_email_email'))
      ->fields('members_phone', array('field_members_phone_value'))
      ->fields('trustees', array('field_trustees_value'))
      ->fields('trustees_email', array('field_trustees_email_email'))
      ->fields('trustees_phone', array('field_trustees_phone_value'))
      ->fields('general_phone', array('field_phone_1_value'))
      ->fields('general_email', array('field_email_email'))
      ->fields('general_web', array('field_web_url'))
      ->fields('general_web', array('field_web_title'))
      ->fields('general_fb', array('field_facebook_link_value'))
      ->fields('general_twitter', array('field_twitter_link_value'))
      ->fields('general_linkedin', array('field_linkedin_link_value'))
      ->fields('general_whatsapp', array('field_whatsapp_link_value'))
      ->fields('general_snapchat', array('field_snapchat_link_value'))
      ->fields('coords', array('field_coords_lat', 'field_coords_lon'))
      //->condition('n.type', 'marae', '=')
      ->condition('n.nid', $node_arr, 'IN');

    $query->leftjoin('maorimaps.field_data_field_street', 'street', 'n.nid = street.entity_id');
    $query->leftjoin('maorimaps.field_data_field_suburb', 'suburb', 'n.nid = suburb.entity_id');
    $query->leftjoin('maorimaps.field_data_field_city', 'city', 'n.nid = city.entity_id');
    $query->leftjoin('maorimaps.field_data_field_postcode', 'postcode', 'n.nid = postcode.entity_id');

    //For Contacts entity creation
    $query->leftjoin('maorimaps.field_data_field_caretaker', 'caretaker', 'n.nid = caretaker.entity_id');
    $query->leftjoin('maorimaps.field_data_field_caretaker_email', 'caretaker_email', 'n.nid = caretaker_email.entity_id');
    $query->leftjoin('maorimaps.field_data_field_caretaker_phone', 'caretaker_phone', 'n.nid = caretaker_phone.entity_id');
    $query->leftjoin('maorimaps.field_data_field_chairperson', 'chairperson', 'n.nid = chairperson.entity_id');
    $query->leftjoin('maorimaps.field_data_field_chairperson_email', 'chairperson_email', 'n.nid = chairperson_email.entity_id');
    $query->leftjoin('maorimaps.field_data_field_chairperson_phone', 'chairperson_phone', 'n.nid = chairperson_phone.entity_id');
    $query->leftjoin('maorimaps.field_data_field_secretary', 'secretary', 'n.nid = secretary.entity_id');
    $query->leftjoin('maorimaps.field_data_field_secretary_email', 'secretary_email', 'n.nid = secretary_email.entity_id');
    $query->leftjoin('maorimaps.field_data_field_secretary_phone', 'secretary_phone', 'n.nid = secretary_phone.entity_id');
    $query->leftjoin('maorimaps.field_data_field_members', 'members', 'n.nid = members.entity_id');
    $query->leftjoin('maorimaps.field_data_field_members_email', 'members_email', 'n.nid = members_email.entity_id');
    $query->leftjoin('maorimaps.field_data_field_members_phone', 'members_phone', 'n.nid = members_phone.entity_id');
    $query->leftjoin('maorimaps.field_data_field_trustees', 'trustees', 'n.nid = trustees.entity_id');
    $query->leftjoin('maorimaps.field_data_field_trustees_email', 'trustees_email', 'n.nid = trustees_email.entity_id');
    $query->leftjoin('maorimaps.field_data_field_trustees_phone', 'trustees_phone', 'n.nid = trustees_phone.entity_id');
    $query->leftjoin('maorimaps.field_data_field_phone_1', 'general_phone', 'n.nid = general_phone.entity_id');
    $query->leftjoin('maorimaps.field_data_field_email', 'general_email', 'n.nid = general_email.entity_id');
    $query->leftjoin('maorimaps.field_data_field_web', 'general_web', 'n.nid = general_web.entity_id');
    $query->leftjoin('maorimaps.field_data_field_facebook_link', 'general_fb', 'n.nid = general_fb.entity_id');
    $query->leftjoin('maorimaps.field_data_field_twitter_link', 'general_twitter', 'n.nid = general_twitter.entity_id');
    $query->leftjoin('maorimaps.field_data_field_linkedin_link', 'general_linkedin', 'n.nid = general_linkedin.entity_id');
    $query->leftjoin('maorimaps.field_data_field_whatsapp_link', 'general_whatsapp', 'n.nid = general_whatsapp.entity_id');
    $query->leftjoin('maorimaps.field_data_field_snapchat_link', 'general_snapchat', 'n.nid = general_snapchat.entity_id');

    //For Coords
    $query->leftjoin('maorimaps.field_data_field_coords', 'coords', 'n.nid = coords.entity_id');

    // For feature_image field
    //$query->leftjoin('content_field_feature_image', 'cff', 'cff.vid = n.vid');
    //$query->leftjoin('files', 'ffi', 'ffi.fid = cff.field_feature_image_fid and ffi.status = 1');
    $query->orderBy('n.title');


    $this->source = new MigrateSourceSQL($query, $source_fields);

    $this->destination = new MigrateDestinationNode('point_of_interest', array('text_format' => 'full_html'));

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          //'description' => 'D7 Unique Node ID',
          'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );


    // Make the mappings
    $this->addFieldMapping('nid', 'nid');
    $this->addFieldMapping('title', 'poi_title');
    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    $this->addFieldMapping('uid')->defaultValue(1);
    $this->addFieldMapping('revision')->defaultValue(TRUE);
    $this->addFieldMapping('revision_uid', 'uid');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('promote', 'promote');
    $this->addFieldMapping('sticky', 'sticky');
    $this->addFieldMapping('comment', 'comment');
    $this->addFieldMapping('language', 'language');
    if (module_exists('path')) {
      $this->addFieldMapping('path')
        ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
          ->issueGroup(t('DNM'));

      }
    }
    $this->addFieldMapping('log')->issueGroup(t('DNM'));
    $this->addFieldMapping('tnid', 'tnid');
    $this->addFieldMapping('translate', 'translate');

    //Entity Mapping
    $this->addFieldMapping('field_description', 'poi_description_id');
    $this->addFieldMapping('field_marae', 'poi_marae_id');
    $this->addFieldMapping('field_waterfall')->issueGroup(t('DNM'));

    $this->addFieldMapping('field_street', 'field_street_value');
    $this->addFieldMapping('field_suburb', 'field_suburb_value');
    $this->addFieldMapping('field_city', 'field_city_value');
    $this->addFieldMapping('field_postcode', 'field_postcode_value');
    $this->addFieldMapping('field_contact', 'contact_entity_id');


    //Coords
    //$location_arguments = MigrateLocationFieldHandler::arguments();
    //$this->addFieldMapping('field_coords', 'Location')->xpath('Location')->callbacks('unserialize')->arguments($location_arguments);

    $this->addFieldMapping('field_coords')->defaultValue(TRUE);
    $this->addFieldMapping('field_coords:latitude', 'coords_lat');
    $this->addFieldMapping('field_coords:longitude', 'coords_lng');
    $this->addFieldMapping('field_coords:name')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:street')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:additional')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:city')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:province')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:postal_code')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:country')->defaultValue('NZ');
    $this->addFieldMapping('field_coords:source')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_coords:is_primary')->defaultValue(TRUE);

    //Mapping of Image Fields
    $this->addFieldMapping('field_gallery_images', 'gallery_images');
    $this->addFieldMapping('field_gallery_images:file_class')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_gallery_images:preserve_files')
      ->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping('field_gallery_images:destination_dir')
      ->defaultValue(DEST_DIR_POI);
    $this->addFieldMapping('field_gallery_images:file_replace')
      ->defaultValue(TRUE);
    $this->addFieldMapping('field_gallery_images:destination_file')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_gallery_images:urlencode')
      ->issueGroup(t('DNM'));
    $this->addFieldMapping('field_gallery_images:alt')->issueGroup(t('DNM'));
    $this->addFieldMapping('field_gallery_images:title')->issueGroup(t('DNM'));;
    $this->addFieldMapping('field_gallery_images:source_dir')
      ->defaultValue(SOURCE_DIR_POI);

    $this->addFieldMapping('field_type')->defaultValue('Marae');
  }

  public function prepareRow($row) {

    //Remove whitespaces
    $row->poi_title = trim($row->title);

    $maraes = db_select('maorimaps.node', 'n')
      ->fields('n', array('nid'))
      ->condition('n.title', '%' . $row->title . '%', 'LIKE')
      ->execute()
      ->fetchAll();

    $description_entity_arr = $image_arr = array();

    foreach ($maraes as $marae) {
      //Entity Reference Description
      $description_entity_id = db_select('field_data_field_poi_description_source_id', 'des')
        ->fields('des', array('entity_id'))
        ->condition('des.field_poi_description_source_id_value', $marae->nid)
        ->execute()
        ->fetchAll();

      foreach ($description_entity_id as $value) {
        $description_entity_arr[] = $value->entity_id;
      }
    }

    $row->poi_description_id = $description_entity_arr;

    //Gallery Images

    $query = db_select('maorimaps.field_data_field_gallery_images', 'gi')
      ->fields('gi', array('field_gallery_images_fid'))
      ->fields('fm', array('filename'))
      ->condition('gi.entity_id', $row->nid)
      ->condition('gi.bundle', '%marae%', 'LIKE');

    $query->leftjoin('maorimaps.file_managed', 'fm', 'gi.field_gallery_images_fid = fm.fid');

    $result = $query
      ->execute()
      ->fetchAll();

    foreach ($result as $value) {
      $image_arr[] = $value->filename;
    }

    $row->gallery_images = $image_arr;


    //Entity Reference Contacts

    $entity_type = 'poi_contacts';
    $entity = entity_create($entity_type, array('type' => 'poi_contacts'));
    $wrapper = entity_metadata_wrapper($entity_type, $entity);

    //Entity Reference Contacts - General
    if (!empty($row->field_email_email)) {
      $wrapper->field_poi_contact_type = "General";
      $wrapper->field_poi_contact_name = $row->title;
      $wrapper->field_poi_contact_phone = $row->field_phone_1_value;
      $wrapper->field_poi_contact_email = $row->field_email_email;
      $link = array(
        'url' => $row->field_web_url,
        'title' => $row->field_web_title,
        'attributes' => array()
      );
      $wrapper->field_poi_contact_web->set($link);

      //$wrapper->field_poi_contact_web=  array('field_poi_contact_web_url' => $row->field_web_url, 'field_poi_contact_web_title' => $row->field_web_title);
      $wrapper->field_poi_contact_facebook_link = $row->field_facebook_link_value;
      $wrapper->field_poi_contact_twitter_link = $row->field_twitter_link_value;
      $wrapper->field_poi_contact_linkedin_link = $row->field_linkedin_link_value;
      $wrapper->field_poi_contact_whatsapp_link = $row->field_whatsapp_link_value;
      $wrapper->field_poi_contact_snapchat_link = $row->field_snapchat_link_value;
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Caretaker
    if (!empty($row->field_caretaker_value)) {
      $wrapper->field_poi_contact_type = "Caretaker";
      $wrapper->field_poi_contact_name = $row->field_caretaker_value;
      $wrapper->field_poi_contact_phone = $row->field_caretaker_phone_value;
      $wrapper->field_poi_contact_email = $row->field_caretaker_email_email;
      $link = array(
        'url' => '',
        'title' => '',
        'attributes' => array()
      );
      $wrapper->field_poi_contact_web->set($link);

      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Chairperson
    if (!empty($row->field_chairperson_value)) {
      $wrapper->field_poi_contact_type = "Chairperson";
      $wrapper->field_poi_contact_name = $row->field_chairperson_value;
      $wrapper->field_poi_contact_phone = $row->field_chairperson_phone_value;
      $wrapper->field_poi_contact_email = $row->field_chairperson_email_email;
      $link = array(
        'url' => '',
        'title' => '',
        'attributes' => array()
      );
      $wrapper->field_poi_contact_web->set($link);
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Secretary
    if (!empty($row->field_secretary_value)) {
      $wrapper->field_poi_contact_type = "Secretary";
      $wrapper->field_poi_contact_name = $row->field_secretary_value;
      $wrapper->field_poi_contact_phone = $row->field_secretary_phone_value;
      $wrapper->field_poi_contact_email = $row->field_secretary_email_email;
      $link = array(
        'url' => '',
        'title' => '',
        'attributes' => array()
      );
      $wrapper->field_poi_contact_web->set($link);
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Members
    if (!empty($row->field_members_value)) {
      $wrapper->field_poi_contact_type = "Member";
      $wrapper->field_poi_contact_name = $row->field_members_value;
      $wrapper->field_poi_contact_phone = $row->field_members_phone_value;
      $wrapper->field_poi_contact_email = $row->field_members_email_email;
      $link = array(
        'url' => '',
        'title' => '',
        'attributes' => array()
      );
      $wrapper->field_poi_contact_web->set($link);
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Trustees
    if (!empty($row->field_trustees_value)) {
      $wrapper->field_poi_contact_type = "Trustee";
      $wrapper->field_poi_contact_name = $row->field_trustees_value;
      $wrapper->field_poi_contact_phone = $row->field_trustees_phone_value;
      $wrapper->field_poi_contact_email = $row->field_trustees_email_email;
      $link = array(
        'url' => '',
        'title' => '',
        'attributes' => array()
      );
      $wrapper->field_poi_contact_web->set($link);
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    $related_contacts = db_select('field_data_field_poi_contact_source_id', 'con')
      ->fields('con', array('entity_id'))
      ->condition('con.field_poi_contact_source_id_value', $row->nid)
      ->execute()
      ->fetchAll();

    $related_contact_arr = array();
    foreach ($related_contacts as $value) {
      $related_contact_arr[] = $value->entity_id;
    }

    $row->contact_entity_id = $related_contact_arr;

    //Entity Reference Contacts

    //Coords
    $coords = db_select('maorimaps.field_data_field_coords', 'coords')
      ->fields('coords', array('field_coords_lat', 'field_coords_lon'))
      ->condition('coords.entity_id', $row->nid)
      ->execute()
      ->FetchAll();

    Foreach ($coords as $value) {
      $row->coords_lat = $value->field_coords_lat;
      $row->coords_lng = $value->field_coords_lon;
      break;
    }


    //Entity Reference Marae
    $marae_entity_id = db_select('field_data_field_poi_type_marae_source_id', 'mar')
      ->fields('mar', array('entity_id'))
      ->condition('mar.field_poi_type_marae_source_id_value', $row->nid)
      ->execute()
      ->fetchAll();

    $marae_entity_arr = array();
    foreach ($marae_entity_id as $value) {
      $marae_entity_arr[] = $value->entity_id;
    }

    $row->poi_marae_id = $marae_entity_arr;
    //Entity Reference Marae

    return TRUE;
  }
}

