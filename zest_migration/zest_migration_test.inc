<?php

define ('STORE_DIR', 'public://');

class ZestNodetypeTest1Migration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);

    // Remove non-existing nids from comment statistics
    //db_query ("delete from node_comment_statistics where nid not in (select nid from node)");

    $this->description = t('Migrates nodetype-Marae');

    $source_fields = array (
      'contact_entity_id' => t('POI description ID'),
    );

    $query = db_select('maorimaps.node', 'n')
      ->fields('n', array('nid', 'language', 'title', 'uid', 'status', 'created', 'changed', 'comment', 'promote', 'sticky', 'tnid', 'translate'))
      ->condition('n.type', 'marae', '=');



    $query->orderBy('n.changed');


    $this->source = new MigrateSourceSQL($query, $source_fields);

    $this->destination = new MigrateDestinationNode('test1', array('text_format' => 'full_html'));

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          //'description' => 'D7 Unique Node ID',
          'alias' => 'n',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );


    // Make the mappings
    $this->addFieldMapping('nid', 'nid');
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('is_new')->defaultValue(FALSE);
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('revision')->defaultValue(TRUE);
    $this->addFieldMapping('revision_uid', 'uid');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('promote', 'promote');
    $this->addFieldMapping('sticky', 'sticky');
    $this->addFieldMapping('comment', 'comment');
    $this->addFieldMapping('language', 'language');
    if (module_exists('path')) {
      $this->addFieldMapping('path')
        ->issueGroup(t('DNM'));
      if (module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
          ->issueGroup(t('DNM'));

      }
    }
    $this->addFieldMapping('log')->issueGroup(t('DNM'));
    $this->addFieldMapping('tnid', 'tnid');
    $this->addFieldMapping('translate', 'translate');

    $this->addFieldMapping('field_contact', 'contact_entity_id');



  }

  public function prepareRow($row) {
    //Entity Reference Contacts

    $entity_type = 'poi_contacts';
    $entity = entity_create($entity_type, array('type' => 'poi_contacts'));
    $wrapper = entity_metadata_wrapper($entity_type, $entity);

    //Entity Reference Contacts - General
    if(!empty($row->field_caretaker_value)) {
      $wrapper->field_poi_contact_type = "General";
      $wrapper->field_poi_contact_name = $row->title;
      $wrapper->field_poi_contact_phone = $row->field_phone_1_value;
      $wrapper->field_poi_contact_email = $row->field_email_email;
      $wrapper->field_poi_contact_web=  array('field_web_url' => $row->field_web_url, 'field_web_title' => $row->field_web_title);
      $wrapper->field_poi_contact_facebook_link = $row->field_facebook_link_value;
      $wrapper->field_poi_contact_twitter_link = $row->field_twitter_link_value;
      $wrapper->field_poi_contact_linkedin_link = $row->field_linkedin_link_value;
      $wrapper->field_poi_contact_whatsapp_link = $row->field_whatsapp_link_value;
      $wrapper->field_poi_contact_snapchat_link = $row->field_snapchat_link_value;
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

     //Entity Reference Contacts - Caretaker
    if(!empty($row->field_caretaker_value)) {
      $wrapper->field_poi_contact_type = "Caretaker";
      $wrapper->field_poi_contact_name = $row->field_caretaker_value;
      $wrapper->field_poi_contact_phone = $row->field_caretaker_phone_value;
      $wrapper->field_poi_contact_email = $row->field_caretaker_email_email;
      $wrapper->field_poi_contact_web= array('field_web_url' => '', '' => 'field_web_title');
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Chairperson
    if(!empty($row->field_chairperson_value)) {
      $wrapper->field_poi_contact_type = "Chairperson";
      $wrapper->field_poi_contact_name = $row->field_chairperson_value;
      $wrapper->field_poi_contact_phone = $row->field_chairperson_phone_value;
      $wrapper->field_poi_contact_email = $row->field_chairperson_email_email;
      $wrapper->field_poi_contact_web= array('field_web_url' => '', '' => 'field_web_title');
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Secretary
    if(!empty($row->field_secretary_value)) {
      $wrapper->field_poi_contact_type = "Secretary";
      $wrapper->field_poi_contact_name = $row->field_secretary_value;
      $wrapper->field_poi_contact_phone = $row->field_secretary_phone_value;
      $wrapper->field_poi_contact_email = $row->field_secretary_email_email;
      $wrapper->field_poi_contact_web= array('field_web_url' => '', '' => 'field_web_title');
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Members
    if(!empty($row->field_members_value)) {
      $wrapper->field_poi_contact_type = "Member";
      $wrapper->field_poi_contact_name = $row->field_members_value;
      $wrapper->field_poi_contact_phone = $row->field_members_phone_value;
      $wrapper->field_poi_contact_email = $row->field_members_email_email;
      $wrapper->field_poi_contact_web= array('field_web_url' => '', '' => 'field_web_title');
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    //Entity Reference Contacts - Trustees
    if(!empty($row->field_trustees_value)) {
      $wrapper->field_poi_contact_type = "Trustee";
      $wrapper->field_poi_contact_name = $row->field_trustees_value;
      $wrapper->field_poi_contact_phone = $row->field_trustees_phone_value;
      $wrapper->field_poi_contact_email = $row->field_trustees_email_email;
      $wrapper->field_poi_contact_web= array('field_web_url' => '', '' => 'field_web_title');
      $wrapper->field_poi_contact_facebook_link = '';
      $wrapper->field_poi_contact_twitter_link = '';
      $wrapper->field_poi_contact_linkedin_link = '';
      $wrapper->field_poi_contact_whatsapp_link = '';
      $wrapper->field_poi_contact_snapchat_link = '';
      $wrapper->field_poi_contact_source_id = $row->nid;
      $wrapper->save();
    }

    $related_contacts = db_select('field_data_field_poi_contact_source_id', 'con')
      ->fields('con', array('entity_id'))
      ->condition('con.field_poi_contact_source_id_value', $row->nid)
      ->execute()
      ->fetchAll();

    $related_contact_arr = array();
    foreach($related_contacts as $value) {
      $related_contact_arr[] = $value->entity_id;
    }

    $row->contact_entity_id = $related_contact_arr;

    //Entity Reference Contacts

    //Coords
    $coords= db_select('maorimaps.field_data_field_coords', 'coords')
      ->fields('coords', array('field_coords_lat', 'field_coords_lon'))
      ->condition('coords.entity_id', $row->nid)
      ->execute()
      ->FetchAll();

    Foreach($coords as $value) {
      $row->coords_lat = $value->field_coords_lat;
      $row->coords_lng = $value->field_coords_lon;
      break;
    }

    //Gallery Images

    $query = db_select('maorimaps.field_data_field_gallery_images', 'gi')
      ->fields('gi', array('field_gallery_images_fid'))
      ->fields('fm', array('filename'))
      ->condition('gi.entity_id', $row->nid)
      ->condition('gi.bundle', '%marae%', 'LIKE');

    $query->leftjoin('file_managed', 'fm', 'fm.fid = gi.field_gallery_images_fid');

    $result = $query
      ->execute()
      ->fetch();

    $image_arr = array();
    foreach($result as $value) {
      $image_arr[] = $value->filename;
    }
    $row->gallery_images = $image_arr;


    //Entity Reference Marae
    $marae_entity_id = db_select('field_data_field_poi_type_marae_source_id', 'mar')
      ->fields('mar', array('entity_id'))
      ->condition('mar.field_poi_type_marae_source_id_value', $row->nid)
      ->execute()
      ->fetchAll();

    $marae_entity_arr = array();
    foreach($marae_entity_id as $value) {
      $marae_entity_arr[] = $value->entity_id;
    }

    $row->poi_marae_id = $marae_entity_arr;
    //Entity Reference Marae

    return TRUE;
  }
}

