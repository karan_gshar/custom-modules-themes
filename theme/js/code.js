
  jQuery(document).ready(function() {
    
  	//Check to see if the window is top if not then display button
    jQuery(window).scroll(function(){
      if (jQuery(this).scrollTop() > 100) {
        jQuery('.scrollToTop').fadeIn();
      } else {
        jQuery('.scrollToTop').fadeOut();
      }
    });
    
    //Click event to scroll to top
    jQuery('.scrollToTop').click(function(){
      jQuery('html, body').animate({scrollTop : 0},800);
      return false;
    });
          
          
     

});

  jQuery(window).resize(function() {

        jQuery(".view-id-store.view-display-id-page_3 .view-content").isotope('reLayout');

        jQuery(".view-id-store.view-display-id-page_9 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-page_6 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-page_7 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-page_8 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-page_9 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-page_10 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-page_11 .view-content").isotope('reLayout');

        jQuery(".view-id-portfolios.view-display-id-block_4 .view-content").isotope('reLayout');
    
    });

  
  jQuery(document).ready(function(){  
	
      var viewCont1 = jQuery('.view-id-store.view-display-id-page_3 .view-content');
      viewCont1.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont2 = jQuery('.view-id-store.view-display-id-page_9 .view-content');
      viewCont2.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });


      var viewCont3 = jQuery('.view-id-portfolios.view-display-id-page_6 .view-content');
      viewCont3.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont4 = jQuery('.view-id-portfolios.view-display-id-page_7 .view-content');
      viewCont4.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont5 = jQuery('.view-id-portfolios.view-display-id-page_8 .view-content');
      viewCont5.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont6 = jQuery('.view-id-portfolios.view-display-id-page_9 .view-content');
      viewCont6.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont7 = jQuery('.view-id-portfolios.view-display-id-page_10 .view-content');
      viewCont7.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont8 = jQuery('.view-id-portfolios.view-display-id-page_11 .view-content');
      viewCont8.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      var viewCont9 = jQuery('.view-id-portfolios.view-display-id-block_4 .view-content');
      viewCont9.isotope({
        // options
        itemSelector : '.views-row',
        layoutMode : 'masonry'
        
      });

      jQuery('.view-isotope-filter .views-row-first a').addClass('btn-primary');
          
      // filter items when filter link is clicked
      jQuery('.view-isotope-filter a').click(function(){

        var selector = jQuery(this).attr('data-filter');
        viewCont9.isotope({ filter: selector });
        jQuery('.view-isotope-filter a').removeClass('btn-primary');
        jQuery(this).addClass("btn-primary");
        return false;
      });



	jQuery("#accordion #collapseOne").collapse('hide');

	
  });

  

jQuery(document).ready(function(){

    // Isotope messes up in Chrome because it initiates before everything has loaded
    // This ensures everything has loaded before applying
    jQuery(window).load(function() {

      jQuery(".view-id-store.view-display-id-page_3 .view-content").isotope('reLayout');

      jQuery(".view-id-store.view-display-id-page_9 .view-content").isotope('reLayout');

      jQuery(".view-id-portfolios.view-display-id-page_6 .view-content").isotope('reLayout');

      jQuery(".view-id-portfolios.view-display-id-page_7 .view-content").isotope('reLayout');

      jQuery(".view-id-portfolios.view-display-id-page_8 .view-content").isotope('reLayout');

      jQuery(".view-id-portfolios.view-display-id-page_9 .view-content").isotope('reLayout');
      jQuery(".view-id-portfolios.view-display-id-page_10 .view-content").isotope('reLayout');
      jQuery(".view-id-portfolios.view-display-id-page_11 .view-content").isotope('reLayout');

      jQuery(".view-id-portfolios.view-display-id-block_4 .view-content").isotope('reLayout');

      
    
    });

});

