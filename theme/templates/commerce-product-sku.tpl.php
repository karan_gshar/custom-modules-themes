<?php

/**
 * @file
 * Default theme implementation to present the SKU on a product page.
 *
 * Available variables:
 * - $sku: The SKU to render.
 * - $label: If present, the string to use as the SKU label.
 *
 * Helper variables:
 * - $product: The fully loaded product object the SKU represents.
 */
?>
<?php if ($sku): ?>
  <span class="commerce-product-sku">
    <?php if ($label): ?>
      <span class="commerce-product-sku-label">
        <strong>Stock Code:</strong>
      </span>
    <?php endif; ?>
    <?php print $sku; ?>
  </span>
<?php endif; ?>
