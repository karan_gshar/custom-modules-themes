<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php $i = 0; ?>

<?php foreach ($rows as $id => $row): ?>





  


    <?php 
  
  //print_r($view->result[$i]->nid);

  

  $node = $view->result[$i];

  $terms = '';

  $language = 'und';

  $nodeId = $node->nid;

  $results = db_query('SELECT tid FROM {taxonomy_index} WHERE nid = :nid', array(':nid' => $nodeId));

  foreach ($results as $result) {

    $term = taxonomy_term_load($result->tid);

    $realname = $term->field_real_name[$language][0]['value'];

    $clean = str_replace('.', '', $realname);

    $terms = $clean.' '.$terms;

  }

  $classes_array[$id] = $terms.$classes_array[$id]; 

  $i = $i + 1 ;

?>
<div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>