<section id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>  >
<div class="container">
<div class="row">
  <div class="col-md-offset-1 col-md-10">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?><div class="title-div">
    <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
  
  </div>
  <?php endif;?>
  <?php print render($title_suffix); ?>

  	 <?php print $content ?>

  </div>
 
 </div>
 </div>

</section> <!-- /.block -->
