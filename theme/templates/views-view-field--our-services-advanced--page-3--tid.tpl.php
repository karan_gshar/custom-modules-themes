<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>



<?php 

	$term_id = $row->tid; 

	$data = taxonomy_select_nodes($term_id);

	$count = count($data);

?>

<?php if ($count > 0): ?>

<div class="row">

	  <div class="related col-md-9 col-md-offset-3">
	  <h4><?php print t('Related Work'); ?></h4>
	  <ul>
	  <?php for ($i=0 ; $i < $count; $i++): ?>
	  <?php if ($i < 3): ?>
	    
	    <?php 

	    	$node_id = $data[$i];

			$node = node_load($node_id);

			$path = drupal_lookup_path('alias','node/'.$node_id);

			$title = $node->title;

	    ?>
	 
	    <li><a href="<?php print $path; ?>" ><?php print $title; ?></a></li>
		
	  <?php endif; ?>

	<?php endfor; ?>
	</ul>
</div>
</div>
  <?php endif; ?>