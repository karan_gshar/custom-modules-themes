<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for xadvanced based themes when admin theme is not.
 *
 * @see theme/settings.inc
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function online_form_system_theme_settings_alter(&$form, &$form_state)  {
  // Work-around for a core bug affecting admin themes.
  // @see https://drupal.org/node/943212
  

}
