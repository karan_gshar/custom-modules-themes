<?php

class mailEntryController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'uid' => isset($user->uid) && !empty($user->uid) ? $user->uid : 27,
      'sent' => time(),
      'sender_from' => '',
      'recipients' => '',
      'email_type' => '',
      'email_subject' => '',
      'email_body' => '',
      'email_body_raw' => '',
    );
    return parent::create($values);
  }
}


/**
 * Task class.
 */
class mailEntry extends Entity {
  protected function defaultLabel() {
    return $this->subject;
  }
}
