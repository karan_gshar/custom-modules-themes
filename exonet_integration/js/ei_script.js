$ = jQuery;
function datatable_init() {
    try {
        var dTable = $(".datatable").dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "sDom": '<"H"Tflri>t<"F"flip>',
            "oTableTools": {
                "sSwfPath": "/" + Drupal.settings.ei_module_path + "/datatables/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "copy",
                    "print",
                    {
                        "sExtends": "collection",
                        "sButtonText": "Save",
                        "aButtons": ["csv", "xls", "pdf"]
                    }
                ]
            }
        })

        if (typeof(dTable.attr('sort_order')) != 'undefined' && (dTable.attr('sort_order') == 'asc' || dTable.attr('sort_order') == 'desc'))
            if (typeof(dTable.attr('id')) != 'undefined') {
                dTable.fnSort([[1, $('#' + dTable.attr('id')).attr('sort_order')]]);
            } else {
                dTable.fnSort([[1, dTable.attr('sort_order')]]);
            }

    } catch (err) {
        switch (err.message) {
            case "Cannot read property 'className' of undefined":
                alert('Check data for proper formating, special characters and proper line ending.' + "\n" +
                    'Error occurs because of missing columns, so table can\'t be created properly.');
                break;
            default:
                alert(err.message);
                break;
        }
    }
}

$(document).ready(function () {
    datatable_init();
    //$("body").ajaxComplete(function(event, XMLHttpRequest, ajaxOptions){
    //  var name = "";
    //  var value = "";
    //  if(typeof(ajaxOptions.extraData) != "undefined"){
    //    name = ajaxOptions.extraData._triggering_element_name;
    //    value = ajaxOptions.extraData._triggering_element_value;
    //  }
    //
    //  if(name.toLowerCase() == "op" && value.toLowerCase() == "execute") datatable_init();
    //});
})
