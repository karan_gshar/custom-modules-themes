<?php

/**
 * Implements hook_views_default_views().
 */
function exo_integration_views_default_views() {
  $view = new view();
  $view->name = 'mail_history';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'custom_mail_tracking';
  $view->human_name = 'Mail History';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Mail History';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'exo integration';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '10, 30, 50, 100, 500';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'email_type' => 'email_type',
    'sent' => 'sent',
    'name' => 'name',
    'sender_from' => 'sender_from',
    'recipients' => 'recipients',
    'email_subject' => 'email_subject',
    'email_body' => 'email_body',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'email_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sent' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sender_from' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'recipients' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'email_subject' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'email_body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No results.';
  /* Relationship: Mail Entry: Sender */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Mail Entry: Email_type */
  $handler->display->display_options['fields']['email_type']['id'] = 'email_type';
  $handler->display->display_options['fields']['email_type']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['fields']['email_type']['field'] = 'email_type';
  $handler->display->display_options['fields']['email_type']['label'] = '';
  $handler->display->display_options['fields']['email_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['email_type']['element_label_colon'] = FALSE;
  /* Field: Mail Entry: Date sent */
  $handler->display->display_options['fields']['sent']['id'] = 'sent';
  $handler->display->display_options['fields']['sent']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['fields']['sent']['field'] = 'sent';
  $handler->display->display_options['fields']['sent']['label'] = '';
  $handler->display->display_options['fields']['sent']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sent']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sent']['date_format'] = 'custom';
  $handler->display->display_options['fields']['sent']['custom_date_format'] = 'd/m/Y H:i';
  $handler->display->display_options['fields']['sent']['timezone'] = 'Pacific/Auckland';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Details';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<strong>Type:</strong><br/>[email_type]<br/>
  <strong>Date sent:</strong><br/>[sent]<br/>
  <strong>Sender:</strong><br/>[name]';
  /* Field: Mail Entry: Sender_from */
  $handler->display->display_options['fields']['sender_from']['id'] = 'sender_from';
  $handler->display->display_options['fields']['sender_from']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['fields']['sender_from']['field'] = 'sender_from';
  $handler->display->display_options['fields']['sender_from']['label'] = '';
  $handler->display->display_options['fields']['sender_from']['exclude'] = TRUE;
  $handler->display->display_options['fields']['sender_from']['element_label_colon'] = FALSE;
  /* Field: Mail Entry: Recipients */
  $handler->display->display_options['fields']['recipients']['id'] = 'recipients';
  $handler->display->display_options['fields']['recipients']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['fields']['recipients']['field'] = 'recipients';
  $handler->display->display_options['fields']['recipients']['label'] = '';
  $handler->display->display_options['fields']['recipients']['exclude'] = TRUE;
  $handler->display->display_options['fields']['recipients']['alter']['text'] = '<pre>[recipients]</pre>';
  $handler->display->display_options['fields']['recipients']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = 'From / To';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<strong>From:</strong><br/>[sender_from] <br/>
  <strong>To:</strong><br/>[recipients]';
  /* Field: Mail Entry: Email_subject */
  $handler->display->display_options['fields']['email_subject']['id'] = 'email_subject';
  $handler->display->display_options['fields']['email_subject']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['fields']['email_subject']['field'] = 'email_subject';
  $handler->display->display_options['fields']['email_subject']['label'] = 'Subject';
  $handler->display->display_options['fields']['email_subject']['alter']['text'] = '<pre>[email_subject] </pre>';
  /* Field: Mail Entry: Email_body */
  $handler->display->display_options['fields']['email_body']['id'] = 'email_body';
  $handler->display->display_options['fields']['email_body']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['fields']['email_body']['field'] = 'email_body';
  $handler->display->display_options['fields']['email_body']['label'] = 'Body';
  $handler->display->display_options['fields']['email_body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['email_body']['alter']['text'] = '<pre>[email_body]</pre>';
  /* Sort criterion: Mail Entry: Date sent */
  $handler->display->display_options['sorts']['sent']['id'] = 'sent';
  $handler->display->display_options['sorts']['sent']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['sorts']['sent']['field'] = 'sent';
  $handler->display->display_options['sorts']['sent']['order'] = 'DESC';
  $handler->display->display_options['sorts']['sent']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sent']['expose']['label'] = 'Date sent';
  /* Sort criterion: Mail Entry: Email_type */
  $handler->display->display_options['sorts']['email_type']['id'] = 'email_type';
  $handler->display->display_options['sorts']['email_type']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['sorts']['email_type']['field'] = 'email_type';
  $handler->display->display_options['sorts']['email_type']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['email_type']['expose']['label'] = 'Type';
  /* Sort criterion: Mail Entry: Sender */
  $handler->display->display_options['sorts']['uid']['id'] = 'uid';
  $handler->display->display_options['sorts']['uid']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['sorts']['uid']['field'] = 'uid';
  $handler->display->display_options['sorts']['uid']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['uid']['expose']['label'] = 'Sender';
  /* Sort criterion: Mail Entry: Sender_from */
  $handler->display->display_options['sorts']['sender_from']['id'] = 'sender_from';
  $handler->display->display_options['sorts']['sender_from']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['sorts']['sender_from']['field'] = 'sender_from';
  $handler->display->display_options['sorts']['sender_from']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['sender_from']['expose']['label'] = 'From';
  /* Sort criterion: Mail Entry: Mail entry ID */
  $handler->display->display_options['sorts']['eid']['id'] = 'eid';
  $handler->display->display_options['sorts']['eid']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['sorts']['eid']['field'] = 'eid';
  $handler->display->display_options['sorts']['eid']['order'] = 'DESC';
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Sender';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['block_description'] = 'mail_history_block';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Mail Entry: Email_type */
  $handler->display->display_options['filters']['email_type']['id'] = 'email_type';
  $handler->display->display_options['filters']['email_type']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['filters']['email_type']['field'] = 'email_type';
  $handler->display->display_options['filters']['email_type']['operator'] = 'contains';
  $handler->display->display_options['filters']['email_type']['group'] = 1;
  $handler->display->display_options['filters']['email_type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['email_type']['expose']['operator_id'] = 'email_type_op';
  $handler->display->display_options['filters']['email_type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['email_type']['expose']['operator'] = 'email_type_op';
  $handler->display->display_options['filters']['email_type']['expose']['identifier'] = 'email_type';
  $handler->display->display_options['filters']['email_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  /* Filter criterion: Mail Entry: Date sent */
  $handler->display->display_options['filters']['sent']['id'] = 'sent';
  $handler->display->display_options['filters']['sent']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['filters']['sent']['field'] = 'sent';
  $handler->display->display_options['filters']['sent']['group'] = 1;
  $handler->display->display_options['filters']['sent']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sent']['expose']['operator_id'] = 'sent_op';
  $handler->display->display_options['filters']['sent']['expose']['label'] = 'Date sent';
  $handler->display->display_options['filters']['sent']['expose']['description'] = 'YYYY-MM-DD HH:MM';
  $handler->display->display_options['filters']['sent']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['sent']['expose']['operator'] = 'sent_op';
  $handler->display->display_options['filters']['sent']['expose']['identifier'] = 'sent';
  $handler->display->display_options['filters']['sent']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['group'] = 1;
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Sender';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  /* Filter criterion: Mail Entry: Sender_from */
  $handler->display->display_options['filters']['sender_from']['id'] = 'sender_from';
  $handler->display->display_options['filters']['sender_from']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['filters']['sender_from']['field'] = 'sender_from';
  $handler->display->display_options['filters']['sender_from']['operator'] = 'contains';
  $handler->display->display_options['filters']['sender_from']['group'] = 1;
  $handler->display->display_options['filters']['sender_from']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sender_from']['expose']['operator_id'] = 'sender_from_op';
  $handler->display->display_options['filters']['sender_from']['expose']['label'] = 'From';
  $handler->display->display_options['filters']['sender_from']['expose']['operator'] = 'sender_from_op';
  $handler->display->display_options['filters']['sender_from']['expose']['identifier'] = 'sender_from';
  $handler->display->display_options['filters']['sender_from']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  /* Filter criterion: Mail Entry: Recipients */
  $handler->display->display_options['filters']['recipients']['id'] = 'recipients';
  $handler->display->display_options['filters']['recipients']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['filters']['recipients']['field'] = 'recipients';
  $handler->display->display_options['filters']['recipients']['operator'] = 'contains';
  $handler->display->display_options['filters']['recipients']['group'] = 1;
  $handler->display->display_options['filters']['recipients']['exposed'] = TRUE;
  $handler->display->display_options['filters']['recipients']['expose']['operator_id'] = 'recipients_op';
  $handler->display->display_options['filters']['recipients']['expose']['label'] = 'To';
  $handler->display->display_options['filters']['recipients']['expose']['operator'] = 'recipients_op';
  $handler->display->display_options['filters']['recipients']['expose']['identifier'] = 'recipients';
  $handler->display->display_options['filters']['recipients']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  /* Filter criterion: Mail Entry: Email_subject */
  $handler->display->display_options['filters']['email_subject']['id'] = 'email_subject';
  $handler->display->display_options['filters']['email_subject']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['filters']['email_subject']['field'] = 'email_subject';
  $handler->display->display_options['filters']['email_subject']['operator'] = 'contains';
  $handler->display->display_options['filters']['email_subject']['group'] = 1;
  $handler->display->display_options['filters']['email_subject']['exposed'] = TRUE;
  $handler->display->display_options['filters']['email_subject']['expose']['operator_id'] = 'email_subject_op';
  $handler->display->display_options['filters']['email_subject']['expose']['label'] = 'Subject';
  $handler->display->display_options['filters']['email_subject']['expose']['operator'] = 'email_subject_op';
  $handler->display->display_options['filters']['email_subject']['expose']['identifier'] = 'email_subject';
  $handler->display->display_options['filters']['email_subject']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  /* Filter criterion: Mail Entry: Email_body */
  $handler->display->display_options['filters']['email_body']['id'] = 'email_body';
  $handler->display->display_options['filters']['email_body']['table'] = 'custom_mail_tracking';
  $handler->display->display_options['filters']['email_body']['field'] = 'email_body';
  $handler->display->display_options['filters']['email_body']['operator'] = 'contains';
  $handler->display->display_options['filters']['email_body']['group'] = 1;
  $handler->display->display_options['filters']['email_body']['exposed'] = TRUE;
  $handler->display->display_options['filters']['email_body']['expose']['operator_id'] = 'email_body_op';
  $handler->display->display_options['filters']['email_body']['expose']['label'] = 'Body';
  $handler->display->display_options['filters']['email_body']['expose']['operator'] = 'email_body_op';
  $handler->display->display_options['filters']['email_body']['expose']['identifier'] = 'email_body';
  $handler->display->display_options['filters']['email_body']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    18 => 0,
    17 => 0,
    19 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0,
    12 => 0,
    13 => 0,
  );
  $handler->display->display_options['path'] = 'admin/management/mail_history';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Email History';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;


  $views[$view->name] = $view;

  return $views;
}