<?php
global $website_ref;
module_load_include('inc', 'exo_integration', 'includes/dollar_sign');
$label = 'font-weight: bold;';
$lefttop = 'border-left: 1pt solid #ccc; border-top: 1pt solid #ccc; padding: 0cm 0.1cm 0cm 0.1cm;';
$leftbottom = 'border-left: 1pt solid #ccc; border-bottom: 1pt solid #ccc; padding: 0cm 0.1cm 0cm 0.1cm;';
$bottom = 'border-bottom: 1pt solid #ccc;';
$right = 'border-right: 1pt solid #ccc;';
$center_text = 'text-align: center;';
$full_width = 'width: 100%;';
$padding_general = 'margin-bottom: 0pt; font-size: 10pt; line-height: 12pt; font-family: Arial;';

function wrap_text($text, $style) {

  return
    '<p class="MsoNormal" style="' . $style . '">' .
    '<span>' .
    $text .
    '</span>' .
    '</p>';
}

?>
<p>
  Thank you for your order - Sektor Ref: "WEB ORD  #<?php echo $data['orderid']; ?>"
</p>

<table cellspacing="0" cellpadding="0" border="0">
  <tbody>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Company:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['deliveryadd1'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Name:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['contactname'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Email address:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['email'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Address:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['deliveryadd2'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text($data['labels']['sub'] . ':', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['deliveryadd3'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text($data['labels']['city'] . ':', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['deliveryadd4'], $padding_general); ?>
    </td>
  </tr>
  <?php if (strtolower($website_ref) == 'au') : ?>
    <tr>
      <td style="<?php echo $label . $lefttop; ?>">
        <?php echo wrap_text('State:', $padding_general); ?>
      </td>
      <td style="<?php echo $lefttop . $right; ?>">
        <?php echo wrap_text($data['deliveryadd5'], $padding_general); ?>
      </td>
    </tr>
  <?php endif; ?>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Country:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text(strtolower($website_ref) == 'au' ? 'Australia' : $data['deliveryadd6'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text(strtolower($website_ref) == 'au' ? 'Contact Name:' : 'Postal Code:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['deliverypostcode'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Phone Number:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['phone'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Purchase Order Number:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text(isset($data['customerorderno']) ? $data['customerorderno'] : '', $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Item Staging:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text(isset($data['staging']) ? ($data['staging'] ? 'Yes' : 'No') : 'N/A', $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Item Staging Instructions:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text(isset($data['staginginstructions']) && $data['staginginstructions'] ? check_markup($data['staginginstructions']) : 'N/A', $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop . $bottom; ?>">
      <?php echo wrap_text('Part Shipping:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right . $bottom; ?>">
      <?php echo wrap_text(isset($data['partshipped']) ? ($data['partshipped'] ? 'Yes' : 'No') : 'N/A', $padding_general); ?>
    </td>
  </tr>
  </tbody>
</table>

<p>&nbsp;</p>

<table cellspacing="0" cellpadding="0" border="0" style="<?php echo $full_width; ?>">
  <thead>
  <tr>
    <td style="<?php echo $lefttop . $bottom . $label; ?>">
      <?php echo wrap_text('Product', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Quantity', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Price per unit ' . DOLLAR_SIGN, $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $right . $label . $center_text; ?>">
      <?php echo wrap_text('Total price ' . DOLLAR_SIGN, $padding_general); ?>
    </td>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($data['items'] as $item) : ?>
    <tr>
      <td style="<?php echo $leftbottom; ?>">
        <?php
        $table =
          '<table cellspacing="0" cellpadding="0" border="0">'
          . '<tbody>'
          . '<tr>'
          . '<td style="' . $label . '">' . wrap_text('Stockcode:', $padding_general) . '</td>'
          . '<td>' . wrap_text($item['stockcode'], $padding_general) . '</td>'
          . '</tr>'
          . '<tr>'
          . '<td style="' . $label . '">' . wrap_text('Description:', $padding_general) . '</td>'
          . '<td>' . wrap_text($item['description'], $padding_general) . '</td>'
          . '</tr>'
          . '</tbody>'
          . '</table>';
        //          echo wrap_text($table, $padding_general);
        echo $table;
        ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text($item['quantity'], $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text($item['unitprice'], $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $right . $center_text; ?>">
        <?php echo wrap_text($item['price'], $padding_general); ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<p>&nbsp;</p>

<table cellspacing="0" cellpadding="0" border="0">
  <tbody>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('TOTAL', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text(DOLLAR_SIGN . number_format(round($data['subtotal'], 2), 2, '.', ''), $padding_general); ?>
    </td>
  </tr>
  <?php if (strtolower($website_ref) != 'my' && strtolower($website_ref) != 'th') : ?>
    <tr>
      <td style="<?php echo $label . $lefttop; ?>">
        <?php echo wrap_text('SHIPPING', $padding_general); ?>
      </td>
      <td style="<?php echo $lefttop . $right; ?>">
        <?php echo wrap_text(DOLLAR_SIGN . number_format(round($data['freight'], 2), 2, '.', ''), $padding_general); ?>
      </td>
    </tr>
  <?php endif; ?>
  <?php if (GST_RATE) : ?>
    <tr>
      <td style="<?php echo $label . $lefttop; ?>">
        <?php echo wrap_text(GST_LABEL, $padding_general); ?>
      </td>
      <td style="<?php echo $lefttop . $right; ?>">
        <?php echo wrap_text(DOLLAR_SIGN . number_format(round($data['gst'], 2), 2, '.', ''), $padding_general); ?>
      </td>
    </tr>
  <?php endif; ?>
  <?php if (!empty($data['creditcardsurcharge'])) : ?>
    <tr>
      <td style="<?php echo $label . $lefttop; ?>">
        <?php echo wrap_text('SURCHARGE', $padding_general); ?>
      </td>
      <td style="<?php echo $lefttop . $right; ?>">
        <?php echo wrap_text(DOLLAR_SIGN . number_format(round($data['creditcardsurcharge'], 2), 2, '.', ''), $padding_general); ?>
      </td>
    </tr>
  <?php endif; ?>
  <tr>
    <td style="<?php echo $label . $lefttop . $bottom; ?>">
      <?php echo wrap_text('ORDER TOTAL', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right . $bottom; ?>">
      <?php echo wrap_text(DOLLAR_SIGN . number_format(round($data['total'], 2), 2, '.', ''), $padding_general); ?>
    </td>
  </tr>
  </tbody>
</table>

<p>&nbsp;</p>

<div style="<?php echo $padding_general; ?>">
  <?php echo $data['footer_reference']; ?>
</div>
<div>
  <p>
    <?php echo '#end_of_email_check#' . rand() . '#'; ?>
  </p>
</div>
