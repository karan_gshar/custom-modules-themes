<?php
module_load_include('inc', 'exo_integration', 'includes/requests/pricing');
module_load_include('inc', 'exo_integration', 'includes/dollar_sign');

$label = 'font-weight: bold;';
$lefttop = 'border-left: 1pt solid #ccc; border-top: 1pt solid #ccc; padding: 0cm 0.1cm 0cm 0.1cm;';
$leftbottom = 'border-left: 1pt solid #ccc; border-bottom: 1pt solid #ccc; padding: 0cm 0.1cm 0cm 0.1cm;';
$bottom = 'border-bottom: 1pt solid #ccc;';
$right = 'border-right: 1pt solid #ccc;';
$center_text = 'text-align: center;';
$full_width = 'width: 100%;';
$padding_general = 'margin-bottom: 0pt; font-size: 10pt; line-height: 12pt; font-family: Arial;';

function wrap_text($text, $style) {

  return
    '<p class="MsoNormal" style="' . $style . '">' .
    '<span>' .
    $text .
    '</span>' .
    '</p>';
}

?>
<p>
  User <?php echo $user_email; ?> has submitted a PE online.
  <br/>
  Please see PE details below, for more details please access PE management
  application.
  <br/>
</p>

<table cellspacing="0" cellpadding="0" border="0">
  <tbody>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('End User Name:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['name'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Client Name:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['clientname'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Account #:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['accountno'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop; ?>">
      <?php echo wrap_text('Reference:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right; ?>">
      <?php echo wrap_text($data['referenceno'], $padding_general); ?>
    </td>
  </tr>
  <tr>
    <td style="<?php echo $label . $lefttop . $bottom; ?>">
      <?php echo wrap_text('Date Submitted:', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $right . $bottom; ?>">
      <?php echo wrap_text(date('d/m/Y', strtotime($data['datecreated'] . ' 00:00:00')), $padding_general); ?>
    </td>
  </tr>

  </tbody>
</table>

<p>&nbsp;</p>

<table cellspacing="0" cellpadding="0" border="0"
       style="<?php echo $full_width; ?>">
  <thead>
  <tr>
    <td style="<?php echo $lefttop . $bottom . $label; ?>">
      <?php echo wrap_text('Stockcode', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label; ?>">
      <?php echo wrap_text('Man. Code', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Quantity', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Current Buy Price ' . DOLLAR_SIGN, $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('List Price ' . DOLLAR_SIGN, $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Discount %', $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Sell Price ' . DOLLAR_SIGN, $padding_general); ?>
    </td>
    <td style="<?php echo $lefttop . $bottom . $label . $center_text; ?>">
      <?php echo wrap_text('Margin %', $padding_general); ?>
    </td>
    <td
      style="<?php echo $lefttop . $bottom . $right . $label . $center_text; ?>">
      <?php echo wrap_text('Buy Price ' . DOLLAR_SIGN, $padding_general); ?>
    </td>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($data['items'] as $item) : ?>
    <tr>
      <td style="<?php echo $leftbottom; ?>">
        <?php echo wrap_text($item['stockcode'], $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text($item['mancode'], $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text($item['quantity'], $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text(ei_get_price(array(
          'stockcode' => $item['stockcode'],
          'accountno' => $data['accountno'],
          'quantity' => $item['quantity']
        )), $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text(number_format($item['listprice'], 2, '.', ''), $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text(number_format($item['discount'], 2, '.', ''), $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text(number_format($item['sellprice'], 2, '.', ''), $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $center_text; ?>">
        <?php echo wrap_text(number_format($item['margin'], 2, '.', ''), $padding_general); ?>
      </td>
      <td style="<?php echo $leftbottom . $right . $center_text; ?>">
        <?php echo wrap_text(number_format($item['buyprice'], 2, '.', ''), $padding_general); ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<p>&nbsp;</p>

<div>
  <p>
    <?php echo '#end_of_email_check#' . rand() . '#'; ?>
  </p>
</div>