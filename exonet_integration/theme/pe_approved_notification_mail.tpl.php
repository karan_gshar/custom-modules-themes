Hello<?php echo $user_reference; ?>,<br/>
<br/>
This email is to confirm that <?php echo $reference; ?> has been approved,
however there may be changes to the original request. Your PE approval number is <?php echo $pe_number; ?>.
Please login to your account to view the approved PE: <a
  href="<?php echo $pe_link; ?>">link</a>.<br/>
<br/>
Should have you any questions or require further assistance, please reply to this email and we will respond as soon as we can.