Your request for an RMA repair has been submitted.<br/>
If you haven’t done so already, please login and print the <a
  href="<?php echo $data['rmaid_url']; ?>">RMA return
  slip</a> and attach this to your goods before returning.
<br/>
If you have any questions or require further assistance please contact us
<?php if (isset($data['reference'])) : ?>
  at <?php echo $data['reference']; ?>
<?php endif; ?>
.
