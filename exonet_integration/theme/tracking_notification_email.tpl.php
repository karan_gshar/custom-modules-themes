<div>This is to confirm that one or more of your items in your order has been shipped.<br/>
  Please note that if you selected Part Ship on your order, you will receive a separate notification as each item is dispatched.
</div><br/>
<div><strong>Sales Order Number: </strong><?php echo $order_id_label; ?></div>
<br/>
<div><strong>Purchase Order Number: </strong><?php echo $customer_order_no; ?>
</div>
<br/>
<div><strong>Tracking Number: </strong><?php echo $tracking_link; ?>
</div>
<br/>
<div>You can review complete details of your order on the My Orders page</div>
<br/>
<div>Thanks for choosing Sektor.</div>