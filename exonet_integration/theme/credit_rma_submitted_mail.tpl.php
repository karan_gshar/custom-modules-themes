Your request for an RMA credit has been submitted.<br/>
Your account manager will be in contact shortly.<br/>
An RMA number will only be issued if this request is approved.<br/>
<br/>
If you have any questions or require further assistance please contact us
<?php if (isset($data['reference'])) : ?>
  at <?php echo $data['reference']; ?>
<?php endif; ?>
.