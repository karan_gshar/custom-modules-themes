<p>
  Hello<?php echo $user_reference; ?>,<br/>
  <br/>
  This email is to confirm that <?php echo $reference; ?> has been received by
  Sektor
  but has not yet been submitted<?php echo $brand_reference; ?>. Please allow up
  to 48 hours for processing.<br/>
  <br/>
  Please note you will not be able to edit this PE online. If you would like to
  make changes to this PE, please reply to this email with the changes required.
  <br/>
  Should have you any questions or require further assistance, please reply to
  this email and we will respond as soon as we can.
  <br/>
</p>