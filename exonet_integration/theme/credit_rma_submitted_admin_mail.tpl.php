Hi<?php echo isset($acc_manager) && !empty($acc_manager) ? $acc_manager . ', ' : ''; ?>,
<br/>
<br/>
A request for an RMA credit has been submitted by<?php echo $data['rma-address_address_line_1']; ?>.

<h3>Sektor Account Holder Details</h3>
Contact Name:<?php echo $data['contact_name']; ?><br/>
Email:<?php echo $data['email']; ?><br/>
Phone:<?php echo $data['phone']; ?><br/>
Fax:<?php echo $data['fax']; ?><br/>
Mobile:<?php echo $data['mobile']; ?><br/>
Invoice #:<?php echo $data['invoice_no']; ?><br/>

Street:<?php echo $data['rma-address_address_line_2']; ?><br/>
<?php echo $data['labels']['sub']; ?>:<?php echo $data['rma-address_address_line_3']; ?>
<br/>
<?php echo $data['labels']['city']; ?>:<?php echo $data['rma-address_address_line_4']; ?>
<br/>
Country:<?php echo $data['rma-address_address_line_5']; ?><br/>
Postcode:<?php echo $data['rma-address_address_post_code']; ?><br/>
<h3>Item Details</h3>
Model #:<?php echo $data['model_no']; ?><br/>
Serial #:<?php echo $data['serial_no']; ?><br/>
Reason for Credit:<?php echo $data['credit_reason']; ?>
