<?php
echo $action; ?> executing:<br/>
<hr/>
<table>
  <tbody>
  <tr>
    <td><strong>Process id:</strong></td>
    <td><?php echo $process_id; ?></td>
  </tr>
  <tr>
    <td><strong>Type:</strong></td>
    <td><?php echo $type; ?></td>
  </tr>
  <tr>
    <td><strong>Time:</strong></td>
    <td><?php echo date('d/m/Y H:i'); ?></td>
  </tr>
  <?php if ($environment) : ?>
    <tr>
      <td><strong>Environment:</strong></td>
      <td><?php echo $environment; ?></td>
    </tr>
  <?php endif; ?>
  <?php if ($website_ref) : ?>
    <tr>
      <td><strong>Website Reference:</strong></td>
      <td><?php echo $website_ref; ?></td>
    </tr>
  <?php endif; ?>
  <tr>
    <td><strong>Domain:</strong></td>
    <td><?php echo $_SERVER['HTTP_HOST']; ?></td>
  </tr>
  </tbody>
  <br/>