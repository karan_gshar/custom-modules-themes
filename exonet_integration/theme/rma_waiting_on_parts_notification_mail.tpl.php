<?php echo $introduction; ?><br/>
Your<?php echo $reference; ?>requires parts that are currently not in stock.
<br/>
They have been ordered however please note some parts can take a few weeks to arrive.
<br/>
Should you have any questions or require further assistance, please reply to this email and we will respond as soon as we can.