<?php echo $user_ref; ?>has requested resubmitting an expired or declined PE.
<br/>
Reseller:<?php echo t('@resid @resname', array(
  '@resid' => $reseller_id,
  '@resname' => $reseller_name
)); ?><br/>
PE Name:<?php echo $pe_name; ?><br/>
PE Number:<?php echo $pe_number; ?><br/>
Date and Time:<?php echo $date; ?><br/>
Please contact<?php echo $user_ref; ?>on<?php echo $email . $phone; ?>to discuss this resubmission further.
<br/>
Thank you.