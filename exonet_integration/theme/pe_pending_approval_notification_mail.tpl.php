Hello<?php echo $user_reference; ?>,<br/>
<br/>
This email is to confirm that <?php echo $reference; ?> has been submitted<?php echo $brand_reference; ?> for processing.
We will notify you once a response has been received, however this can take between 3-5 working days.
<br/>
<br/>
Should have you any questions or require further assistance, please reply to this email and we will respond as soon as we can.