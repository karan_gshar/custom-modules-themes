Hello<?php echo $user_reference; ?>,<br/>
<br/>
This email is to confirm that <?php echo $reference; ?> has now expired. You can still resubmit this PE for approval again.
<br/>
<br/>
Should have you any questions or require further assistance, please reply to this email and we will respond as soon as we can.