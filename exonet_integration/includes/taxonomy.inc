<?php

// Main callback
// Sorting out fields and passing all prepared data to term creation/updation function`
function ei_update_category($category_data, $vocabulary) {
  $parent_data = NULL;

  if (isset($category_data['groupname'])) {
    $extra = array();

    if (isset($category_data['seqno'])) {
      //$extra['weight'] 		= $category_data['seqno'];

      if (isset($category_data['sortorder'])) {
        $extra['field_category_sort_order'] = array(LANGUAGE_NONE => array(0 => array('value' => $category_data['sortorder'])));
      }

      $extra['old_id'] = $category_data['seqno'];
    }

    $extra['description'] = isset($category_data['description']) ? check_markup($category_data['description'], 'filtered_html') : '';
    $extra['format'] = 'filtered_html';

    // If has parent set up, it's on sublevel (root level is level 1) and parent id != current id, so it does not reference itself
    $ids_are_set = isset($category_data['parentid']) && isset($category_data['levelid']);
    $not_master_category = $category_data['levelid'] > 1;
    $not_referencing_itself = !isset($category_data['seqno']) || $category_data['seqno'] != $category_data['parentid'];
    if ($ids_are_set && $not_master_category && $not_referencing_itself) {
      $extra['parent'] = (integer) trim(ei_check_term($category_data['parentid'], $vocabulary, 'check parents'));
      // Parent might be not found because it will be added later
      // So we need to create reference to parent and child for second loop
      if (!$extra['parent']) {
        $parent_data['parent_id'] = $category_data['parentid'];
        unset($extra['parent']);
      }
    }

    //watchdog('exo_integration: term creation',  $category_data['groupname'] , null, WATCHDOG_NOTICE);

    $term_data = ei_get_tid($category_data['groupname'], $vocabulary, $extra);

    if (!$term_data['status']) {
      $parent_data = NULL;
      watchdog('exo_integration: Can\'t save category', '<pre>' . print_r($category_data, 1) . '</pre>', NULL, WATCHDOG_WARNING);
    }
    else {
      if (isset($parent_data)) {
        $parent_data['children_id'] = $term_data['tid'];
      }
//			watchdog('exo_integration: Term saved', 'tid: ' . $term_data['tid'], null, WATCHDOG_NOTICE);
    }
  }
  else {
    watchdog('exo_integration: Unknown category name (groupname)', '<pre>' . print_r($category_data, 1) . '</pre>', NULL, WATCHDOG_WARNING);
  }
  return $parent_data;
}


// Second loop
// Just in case if we have terms with parent reference and can't find existing parent during elements loop (means childern was created earlier than parent)
// So, when all categories created try to find parent again.
function ei_update_term_parents($tid, $p_tid, $vocabulary) {
  if ($p_tid) {
    $p_new_tid = (integer) trim(ei_check_term($p_tid, $vocabulary, 'update parents'));
    if (!$p_new_tid) {
      watchdog('exo_integration: Need to create parent relationship', 'Term current id: ' . $tid . ' <br/>Parent old id: ' . $p_tid, NULL, WATCHDOG_WARNING);
    }
    else {
      $term = taxonomy_term_load($tid);
      $term->parent = $p_new_tid;
      taxonomy_term_save($term);
    }
  }
}


// Check if term exist
// If exist replace data with newly passed
// If does not exist - create new term
// Returns array with 2 arguments - tid of created/updated term and status if term was created or updated
function ei_get_tid($term_name = NULL, $vocab_name = NULL, $extra_fields = array()) {
  $term_id = 0;
  $action_status = 0;

  $term_name = trim($term_name);
  $vocab_name = trim($vocab_name);

  if (isset($term_name) && !empty($term_name) && isset($vocab_name) && !empty($vocab_name)) {
    $has_old_id = isset($extra_fields['old_id']);

    if ($has_old_id) {
      $tid = ei_check_term($extra_fields['old_id'], $vocab_name, 'create term');
      if ($tid) {
        $term = taxonomy_term_load($tid);
      }
    }

    if (!isset($term)) {
      //Create new term
      $vocabulary = taxonomy_vocabulary_machine_name_load($vocab_name);
      $term = (object) array(
        'vid' => $vocabulary->vid
      );

      if (isset($extra_fields['old_id'])) {
        $term->field_old_id[LANGUAGE_NONE] = array(
          array(
            'value' => $extra_fields['old_id']
          )
        );
      }
    }

    if (isset($extra_fields['old_id'])) {
      unset($extra_fields['old_id']);
    }

    $term->name = $term_name;
    if (!empty($extra_fields)) {
      foreach ($extra_fields as $key => $data) {
        $term->{$key} = $data;
      }
    }

    $action_status = taxonomy_term_save($term);
    $term_id = $term->tid;
  }

  return array('status' => $action_status, 'tid' => $term_id);
}


// Get new term id based on old id
function ei_check_term($seqno, $vocabulary, $event = NULL) {
  $params = array(
    ':type' => 'taxonomy_term',
    ':bundle' => trim($vocabulary),
    ':id' => trim($seqno)
  );
  $tid = db_query('SELECT entity_id FROM {field_data_field_old_id} WHERE entity_type=:type AND bundle=:bundle AND field_old_id_value=:id', $params)->fetchField();

  if (!$tid && $event != 'create term') {
    $data = array(
      '!seqno' => trim($seqno),
      '!event' => (string) $event,
    );
    watchdog('exo_integration: term check', 'Term id not found for seqno: !seqno <br />Event: !event', $data, WATCHDOG_WARNING);
  }

  return $tid;
}


// Delete term from vocabulary
function ei_delete_term($old_tid, $vocabulary) {
  if ($tid = ei_check_term($old_tid, $vocabulary, 'term delete')) {
    ei_remove_term($tid, 'field_categories');
  }
}


function ei_remove_term($tid, $field = NULL, $nid = NULL, $delete_term = TRUE) {
  $query = db_delete('taxonomy_index')->condition('tid', $tid);
  if ($nid) {
    $query->condition('nid', $nid);
  }
  $query->execute();


  if ($field && db_table_exists('field_data_' . $field)) {
    $query = db_delete('field_data_' . $field)->condition($field . '_tid', $tid);
    if ($nid) {
      $query->condition('entity_id', $nid);
    }
    $query->execute();
  }

  if ($field && db_table_exists('field_revision_' . $field)) {
    $query = db_delete('field_revision_' . $field)->condition($field . '_tid', $tid);
    if ($nid) {
      $query->condition('entity_id', $nid);
    }
    $query->execute();
  }

  if ($delete_term) {
    taxonomy_term_delete($tid);
  }
}


function ei_check_and_remove_term($tid, $field_name, $remove_term = TRUE) {
  if (!taxonomy_term_load($tid)) {
    ei_remove_term($tid, $field_name, NULL, $remove_term);
    return TRUE;
  }
  return FALSE;
}