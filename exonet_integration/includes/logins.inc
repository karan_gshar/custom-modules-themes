<?php
/**
 * Updates logins or creates it if not exists.
 * @param  array $login_data contain data to populate login account. Fields "uid" and "usertype" are mandatory minimum.
 */
function ei_update_logins($login_data) {
  if (isset($login_data['uid'])) {
    $exo_uid = $login_data['uid'];

    $uid_result = ei_check_login($exo_uid);

    if (is_object($uid_result)) {
      $account = $uid_result;
      $is_new_account = FALSE;

      if (!isset($uid_result->uid)) {
        $is_new_account = TRUE;
        $account->field_exo_uid['und'][0]['value'] = $exo_uid;
      }

      ei_update_login_fields($account, $login_data);

      if ($full_account = user_save($account)) {
        /** Been suspended for now.
         * if ($is_new_account) {
         * module_load_include('module', 'user');
         * _user_mail_notify('register_pending_approval', $full_account);
         * }
         **/
        syslog(LOG_NOTICE, 'exo_integration: User created/updated with exo uid: ' . $exo_uid . ' Uid: ' . $full_account->uid . ' Reference: sektor');
      }
      else {
        watchdog('exo_integration: User has not been saved', 'Exo uid: ' . $exo_uid . ' Account: <pre>' . print_r($account, 1) . '</pre>', NULL, WATCHDOG_ERROR);
      }
    }
    else {
      watchdog('exo_integration: Login update', 'Can\'t update login. Multiple logins found with same uid: <pre>' . print_r($login_data, 1) . '</pre>', NULL, WATCHDOG_ERROR);
    }
  }
  else {
    if (!isset($login_data['uid'])) {
      watchdog('exo_integration: Login creation', 'No login uid set up: <pre>' . print_r($login_data, 1) . '</pre>', NULL, WATCHDOG_ERROR);
    }
  }
}


function ei_check_login($exo_uid) {
  $uid_ckeck = db_select('field_data_field_exo_uid', 'exo_uid');
  $uid_ckeck->fields('exo_uid', array('entity_id'));
  $uid_ckeck->condition('exo_uid.field_exo_uid_value', trim($exo_uid));
  $uid_result = $uid_ckeck->execute()->fetchAll();

  if (count($uid_result) == 1) {
    $result = user_load(array_shift($uid_result)->entity_id);
  }
  else {
    if (count($uid_result) == 0) {
      $result = new stdClass();
    }
    else {
      $result = $uid_result;
      watchdog('exo_integration: Multiple accounts found', 'Same exo_uid: !exo_uid', array('!exo_uid' => '<pre>' . print_r($exo_uid, 1) . '</pre>'), WATCHDOG_ERROR);
    }
  }

  return $result;
}


function ei_update_login_fields($account, $data) {
  global $website_ref;

  if (!isset($account->uid)) {
    $account->is_new = TRUE;
  }
  $account->timezone = date_default_timezone(FALSE);

  $data['loginid'] = isset($data['loginid']) ? trim($data['loginid']) : FALSE;

  if (isset($data['usertype']) && is_numeric($data['usertype']) && $data['usertype'] == 1) {
    if (strtolower($website_ref) == 'my') {
      $suffix = 'my.sektor.co';
    }
    elseif (strtolower($website_ref) == 'th') {
      $suffix = 'th.sektor.co';
    }
    elseif (strtolower($website_ref) == 'au') {
      $suffix = 'sektor.com.au';
    }
    else {
      $suffix = 'sektor.co.nz';
    }

    if (!empty($data['loginid'])) {
      $tmp_name = $data['loginid'] . '@' . $suffix;
      $tmp_mail = $data['loginid'] . '@' . $suffix;

      // Name
      /**
       * if username is not used it's ok
       */
      if (!ei_used_as_username($tmp_name)) {
        $account->name = $tmp_name;
      }
      /**
       * But if it is used,
       *  then if it's new account, substitute name with fallback
       *   if existing just don't overwrite then.
       */
      else {
        if (isset($account->is_new)) {
          $account->name = ei_get_noname();
          watchdog('exo_integration: Login', 'Invalid username: !name <br/>Exo_uid: !uid', array(
            '!name' => $tmp_name,
            '!uid' => $data['uid']
          ), WATCHDOG_ERROR);
        }
      }

      // Mail
      /**
       * if email is not used it's ok
       */
      if (!ei_used_as_email($tmp_mail)) {
        $account->mail = $tmp_mail;
      }
      /**
       * But if it is used,
       *  then if it's new account, substitute email with fallback
       *   if existing just don't overwrite then.
       */
      else {
        if (isset($account->is_new)) {
          $account->mail = ei_get_nomail();
          watchdog('exo_integration: Login', 'Invalid email: !mail <br/>Exo_uid: !uid', array(
            '!mail' => $tmp_mail,
            '!uid' => $data['uid']
          ), WATCHDOG_ERROR);
        }
      }
    }
    else {
      $account->name = ei_get_noname();
      $account->mail = ei_get_nomail();
    }
  }
  else {
    // Name
    if (!empty($data['loginid'])) {
      /**
       * if username is not used it's ok
       */
      if (!ei_used_as_username($data['loginid'])) {
        $account->name = $data['loginid'];
      }
      /**
       * But if it is used,
       *  then if it's new account, substitute name with fallback
       *   if existing just don't overwrite then.
       */
      else {
        if (isset($account->is_new)) {
          $account->name = ei_get_noname();
          watchdog('exo_integration: Login', 'Invalid username: !name <br/>Exo_uid: !uid', array(
            '!name' => $data['loginid'],
            '!uid' => $data['uid']
          ), WATCHDOG_ERROR);
        }
      }
    }
    else {
      $account->name = ei_get_noname();
      watchdog('exo_integration: Login', 'Invalid username: !name <br/>Exo_uid: !uid', array(
        '!name' => $data['loginid'],
        '!uid' => $data['uid']
      ), WATCHDOG_CRITICAL);
    }

    // Mail
    if (!empty($data['loginid']) && valid_email_address($data['loginid'])) {
      /**
       * if email is not used it's ok
       */
      if (!ei_used_as_email($data['loginid'])) {
        $account->mail = $data['loginid'];
      }
      /**
       * But if it is used,
       *  then if it's new account, substitute email with fallback
       *   if existing just don't overwrite then.
       */
      else {
        if (isset($account->is_new)) {
          $account->mail = ei_get_nomail();
          watchdog('exo_integration: Login', 'Invalid email: !mail <br/>Exo_uid: !uid', array(
            '!mail' => $data['loginid'],
            '!uid' => $data['uid']
          ), WATCHDOG_CRITICAL);
        }
      }
    }
    else {
      $account->mail = ei_get_nomail();
      // Don't show errors for transitional and parent logins
      if ($data['usertype'] != 1 && $data['usertype'] != 3) {
        watchdog('exo_integration: Login', 'Invalid email: !mail <br/>Exo_uid: !uid', array(
          '!mail' => $data['loginid'],
          '!uid' => $data['uid']
        ), WATCHDOG_CRITICAL);
      }
    }
  }

// Password
  if (isset($account->is_new) && $account->is_new) {
    if (isset($data['password']) && strlen($data['password']) > 0) {
      $account->pass = $data['password'];
    }
    else {
      require_once './includes/password.inc';
      $account->pass = user_hash_password(user_password());
    }
  }

// Status
  if (isset($data['isactive']) && is_numeric($data['isactive'])) {
    $account->status = (bool) $data['isactive'];
  }
  else {
    $account->status = 1;
  }

// Roles
  $acc_roles = array();

  // Preserve particular roles from being overwritten
  if (isset($account->roles) && !empty($account->roles)) {
    $roles_to_check = array(
      19 => 'Web Admin',
      20 => 'Masquerade',
      22 => 'PER Stock',
      23 => 'SYD & PER Stock'
    );

    foreach ($roles_to_check as $rid => $title) {
      if (in_array($rid, array_keys($account->roles))) {
        $acc_roles[$rid] = $title;
      }
    }
  }

  if (isset($data['accesslevel']) && is_numeric($data['accesslevel'])) {
    switch ($data['accesslevel']) {
      case '1':
        // Sales only
        $acc_roles[14] = 'User Access Level 1';
        break;
      case '2':
        // Account Only
        $acc_roles[15] = 'User Access Level 2';
        break;
      case '3':
        // Service only
        $acc_roles[16] = 'User Access Level 3';
        break;
      case '4':
        // Sales & Accounts
        $acc_roles[18] = 'User Access Level 4';
        break;
      case '5':
        // Admin
        $acc_roles[17] = 'User Access Level 5';
        break;
      case '6':
        // New Sales Role
        $acc_roles[21] = 'User Access Level 6';
        break;
    }
  }
  $acc_roles[DRUPAL_AUTHENTICATED_RID] = 'Authenticated user';

  $account->roles = $acc_roles;

// Account No
  if (isset($data['accountno']) && is_numeric($data['accountno'])) {
    $account->field_account_no[LANGUAGE_NONE][0]['value'] = $data['accountno'];
  }
  else {
    $account->field_account_no[LANGUAGE_NONE][0]['value'] = '-';
  }

// Domain Code
  if (isset($data['caliborsubdomain'])) {
    $account->field_domain_code[LANGUAGE_NONE][0]['value'] = check_plain($data['caliborsubdomain']);
  }
  else {
    $account->field_domain_code[LANGUAGE_NONE][0]['value'] = '';
  }

// Calibor Reseller
  if (isset($data['caliborreseller']) && is_numeric($data['accountno'])) {
    $account->field_calibor_reseller[LANGUAGE_NONE][0]['value'] = filter_var($data['caliborreseller'], FILTER_SANITIZE_NUMBER_INT);
  }
  else {
    $account->field_calibor_reseller[LANGUAGE_NONE][0]['value'] = 0;
  }

  /*
  // Calibor Admin
  if (isset($data['caliboradmin']) && is_numeric($data['accountno'])) {
    $account->field_calibor_admin[LANGUAGE_NONE][0]['value'] = filter_var($data['caliboradmin'], FILTER_SANITIZE_NUMBER_INT);
  }
  else {
    $account->field_calibor_admin[LANGUAGE_NONE][0]['value'] = 0;
  }
  */
}

function ei_get_noname() {
  $name_ckeck = db_select('users', 'u');
  $name_ckeck->addField('u', 'name');
  $name_ckeck->condition('u.name', 'no_name_%', 'LIKE');
  $name_result = $name_ckeck->execute()->fetchAll();

  $max_index = 0;
  if (!empty($name_result)) {
    foreach ($name_result as $name) {
      $components = explode('_', $name->name);
      if (isset($components[2]) && is_numeric($components[2]) && $components[2] >= $max_index) {
        $max_index = ($components[2] + 1);
      }
    }
  }

  return 'no_name_' . $max_index;
}

function ei_get_nomail() {
  $mail_ckeck = db_select('users', 'u');
  $mail_ckeck->addField('u', 'mail');
  $mail_ckeck->condition('u.mail', 'no_name_%@%', 'LIKE');
  $mail_result = $mail_ckeck->execute()->fetchAll();

  $max_index = 0;
  if (!empty($mail_result)) {
    foreach ($mail_result as $mail) {
      $components = explode('@', $mail->mail);
      $components = explode('_', $components[0]);
      if (isset($components[2]) && is_numeric($components[2]) && $components[2] >= $max_index) {
        $max_index = ($components[2] + 1);
      }
    }
  }

  return 'no_name_' . $max_index . '@example.com';
}

function ei_delete_login($exo_uid) {
  $account = ei_check_login($exo_uid);

  if (is_object($account)) {
    if (isset($account->uid)) {
      user_delete($account->uid);
      syslog(LOG_NOTICE, 'exo_integration: Account deleted with exo uid: ' . $exo_uid . ' Reference: sektor');
    }
    else {
      watchdog('exo_integration: Account delete', 'No result found for exo uid: ' . $exo_uid . '. Already deleted?', NULL, WATCHDOG_NOTICE);
    }
  }
}


/**
 * Checks whether string has been used as user name before or not.
 */
function ei_used_as_username($string) {
  return db_query('SELECT COUNT(uid) FROM users WHERE name=:name', array(':name' => $string))->fetchField();
}


/**
 * Checks whether string has been used as email before or not.
 */
function ei_used_as_email($string) {
  return db_query('SELECT COUNT(uid) FROM users WHERE mail=:mail', array(':mail' => $string))->fetchField();
}