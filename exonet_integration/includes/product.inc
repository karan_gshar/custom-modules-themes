<?php
/**
 * Updates product or creates it if not exists.
 * @param  array $product_data contain data to populate product node. Field "stockcode" is mandatory minimum.
 * @return array if any related products passed, then returns associative list of related products stockcodes with referenced product stockcode as root key
 */
function ei_update_product(array $product_data) {
  $related_products = array();
  $to_log = '<pre>' . print_r($product_data, 1) . '</pre>';

  if (isset($product_data['stockcode'])) {
    $stockcode = trim($product_data['stockcode']);

    $product = ei_check_product($stockcode);

    if (!isset($product->nid)) {
      $product->field_stock_code[LANGUAGE_NONE][0]['value'] = $stockcode;
      $product->field_stock_code[LANGUAGE_NONE][0]['safe_value'] = $stockcode;
      $product->field_stock_code[LANGUAGE_NONE][0]['format'] = NULL;
    }

    $related_products = ei_update_product_fields($product, $product_data);

    if (!variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT)) {
      // Turn off automatic path generation
      $product->path['pathauto'] = FALSE;
      $product->pathauto_perform_alias = FALSE;
    }

    node_save($product);

    if (!variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT)) {
      ei_update_product_alias($product, $product_data, FALSE);
    }
    ei_update_product_rating($product, $product_data);
    ei_update_product_meta($product, $product_data);

    syslog(LOG_NOTICE, 'exo_integration: Product created with stockcode: ' . $stockcode . ' Nid: ' . $product->nid . ' Reference: sektor');
  }
  else {
    watchdog('exo_integration: Product creation', 'No product stockcode set up: !data', array('!data' => $to_log), WATCHDOG_ERROR);
  }

  return $related_products;
}


/**
 * Get product based on stockcode
 * @param $stockcode product identifier in exo system
 * @return object|array return depends on:<br/>
 *  1) object if product found<br/>
 *  2) empty object if no product found<br/>
 *  3) array of nids if multiple products found
 */
function ei_check_product($stockcode) {
  $params = array(
    ':type' => 'node',
    ':bundle' => 'product',
    ':value' => trim($stockcode),
  );
  $nid = db_query('SELECT entity_id FROM {field_data_field_stock_code} WHERE entity_type=:type AND bundle=:bundle AND field_stock_code_value=:value', $params)->fetchField();

  if ($nid) {
    $result = node_load($nid);
  }
  else {
    $result = (object) array(
      'type' => 'product',
    );
    node_object_prepare($result);
  }

  return $result;
}


/**
 * Updates product ct fields. Everything except content type system fields and product stockcode.
 * @param object $product content type object to perform action on.
 * @param array $data data to populate object fields. Only particular fiedls would be selected.
 * @return array stockcodes of related products. Empty if no related products exists.
 */
function ei_update_product_fields($product, array $data) {
  module_load_include('inc', 'exo_integration', 'includes/defaults');
  module_load_include('inc', 'exo_integration', 'includes/support_functions');

  $product->uid = CREATOR_UID;
  $product->language = LANGUAGE_NONE;

// Status
  if (isset($data['isactive']) && ($data['isactive'] == 0 || $data['isactive'] == 1)) {
    $product->status = (int) $data['isactive'];
  }
  else {
    $product->status = 1;
  }

// Promoted
  if (isset($data['homepage']) && ($data['homepage'] == 0 || $data['homepage'] == 1)) {
    $product->promote = (int) $data['homepage'];
  }
  else {
    $product->promote = 0;
  }

// Featured
  if (isset($data['featured']) && ($data['featured'] == 0 || $data['featured'] == 1)) {
    $product->field_featured[LANGUAGE_NONE][0]['value'] = (int) $data['featured'];
  }
  else {
    $product->field_featured[LANGUAGE_NONE][0]['value'] = 0;
  }

// In Stock
  if (isset($data['instock']) && ($data['instock'] == 0 || $data['instock'] == 1)) {
    $product->field_in_stock[LANGUAGE_NONE][0]['value'] = (int) $data['instock'];
  }
  else {
    $product->field_in_stock[LANGUAGE_NONE][0]['value'] = 0;
  }

  // Sell price
  if (isset($data['sellprice1']) && !empty($data['sellprice1'])) {
    $product->field_sell_price[LANGUAGE_NONE][0]['value'] = (float) $data['sellprice1'];
  }
  else {
    $product->field_sell_price[LANGUAGE_NONE][0]['value'] = 0.00;
  }

// Core Product
  if (isset($data['core']) && ($data['core'] == 0 || $data['core'] == 1)) {
    $product->field_core_product[LANGUAGE_NONE][0]['value'] = (int) $data['core'];
  }
  else {
    $product->field_core_product[LANGUAGE_NONE][0]['value'] = 0;
  }

// Freight Free
  if (isset($data['freightfree']) && ($data['freightfree'] == 0 || $data['freightfree'] == 1)) {
    $product->field_freight_free[LANGUAGE_NONE][0]['value'] = (int) $data['freightfree'];
  }
  else {
    $product->field_freight_free[LANGUAGE_NONE][0]['value'] = 0;
  }

// Account Specific
  if (isset($data['accountspecific']) && ($data['accountspecific'] == 0 || $data['accountspecific'] == 1)) {
    $product->field_is_account_specific[LANGUAGE_NONE][0]['value'] = (int) $data['accountspecific'];
  }
  else {
    $product->field_is_account_specific[LANGUAGE_NONE][0]['value'] = 0;
  }

// Product Name
  if (isset($data['description']) && !empty($data['description'])) {
    $product->title = substr($data['description'], 0, 255);
  }
  else {
    if (isset($data['h1tag']) && !empty($data['h1tag'])) {
      $product->title = substr($data['h1tag'], 0, 255);
    }
    else {
      $product->title = substr($data['stockcode'], 0, 255);
    }
  }

  if (isset($data['friendlycaption']) && $data['friendlycaption']) {
    $product->field_friendly_caption[LANGUAGE_NONE][0]['value'] = substr($data['friendlycaption'], 0, 1023);
  }
  else {
    $product->field_friendly_caption[LANGUAGE_NONE][0]['value'] = '';
  }

// Description
  $desc_value = array();

  // So if h1tag went to title, then need to save description as well
  if (isset($data['description']) && !empty($data['description']) &&
    (isset($data['h1tag']) && !empty($data['h1tag']))
  ) {
    $desc_value[] = html_entity_decode($data['description'], ENT_QUOTES);
  }

  if (isset($data['saleshtml']) && !empty($data['saleshtml'])) {
    $desc_value[] = html_entity_decode($data['saleshtml'], ENT_QUOTES);
  }

  if (!empty($desc_value)) {
    $str_desc_value = implode('<br/>', $desc_value);
    $product->body[LANGUAGE_NONE][0]['value'] = $str_desc_value;
    $product->body[LANGUAGE_NONE][0]['safe_value'] = $str_desc_value;
    $product->body[LANGUAGE_NONE][0]['format'] = 'full_html';
  }
  else {
    unset($product->body[LANGUAGE_NONE]);
//    $product->body[LANGUAGE_NONE][0]['value']       = '';
//    $product->body[LANGUAGE_NONE][0]['safe_value']  = '';
//    $product->body[LANGUAGE_NONE][0]['format']      = 'full_html';
  }

// Weight
  if (isset($data['weight']) && !empty($data['weight']) && is_numeric($data['weight'])) {
    $product->field_weight[LANGUAGE_NONE][0]['value'] = number_format($data['weight'], 2, '.', '');
  }
  else {
    $product->field_weight[LANGUAGE_NONE][0]['value'] = 0.00;
  }

// Cubic
  if (isset($data['cubic']) && !empty($data['cubic']) && is_numeric($data['cubic'])) {
    $product->field_cubic[LANGUAGE_NONE][0]['value'] = number_format($data['cubic'], 10, '.', '');
  }
  else {
    $product->field_cubic[LANGUAGE_NONE][0]['value'] = 0.0000000000;
  }

// Warranty
  if (isset($data['warrantydescription']) && !empty($data['warrantydescription'])) {
    $product->field_warranty[LANGUAGE_NONE][0]['value'] = substr(html_entity_decode($data['warrantydescription'], ENT_QUOTES), 0, 255);
    $product->field_warranty[LANGUAGE_NONE][0]['safe_value'] = substr(html_entity_decode($data['warrantydescription'], ENT_QUOTES), 0, 255);
    $product->field_warranty[LANGUAGE_NONE][0]['format'] = 'full_html';
  }
  else {
    unset($product->field_warranty[LANGUAGE_NONE]);
//    $product->field_warranty[LANGUAGE_NONE][0]['value']       = '';
//    $product->field_warranty[LANGUAGE_NONE][0]['safe_value']  = '';
//    $product->field_warranty[LANGUAGE_NONE][0]['format']      = 'full_html';
  }

// Sort Order
  if (isset($data['sortorder']) && !empty($data['sortorder']) && is_numeric($data['sortorder'])) {
    $product->field_sort_order[LANGUAGE_NONE][0]['value'] = $data['sortorder'];
  }
  else {
    $product->field_sort_order[LANGUAGE_NONE][0]['value'] = 0;
  }

// Stock Unit
  if (isset($data['stockunit']) && !empty($data['stockunit']) && is_numeric($data['stockunit'])) {
    $product->field_stock_unit[LANGUAGE_NONE][0]['value'] = $data['stockunit'];
  }
  else {
    $product->field_stock_unit[LANGUAGE_NONE][0]['value'] = 0;
  }

// Calibor Item
  if (isset($data['caliboritem']) && ($data['caliboritem'] == 0 || $data['caliboritem'] == 1)) {
    $product->field_calibor_item[LANGUAGE_NONE][0]['value'] = $data['caliboritem'];
  }
  else {
    $product->field_calibor_item[LANGUAGE_NONE][0]['value'] = 0;
  }

// Mancode
  if (isset($data['mancode']) && !empty($data['mancode'])) {
    $product->field_mancode[LANGUAGE_NONE][0]['value'] = substr(html_entity_decode($data['mancode'], ENT_QUOTES), 0, 255);
    $product->field_mancode[LANGUAGE_NONE][0]['safe_value'] = substr(html_entity_decode($data['mancode'], ENT_QUOTES), 0, 255);
    $product->field_mancode[LANGUAGE_NONE][0]['format'] = NULL;
  }
  else {
    unset($product->field_mancode[LANGUAGE_NONE]);
//    $product->field_mancode[LANGUAGE_NONE][0]['value']       = '';
//    $product->field_mancode[LANGUAGE_NONE][0]['safe_value']  = '';
//    $product->field_mancode[LANGUAGE_NONE][0]['format']      = null;
  }

// Classification
  if (isset($data['classification']) && !empty($data['classification']) && is_numeric($data['classification'])) {
    $product->field_classification[LANGUAGE_NONE][0]['value'] = (int) $data['classification'];
  }
  else {
    $product->field_classification[LANGUAGE_NONE][0]['value'] = 0;
  }

// Minimum order quantity
  if (isset($data['moq']) && !empty($data['moq']) && is_numeric($data['moq'])) {
    $product->field_moq[LANGUAGE_NONE][0]['value'] = (int) $data['moq'];
  }
  else {
    $product->field_moq[LANGUAGE_NONE][0]['value'] = 0;
  }

// Staging
  if (isset($data['staging']) && ($data['staging'] == 0 || $data['staging'] == 1)) {
    $product->field_staging[LANGUAGE_NONE][0]['value'] = (int) $data['staging'];
  }
  else {
    $product->field_staging[LANGUAGE_NONE][0]['value'] = 0;
  }

// Rank
  if (isset($data['webrank']) && !empty($data['webrank']) && is_numeric($data['webrank'])) {
    $product->field_rank[LANGUAGE_NONE][0]['value'] = (int) $data['webrank'];
    if ((int) $data['webrank'] == 2147483647) {
      $product->field_has_rank[LANGUAGE_NONE][0]['value'] = 0;
    }
    else {
      $product->field_has_rank[LANGUAGE_NONE][0]['value'] = 1;
    }
  }
  else {
    if (isset($data['rank']) && !empty($data['rank']) && is_numeric($data['rank'])) {
      $product->field_rank[LANGUAGE_NONE][0]['value'] = (int) $data['rank'];
      if ((int) $data['rank'] == -1) {
        $product->field_has_rank[LANGUAGE_NONE][0]['value'] = 0;
      }
      else {
        $product->field_has_rank[LANGUAGE_NONE][0]['value'] = 1;
      }
    }
    else {
      $product->field_rank[LANGUAGE_NONE][0]['value'] = -1;
      $product->field_has_rank[LANGUAGE_NONE][0]['value'] = 0;
    }
  }

// Allow PE
  if (isset($data['allowpe']) && ($data['allowpe'] == 0 || $data['allowpe'] == 1)) {
    $product->field_allow_pe[LANGUAGE_NONE][0]['value'] = (int) $data['allowpe'];
  }
  else {
    $product->field_allow_pe[LANGUAGE_NONE][0]['value'] = 0;
  }


// Mega Drop Down
  if (isset($data['megadropdown']) && ($data['megadropdown'] == 0 || $data['megadropdown'] == 1)) {
    $product->field_mega_drop_down[LANGUAGE_NONE][0]['value'] = (int) $data['megadropdown'];
  }
  else {
    $product->field_mega_drop_down[LANGUAGE_NONE][0]['value'] = 0;
  }

// Out Of stock
  if (isset($data['outofstock']) && !empty($data['outofstock'])) {
    $product->field_out_of_stock[LANGUAGE_NONE][0]['value'] = substr(html_entity_decode($data['outofstock'], ENT_QUOTES), 0, 255);
    $product->field_out_of_stock[LANGUAGE_NONE][0]['safe_value'] = substr(html_entity_decode($data['outofstock'], ENT_QUOTES), 0, 255);
    $product->field_out_of_stock[LANGUAGE_NONE][0]['format'] = NULL;
  }
  else {
    unset($product->field_out_of_stock[LANGUAGE_NONE]);
//    $product->field_out_of_stock[LANGUAGE_NONE][0]['value']       = '';
//    $product->field_out_of_stock[LANGUAGE_NONE][0]['safe_value']  = '';
//    $product->field_out_of_stock[LANGUAGE_NONE][0]['format']      = null;
  }

// Due Date
  if (isset($data['duedate'])) {
    $data['duedate'] = trim($data['duedate']);
    if (!empty($data['duedate'])) {
      $date = date('Y-m-d H:i:s', strtotime($data['duedate']));
      if (preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/is', $date)) {
        $product->field_due_date[LANGUAGE_NONE][0]['value'] = $date;
        $product->field_due_date[LANGUAGE_NONE][0]['timezone'] = date_default_timezone();
        $product->field_due_date[LANGUAGE_NONE][0]['timezone_db'] = date_default_timezone();
        $product->field_due_date[LANGUAGE_NONE][0]['date_type'] = 'datetime';
      }
    }
  }
  else {
    unset($product->field_due_date[LANGUAGE_NONE]);
//    $product->field_due_date[LANGUAGE_NONE][0]['value'] = '0000-00-00 00:00:00';
//    $product->field_due_date[LANGUAGE_NONE][0]['timezone'] = date_default_timezone();
//    $product->field_due_date[LANGUAGE_NONE][0]['timezone_db'] = date_default_timezone();
//    $product->field_due_date[LANGUAGE_NONE][0]['date_type'] = 'datetime';
  }

// Location
  if (isset($data['location']) && !empty($data['location'])) {
    $product->field_location[LANGUAGE_NONE][0]['value'] = $data['location'];
    $product->field_location[LANGUAGE_NONE][0]['format'] = NULL;
    $product->field_location[LANGUAGE_NONE][0]['safe_value'] = $data['location'];
  }
  else {
    unset($product->field_location[LANGUAGE_NONE]);
//    $product->field_location[LANGUAGE_NONE][0]['value'] = '';
//    $product->field_location[LANGUAGE_NONE][0]['format'] = null;
//    $product->field_location[LANGUAGE_NONE][0]['safe_value'] = '';
  }

// Incoming
  if (isset($data['incoming']) && !empty($data['incoming'])) {
    $product->field_incoming[LANGUAGE_NONE][0]['value'] = $data['incoming'];
    $product->field_incoming[LANGUAGE_NONE][0]['format'] = NULL;
    $product->field_incoming[LANGUAGE_NONE][0]['safe_value'] = $data['incoming'];
  }
  else {
    unset($product->field_incoming[LANGUAGE_NONE]);
//    $product->field_incoming[LANGUAGE_NONE][0]['value'] = '';
//    $product->field_incoming[LANGUAGE_NONE][0]['format'] = null;
//    $product->field_incoming[LANGUAGE_NONE][0]['safe_value'] = '';
  }

// Product Drivers
  if (isset($data['productdrivers']) && !empty($data['productdrivers'])) {
    $value = strip_tags(html_entity_decode($data['productdrivers']), '<a>');
    $product->field_product_drivers[LANGUAGE_NONE][0]['value'] = $value;
    $product->field_product_drivers[LANGUAGE_NONE][0]['format'] = 'filtered_html';
    $product->field_product_drivers[LANGUAGE_NONE][0]['safe_value'] = $value;
  }
  else {
    unset($product->field_product_drivers[LANGUAGE_NONE]);
//    $product->field_product_drivers[LANGUAGE_NONE][0]['value'] = '';
//    $product->field_product_drivers[LANGUAGE_NONE][0]['format'] = null;
//    $product->field_product_drivers[LANGUAGE_NONE][0]['safe_value'] = '';
  }

// Related Products
  $related_products = array();
  if (isset($data['relatedproducts']) && !empty($data['relatedproducts'])) {
    $related_stockcodes = explode(',', $data['relatedproducts']);
    if (!empty($related_stockcodes)) {
      foreach ($related_stockcodes as $related_stockcode) {
        if (isset($related_stockcode) && !empty($related_stockcode)) {
          $related_products[] = $related_stockcode;
        }
      }
    }
  }

// Brand
  if (isset($product->field_brand[LANGUAGE_NONE]) && !empty($product->field_brand[LANGUAGE_NONE])) {
    module_load_include('inc', 'exo_integration', 'includes/taxonomy');
    foreach ($product->field_brand[LANGUAGE_NONE] as $brand_index => $brand) {
      if (ei_check_and_remove_term($brand['tid'], 'field_brand', FALSE)) {
        $product->field_brand[LANGUAGE_NONE][$brand_index];
      }
    }
  }
  if (isset($data['brandseqno']) && !empty($data['brandseqno'])) {
    if (!empty($product->field_brand[LANGUAGE_NONE])) {
      module_load_include('inc', 'exo_integration', 'includes/taxonomy');
      foreach ($product->field_brand[LANGUAGE_NONE] as $brand) {
        ei_remove_term($brand['tid'], 'field_brand', $product->nid, FALSE);
      }
    }
    unset($product->field_brand[LANGUAGE_NONE]);
    module_load_include('inc', 'exo_integration', 'includes/brands');

    $seqno_result = ei_check_brand($data['brandseqno']);

    if (is_object($seqno_result)) {
      if (isset($seqno_result->nid)) {
        if (isset($seqno_result->field_brand_reference[LANGUAGE_NONE][0]['tid'])) {
          $product->field_brand[LANGUAGE_NONE][0]['tid'] = $seqno_result->field_brand_reference[LANGUAGE_NONE][0]['tid'];
        }
        else {
          watchdog('exo_integration: Product update', 'Can\'t update brand. No taxonomy reference found for brand with seqno: ' . $data['brandseqno'], NULL, WATCHDOG_ERROR);
        }
      }
      else {
        watchdog('exo_integration: Product update', 'Can\'t update brand. No brand found for seqno: ' . $data['brandseqno'] . ' Attempting to create one.', NULL, WATCHDOG_NOTICE);
        if (isset($data['brand']) && !empty($data['brand'])) {
          // Do I need to create new brand if current does not exist?
          $new_brand_data = array(
            'seqno' => $data['brandseqno'],
            'name' => $data['brand']
          );
          ei_update_brand($new_brand_data);
        }
        else {
          watchdog('exo_integration: Product update', 'Can\'t create brand. No valid brand name found: <pre>' . print_r($data, 1) . '</pre>', NULL, WATCHDOG_ERROR);
        }
      }
    }
    else {
      watchdog('exo_integration: Product update', 'Can\'t update brand. Multiple brands found with same seqno: ' . $data['brandseqno'], NULL, WATCHDOG_ERROR);
    }
  }
  elseif (isset($data['brand']) && !empty($data['brand'])) {
    if (!empty($product->field_brand[LANGUAGE_NONE])) {
      module_load_include('inc', 'exo_integration', 'includes/taxonomy');
      foreach ($product->field_brand[LANGUAGE_NONE] as $brand) {
        ei_remove_term($brand['tid'], 'field_brand', $product->nid, FALSE);
      }
    }
    unset($product->field_brand[LANGUAGE_NONE]);

    $return_tid = NULL;
    $current_term = taxonomy_get_term_by_name($data['brand'], 'brands');
    if (!empty($current_term)) {
      $tmp_term = array_shift($current_term);
      $return_tid = $tmp_term->tid;
    }
    else {
      module_load_include('inc', 'exo_integration', 'includes/brands');
      $vocabulary = taxonomy_vocabulary_machine_name_load('brands');

      $term = (object) array(
        'vid' => $vocabulary->vid,
        'name' => $data['brand']
      );

      taxonomy_term_save($term);
      $return_tid = $term->tid;

      $new_brand_data = array('seqno' => 0, 'name' => $data['brand']);
      ei_update_brand($new_brand_data);
    }

    if ($return_tid) {
      $product->field_brand[LANGUAGE_NONE][0]['tid'] = $return_tid;
    }
  }

// Product Tags
  if (isset($product->field_producttags[LANGUAGE_NONE]) && !empty($product->field_producttags[LANGUAGE_NONE])) {
    module_load_include('inc', 'exo_integration', 'includes/taxonomy');
    foreach ($product->field_producttags[LANGUAGE_NONE] as $tag_index => $tag) {
      if (ei_check_and_remove_term($tag['tid'], 'field_producttags', FALSE)) {
        unset($product->field_producttags[LANGUAGE_NONE][$tag_index]);
      }
    }
  }

  if (isset($data['producttags'])) {
    if (!empty($product->field_producttags[LANGUAGE_NONE])) {
      module_load_include('inc', 'exo_integration', 'includes/taxonomy');
      foreach ($product->field_producttags[LANGUAGE_NONE] as $tag) {
        ei_remove_term($tag['tid'], 'field_producttags', $product->nid, FALSE);
      }
    }
    unset($product->field_producttags[LANGUAGE_NONE]);
    $tags = explode(',', $data['producttags']);
    if (!empty($tags)) {
      foreach ($tags as $tag) {
        if ($tag != NULL && strlen($tag) > 0) {
          $tid = ei_get_term_id('tags', html_entity_decode($tag, ENT_QUOTES));
          $product->field_producttags[LANGUAGE_NONE][]['tid'] = $tid;
        }
      }
    }
  }


// Video 1
  // Notice - I'm not deleting files, referenced from these fields
  unset($product->field_video[LANGUAGE_NONE]);
  if (isset($data['video1token']) && !empty($data['video1token'])) {
    $video = ei_get_video($data['video1token']);
    if ($video) {
      $product->field_video[LANGUAGE_NONE][] = $video;
    }
  }

// Video 2
  if (isset($data['video2token']) && !empty($data['video2token'])) {
    $video = ei_get_video($data['video2token']);
    if ($video) {
      $product->field_video[LANGUAGE_NONE][] = $video;
    }
  }

// Specific Accounts
  if (isset($data['specificaccounts']) && !empty($data['specificaccounts'])) {
    $accs = explode(',', $data['specificaccounts']);
    if (!empty($accs)) {
      $product->field_accounts[LANGUAGE_NONE] = array();
      foreach ($accs as $acc) {
        $product->field_accounts[LANGUAGE_NONE][] = array(
          'value' => trim($acc),
        );
      }
    }
    else {
      $product->field_accounts = NULL;
    }
  }
  else {
    $product->field_accounts = NULL;
  }

// Images
// Pdfs
  $images = array();
  $pdfs = array();
  /*
  if(is_array($data))
    foreach($data as $key => $data_value){
      if(preg_match('/image(.*)/is', $key) && $data[$key] != null && strlen($data[$key]) > 0){
        $images[] = $data_value;
      }elseif(preg_match('/pdf(.*)/is', $key) && $data[$key] != null && strlen($data[$key]) > 0){
        $pdfs[] = $data_value;
      }
    }
    */
  if (isset($data['image1']) && !empty($data['image1'])) {
    $images[] = $data['image1'];
  }
  if (isset($data['image2']) && !empty($data['image2'])) {
    $images[] = $data['image2'];
  }
  if (isset($data['image3']) && !empty($data['image3'])) {
    $images[] = $data['image3'];
  }

  if (isset($data['pdf1']) && !empty($data['pdf1'])) {
    $pdfs[] = $data['pdf1'];
  }
  if (isset($data['pdf2']) && !empty($data['pdf2'])) {
    $pdfs[] = $data['pdf2'];
  }
  if (isset($data['pdf3']) && !empty($data['pdf3'])) {
    $pdfs[] = $data['pdf3'];
  }
  if (isset($data['pdf4']) && !empty($data['pdf4'])) {
    $pdfs[] = $data['pdf4'];
  }


  if (!empty($images)) {
    $clean = FALSE;
    foreach ($images as $filename) {
      // Do Image stuff
      $filename_original = $filename;
      $folder_original = 'product/original_images';

      $filename_destination = $filename;
      $folder_destination = 'product/images';

      $img = ei_save_single_file(CREATOR_UID, $filename_original, $folder_original, $filename_destination, $folder_destination);
      if ($img) {
        if (!$clean && isset($product->field_main_image[LANGUAGE_NONE])) {
          if (!empty($product->field_main_image[LANGUAGE_NONE])) {
            foreach ($product->field_main_image[LANGUAGE_NONE] as $index => $image) {
              $file = file_load($image['fid']);
              file_delete($file);
              unset($product->field_main_image[LANGUAGE_NONE][$index]);
            }
          }

          $product->field_main_image[LANGUAGE_NONE] = array();
          $clean = TRUE;
        }

        $product->field_main_image[LANGUAGE_NONE][] = array(
          'fid' => $img->fid,
          'uid' => $img->uid,
          'filename' => $img->filename,
          'uri' => $img->uri,
          'filemime' => $img->filemime,
          'filesize' => $img->filesize,
          'status' => $img->status,
          'display' => $img->status,
          'type' => 'image'
        );

        _ei_flush_image_cache('product', $folder_destination, $filename_destination);
      }
    }
  }
  else {
    if (isset($product->field_main_image[LANGUAGE_NONE])) {
      if (!empty($product->field_main_image[LANGUAGE_NONE])) {
        foreach ($product->field_main_image[LANGUAGE_NONE] as $index => $image) {
          $file = file_load($image['fid']);
          file_delete($file);
          unset($product->field_main_image[LANGUAGE_NONE][$index]);
        }

        $product->field_main_image[LANGUAGE_NONE] = array();
      }

      if (empty($product->field_main_image[LANGUAGE_NONE])) {
        unset($product->field_main_image[LANGUAGE_NONE]);
      }
    }
  }

  if (!empty($pdfs)) {
    $clean = FALSE;

    foreach ($pdfs as $filename) {
      // Do PDF stuff
      $filename_original = $filename;
      $folder_original = 'product/original_pdfs';

      $filename_destination = $filename;
      $folder_destination = 'product/pdfs';

      $pdf = ei_save_single_file(CREATOR_UID, $filename_original, $folder_original, $filename_destination, $folder_destination);
      if ($pdf) {
        if (!$clean && isset($product->field_pdf[LANGUAGE_NONE])) {
          if (!empty($product->field_pdf[LANGUAGE_NONE])) {
            foreach ($product->field_pdf[LANGUAGE_NONE] as $index => $pdf_ref) {
              $file = file_load($pdf_ref['fid']);
              file_delete($file);
              unset($product->field_pdf[LANGUAGE_NONE][$index]);
            }
          }

          $product->field_pdf[LANGUAGE_NONE] = array();
          $clean = TRUE;
        }

        $product->field_pdf[LANGUAGE_NONE][] = array(
          'fid' => $pdf->fid,
          'uid' => $pdf->uid,
          'filename' => $pdf->filename,
          'uri' => $pdf->uri,
          'filemime' => $pdf->filemime,
          'filesize' => $pdf->filesize,
          'status' => $pdf->status,
          'display' => $pdf->status,
          'type' => 'default'
        );
      }
    }
  }
  else {
    if (isset($product->field_pdf[LANGUAGE_NONE])) {
      if (!empty($product->field_pdf[LANGUAGE_NONE])) {
        foreach ($product->field_pdf[LANGUAGE_NONE] as $index => $pdf_ref) {
          $file = file_load($pdf_ref['fid']);
          file_delete($file);
          unset($product->field_pdf[LANGUAGE_NONE][$index]);
        }

        $product->field_pdf[LANGUAGE_NONE] = array();
      }

      if (empty($product->field_pdf[LANGUAGE_NONE])) {
        unset($product->field_pdf[LANGUAGE_NONE]);
      }
    }
  }

  return $related_products;
}


/**
 * Update references between "parent" product and products defined as related. Only if both products exists. Otherwise ignored.
 * @param string $parent_stockcode original product stockcode.
 * @param array $related_products array of stockcodes for related products.
 */
function ei_update_related_products($parent_stockcode, array $related_products) {
  if (isset($parent_stockcode) && !empty($related_products)) {
    $params = array(
      ':stockcode' => $parent_stockcode
    );

    $nid = db_query('SELECT entity_id FROM {field_data_field_stock_code} WHERE field_stock_code_value=:stockcode', $params)->fetchField();

    if ($product = node_load($nid)) {
      $set_stockcodes = array();
      $set_data = array();

      foreach ($related_products as $related_product_stockcode) {
        $params = array(
          ':stockcode' => $related_product_stockcode
        );
        $rel_nid = db_query('SELECT entity_id FROM {field_data_field_stock_code} WHERE field_stock_code_value=:stockcode', $params)->fetchField();

        if ($rel_nid) {
          $set_stockcodes[] = $related_product_stockcode;

          $set_data[] = array(
            'target_id' => $rel_nid,
            'access' => TRUE
          );
        }
      }

      $product->field_related_products = array(LANGUAGE_NONE => $set_data);

      if (!variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT)) {
        // Turn off automatic path generation
        $product->path['pathauto'] = FALSE;
        $product->pathauto_perform_alias = FALSE;
      }

      node_save($product);

      $params = array(
        '!stockcode' => $parent_stockcode,
        '!related' => implode(', ', $set_stockcodes),
      );
      watchdog('exo_integration: Related products', 'Updated: !stockcode, set related products: !related', $params, WATCHDOG_NOTICE);
    }
    else {
      watchdog('exo_integration: Related products', 'No result found for stockcode: @stcode', array('@stcode' => $parent_stockcode), WATCHDOG_NOTICE);
    }
  }
  else {
    watchdog('exo_integration: Related products', 'Can\'t assign products. Parent stockcode: ' . $parent_stockcode . ' <br/>Related products: <pre>' . print_r($related_products, 1) . '</pre>', NULL, WATCHDOG_ERROR);
  }
}


/**
 * Delete product by stockcode.
 * @param string $stockcode unique identifier in exo system.
 */
function ei_delete_product($stockcode) {
  module_load_include('module', 'simplemeta');
  module_load_include('inc', 'exo_integration', 'includes/placements');

  $product = ei_check_product($stockcode);

  if (isset($product->nid)) {
    $language = variable_get('simplemeta_language_enable', FALSE) ? $product->language : '';
    $simplemeta = simplemeta_meta_load_by_path('node/' . $product->nid, $language);
    if ($simplemeta) {
      simplemeta_meta_delete($simplemeta->sid);
    }

    ei_delete_all_placement($stockcode);

    node_delete($product->nid);
    syslog(LOG_NOTICE, 'exo_integration: Product deleted with stockcode: ' . $stockcode . ' Reference: sektor');
  }
  else {
    watchdog('exo_integration: Product delete', 'No result found for stockcode: ' . $stockcode . '. Already deleted?', NULL, WATCHDOG_NOTICE);
  }
}


/**
 * Get video from youtube, based on video id.
 * @param string $video_id video identifier on youtube
 * @return object|false return saved object or false in case of invalid video id, inaccessible video, deleted video or eny errors.
 */
function ei_get_video($video_id) {
  module_load_include('inc', 'media_youtube', 'includes/MediaInternetYouTubeHandler.inc');

  try {
    if (MediaInternetYouTubeHandler::validId($video_id)) {
      $obj = new MediaInternetYouTubeHandler('http://www.youtube.com/v/' . $video_id);
    }
    else {
      $obj = FALSE;
    }
  } catch (MediaInternetNoHandlerException $e) {
    watchdog('exo_integration: Youtube videos', 'Error: ' . $e->getMessage() . ' <br/>Video id: ' . $video_id, NULL, WATCHDOG_ERROR);
    $obj = FALSE;
  } catch (MediaInternetValidationException $e) {
    watchdog('exo_integration: Youtube videos', 'Error: ' . $e->getMessage() . ' <br/>Video id: ' . $video_id, NULL, WATCHDOG_ERROR);
    $obj = FALSE;
  } catch (Exception $e) {
    watchdog('exo_integration: Youtube videos', 'Error: ' . $e->getMessage() . ' <br/>Video id: ' . $video_id, NULL, WATCHDOG_ERROR);
    $obj = FALSE;
  }

  if ($obj) {
    $file = $obj->getFileObject();
    $file->display = 1;
    file_save($file);
    return (array) $file;
  }
  else {
    return FALSE;
  }
}


/**
 * Updates rating for product. Uses "fivestar" module.
 * @param object $product which needs to be rated.
 * @param integer $rating between 0 and 5 inclusively.
 */
function ei_update_product_rating($product, array $data) {
  if (isset($data['rating']) && is_numeric($data['rating'])) {
    $rating = (int) $data['rating'];

    if ($rating >= 0 && $rating <= 5) {
      module_load_include('module', 'fivestar');
      $new_rate = 20 * $rating;
      _fivestar_cast_vote('node', $product->nid, $new_rate, 'rating', 27, TRUE);
    }
  }
}


/*
  Serialized $meta->data structure:
a:3:
{
    s:5:"title";
      s:50:"DIGIPOS RETAIL ACTIVE DC 1.8 2GB RAM 160 HDD NO OS";
    s:11:"description";
      s:149:"Now breakthrough technology from DigiPoS called the Retail Active, bridges the gap; delivering a designed-for-retail solution at a competitive price.";
    s:8:"keywords";
      s:63:"TAGS: POS, Retail, PC, Serial Ports, USB Ports, Small Footprint";
}
*/
/**
 * Updates meta data (Simple Meta module)
 * @param object $product which needs to have metadata saved.
 * @param array $data data to use for meta descriptions
 */
function ei_update_product_meta($product, array $data) {
  module_load_include('module', 'simplemeta');

  $language = variable_get('simplemeta_language_enable', FALSE) ? $product->language : '';
  $path = 'node/' . $product->nid;

  $meta = simplemeta_meta_load_by_path($path, $language);
  if (!$meta) {
    $meta = new stdClass();
  }

  $meta->data = array('title' => '', 'description' => '', 'keywords' => '');
  $meta->path = $path;
  $meta->language = $language;


// Title
  if (isset($data['titletag']) && $data['titletag'] != NULL && strlen($data['titletag']) > 0) {
    $meta->data['title'] = strip_tags($data['titletag']);
  }
  else {
    $meta->data['title'] = strip_tags($product->title);
  }

// Description
  if (isset($data['metadescription']) && $data['metadescription'] != NULL && strlen($data['metadescription']) > 0) {
    $meta->data['description'] = $data['metadescription'];
  }
  else {
    $meta->data['description'] = isset($product->body[LANGUAGE_NONE][0]['value']) ? strip_tags($product->body[LANGUAGE_NONE][0]['value']) : '';
  }

// Keywords
  if (isset($data['description']) && $data['description'] != NULL && strlen($data['description']) > 0) {
    $meta->data['keywords'] = strip_tags(str_replace(' ', ', ', $data['description']));
  }

  simplemeta_meta_save($meta);
}


/**
 * Set alias to whichever is passed from exo
 */
function ei_update_product_alias($product, array $data, $disable_pathauto_on_the_node = TRUE) {
  if ($product) {
    $source = 'node/' . $product->nid;

    if ($disable_pathauto_on_the_node) {
      // Turn off automatic path generation
      $product->path['pathauto'] = FALSE;
      $product->pathauto_perform_alias = FALSE;
      node_save($product);
    }

    if (isset($data['weburldesc']) && $trimmed = trim($data['weburldesc'])) {
      $path = path_load(array('source' => $source));

      if (!$path) {
        $path = array(
          'source' => $source,
        );
      }

      $path['alias'] = filter_var($trimmed, FILTER_SANITIZE_URL);

      path_save($path);
    }
    else {
      pathauto_node_delete($product);
    }
  }
}


/**
 * Prepare fields for _get_product_orphaned_ids()
 */
function _get_product_fields_to_check() {
  $fields = field_info_instances("node", "product");
  return array_keys($fields);
}


/**
 * Retrieves nids for products which has missing node.
 */
function _get_product_orphaned_ids($fields, $log = TRUE) {
  $all_nids = array();

  $current_product_nids = db_query('SELECT nid FROM {node} WHERE type=:type', array(':type' => 'product'))->fetchCol();

  $references = array();
  foreach ($fields as $field) {
    $table_name = 'field_data_' . $field;

    if (db_table_exists($table_name)) {
      $query = db_select($table_name, 't');
      $query->fields('t', array('entity_id'));
      $query->condition('entity_type', 'node');
      $query->condition('bundle', 'product');

      if (!empty($current_product_nids)) {
        $query->condition('entity_id', $current_product_nids, 'NOT IN');
      }

      $nids = $query->execute()->fetchCol();
      $all_nids = array_merge($all_nids, $nids);

      if (!empty($nids)) {
        $references[$table_name] = $nids;
      }
    }
  }

  if ($log) {
    watchdog('exo_integration: Orphaned data', 'Removing orphaned data: <br/>!data', array('!data' => '<pre>' . print_r($references, 1) . '</pre>'), WATCHDOG_INFO);
  }

  return array_unique($all_nids);
}


/**
 * Retrieves nids for products which has missing node.
 */
function _remove_orphaned_data($fields, $nids) {
  foreach ($fields as $field) {
    $table_name = 'field_data_' . $field;

    if (db_table_exists($table_name) && !empty($nids)) {
      $query = db_delete($table_name);
      $query->condition('entity_id', $nids, 'IN');
      $query->condition('entity_type', 'node');
      $query->condition('bundle', 'product');
      $query->execute();
      unset($query);
    }
  }
}
