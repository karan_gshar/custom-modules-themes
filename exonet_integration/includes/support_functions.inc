<?php

/**
 * Copying file from source to destination and saves it to file managed table.<br/>
 * Both locations should exist, be accessible and located withing drupal public folder.<br/>
 * If you need to copy from external source, use symlinks to create reference.
 * @param integer $file_uid file creator uid.
 * @param string $source_filename just filename of source file.
 * @param string $source_folder path to file inside of public folder. No trailing or leading slashes. If it has subfolders write them in ussual url way (like "folder/subfolder")
 * @param string $destination_filename filename of destination file. Just in case you want to rename it. Otherwise assign same value as source.
 * @param string $destination_folder destination folder for files withing public folder.
 * @param string $filemime if you know filemime or have some specific filemime for file
 * @param integer $method method of copying files (FILE_EXISTS_ERROR|FILE_EXISTS_RENAME|FILE_EXISTS_REPLACE). Default to FILE_EXISTS_REPLACE.
 * @param boolean $do_error_messages throw errors in watchdog or not.
 * @return object|null return file object or null if error occured.
 */
function ei_save_single_file($file_uid, $source_filename, $source_folder, $destination_filename, $destination_folder, $filemime = NULL, $method = FILE_EXISTS_REPLACE, $do_error_messages = TRUE) {
  $result_file = NULL;

  //drupal_set_message($file_uid . ' From: ' . $source_folder . '/' . $source_filename . ' To: ' . $destination_folder . '/' . $destination_filename . ' Filemime: ' . $filemime);
  $source_filename = substr($source_filename, (strlen($source_filename) - 255), 255);
  $source = file_build_uri($source_folder . '/' . $source_filename);
  $source_realpath = drupal_realpath('public://') . '/' . $source_folder . '/' . $source_filename;

  if (file_exists($source_realpath)) {
    $file_folder_url = file_build_uri($destination_folder);
    $check = file_prepare_directory($file_folder_url, FILE_CREATE_DIRECTORY);
    if (!$check) {
      $check = file_prepare_directory($file_folder_url, FILE_MODIFY_PERMISSIONS);
    }

    if ($check) {
      $destination_filename = substr($destination_filename, (strlen($destination_filename) - 255), 255);
      $destination = file_build_uri($destination_folder . '/' . $destination_filename);
      $destination_realpath = drupal_realpath('public://') . '/' . $destination_folder . '/' . $destination_filename;

      $source_file = (object) array(
        'filename' => $source_filename,
        'uid' => $file_uid,
        'status' => FILE_STATUS_PERMANENT,
        'uri' => $source_realpath,
        'filemime' => !isset($filemime) ? file_get_mimetype($source) : $filemime,
      );

      if (!file_exists($destination_realpath)) {
        $result_file = file_copy($source_file, $destination, $method);
      }
      else {
        $query = db_select('file_managed', 'f');
        $result = $query->fields('f')
          ->condition('f.uri', $destination)
          ->execute()
          ->fetchAssoc();

        if (isset($result['fid'])) {
          $result_file = file_load($result['fid']);
        }
        else {
          unlink($destination_realpath);
          $result_file = file_copy($source_file, $destination, $method);
        }
      }
    }
    else {
      if ($do_error_messages) {
        watchdog('exo_integration: File manipulations', 'Can\'t move file. Destination ' . $destination_folder . ' could not been created or has read-only permissions which can\'t be overwritten.', NULL, WATCHDOG_ERROR);
      }
    }
  }
  else {
    // Too many pdfs does not exist so it garbaging watchdog.
    //if($do_error_messages) watchdog('exo_integration: File manipulations', 'File does not exist: ' . $source, null, WATCHDOG_WARNING);
  }
  return $result_file;
}


function ei_save_file($filename, $folder) {
  $return = FALSE;
  /**
   * Because we are using symlinks, there are some issues related to this
   * Firstly, we can't use image styles as I understood, only original image reference.
   * Secondly, it will not delete file physically, because it says that file does not exist and unfortunately throw error in watchdog.
   * Thirdly, it doesn't want to get filesize on file save, so I've copied file_save logic and commented out unnecessary
   */
  $destination = drupal_realpath('public://') . '/' . $folder . '/' . $filename;

  if (file_exists($destination)) {
    $uri = 'public://' . $folder . '/' . $filename;
    $query = db_select('file_managed', 'files');
    $query->fields('files', array('fid'));
    $query->condition('files.uri', $uri);
    $result = $query->execute()->fetchAll();

    if (!empty($result)) {
      $return = file_load(array_shift($result)->fid);
    }
    else {
      $file = new stdCLass();
      $file->filename = $filename;
      $file->uid = CREATOR_UID;
      $file->status = FILE_STATUS_PERMANENT;
      $file->uri = $uri;
      $file->filemime = file_get_mimetype($destination);
      $file->filesize = filesize($destination);
      $file->timestamp = time();

      /** LOGIC copied from file_save() function in /include/file.inc **/
      /*
              $file->timestamp = REQUEST_TIME;
              $file->filesize = filesize($file->uri);

              // Load the stored entity, if any.
              if (!empty($file->fid) && !isset($file->original)) {
                $file->original = entity_load_unchanged('file', $file->fid);
              }
      */
      module_invoke_all('file_presave', $file);
      module_invoke_all('entity_presave', $file, 'file');

//        if (empty($file->fid)) {
      drupal_write_record('file_managed', $file);
      // Inform modules about the newly added file.
      module_invoke_all('file_insert', $file);
      module_invoke_all('entity_insert', $file, 'file');
      /*
              }
              else {
                drupal_write_record('file_managed', $file, 'fid');
                // Inform modules that the file has been updated.
                module_invoke_all('file_update', $file);
                module_invoke_all('entity_update', $file, 'file');
              }

              unset($file->original);
       */
      $return = $file;
    }
  }
  return $return;
}


function ei_get_term_id($vocab_name, $term_name) {
  $return_tid = NULL;
  if ($term_name != NULL && strlen(trim($term_name)) > 0 &&
    $vocab_name != NULL && strlen(trim($vocab_name)) > 0
  ) {
    //Check if term exist
    $current_term = taxonomy_get_term_by_name($term_name, $vocab_name);
    if (!empty($current_term)) {
      //If it is ... Just save tid
      $tmp_term = array_shift($current_term);
      $return_tid = $tmp_term->tid;
    }
    else {
      //Else create new term and save new tid
      $vocabulary = taxonomy_vocabulary_machine_name_load($vocab_name);

      $term = (object) array(
        'vid' => $vocabulary->vid,
        'name' => $term_name
      );

      taxonomy_term_save($term);
      $return_tid = $term->tid;
    }
  }
  return $return_tid;
}


function _ei_get_file_id_by_filename_url($uri) {
  return db_query('SELECT fid FROM {file_managed} WHERE uri LIKE :uri', array(':uri' => $uri))->fetchField();
}


function _ei_update_file($filename, $type) {
  switch ($type) {
    case 'product':
      if (substr($filename, strlen($filename) - 4, 4) == '.pdf') {
        $source_folder = 'product/original_pdfs';
        $destination_folder = 'product/pdfs';
        $operation = 'product pdf update';
      }
      else {
        $source_folder = 'product/original_images';
        $destination_folder = 'product/images';
        $operation = 'product image update';
      }
      break;
    case 'brand':
      $source_folder = 'original_brand_logos';
      $destination_folder = 'brands';
      $operation = 'brand logo update';
      break;
  }

  if (isset($source_folder) && isset($destination_folder)) {
    $source_realpath = drupal_realpath('public://') . '/' . $source_folder . '/' . $filename;
    $destination_realpath = drupal_realpath('public://') . '/' . $destination_folder . '/' . $filename;
    if (file_exists($source_realpath)) {
      if (file_exists($destination_realpath)) {
        if (copy($source_realpath, $destination_realpath)) {
          _ei_flush_image_cache($type, $destination_folder, $filename);
          watchdog('exo_integration: file update', 'Successfully updated a file: !filename. Operation: !operation.', array(
            '!filename' => $filename,
            '!operation' => $operation
          ), WATCHDOG_INFO);
        }
        else {
          watchdog('exo_integration: file update', 'File update operation failed, trying to remove and reupload file: !filename. Operation: !operation.', array(
            '!filename' => $filename,
            '!operation' => $operation
          ), WATCHDOG_DEBUG);
          if (unlink($destination_realpath)) {
            if (copy($source_realpath, $destination_realpath)) {
              _ei_flush_image_cache($type, $destination_folder, $filename);
              watchdog('exo_integration: file update', 'Successfully updated a file: !filename. Operation: !operation.', array(
                '!filename' => $filename,
                '!operation' => $operation
              ), WATCHDOG_INFO);
            }
            else {
              watchdog('exo_integration: file update', 'File reupload failed: !filename. Operation: !operation.', array(
                '!filename' => $filename,
                '!operation' => $operation
              ), WATCHDOG_CRITICAL);
            }
          }
          else {
            watchdog('exo_integration: file update', 'Cannot remove file from destination: !path. Operation: !operation.', array(
              '!path' => $destination_realpath,
              '!operation' => $operation
            ), WATCHDOG_ERROR);
          }
        }
      }
      else {
        // Do I need to trigger anything here?
        // For example when they upload new file, which does not have any product references yet
      }
    }
    else {
      watchdog('exo_integration: file update', 'File not found: !filename. Operation: !operation.', array(
        '!filename' => $filename,
        '!operation' => $operation
      ), WATCHDOG_ERROR);
    }
  }
  else {
    watchdog('exo_integration: file update', 'Not valid type: !type', array('!type' => $type), WATCHDOG_ERROR);
  }
}


function _ei_flush_image_cache($type, $folder, $filename) {
  switch ($type) {
    case 'product':
      $styles = array(
        '120x110', // Management Product List
//        'product-image', // Product node image
        'galleryformatter_slide', // Product node image
        'product-image-slider', // featured products
//        'product_featured_block', // Not sure
        'product_image_in_cart', // Cart page image styles
        'product_image_in_pe', // PE details page product image styles
        'product_list_image_thumbnail', // Products under categories
        'product_page_large_view', // Product page colorbox popup style
      );
      break;
    case 'brand':
      $styles = array(
        'brand_logo', // Brand logos at the bottom
//        'brand_logo_description_block', // Not sure
//        'brand_logo_featured_block', // Not sure
        'brand_logo_gallery', // Brand list page
        'brand_logo_product_page', // Product page
        'category_description_image_style', // Brand description block
        'featured_category_logo', // 'Categories Featured Product Display' view
      );

      break;
  }

  if (isset($styles) && !empty($styles)) {
    foreach ($styles as $style) {
      $file = drupal_realpath('public://') . '/styles/' . $style . '/public/' . $folder . '/' . $filename;
      if (file_exists($file)) {
        unlink($file);
      }
//      image_style_flush(image_style_load($style)); // just don't want to put overhead by flushing image styles for all products
    }
  }
}