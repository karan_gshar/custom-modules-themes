<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');

/**
 * Gets list of orders
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user.
 * @return array contain account info or empty if no result fetched
 */
function ei_get_orders_history($params = array()) {
  global $user;
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );

    if (isset($params['enduserid']) && !empty($params['enduserid'])) {
      $parameters[] = array(
        'variable' => 'enduserid',
        'value' => $params['enduserid']
      );
    }

    if (isset($params['sortby']) && isset($params['sortdir'])) {
      $parameters[] = array(
        'variable' => 'order',
        'value' => $params['sortby']
      );
      $parameters[] = array(
        'variable' => 'direction',
        'value' => $params['sortdir']
      );
    }

    if (isset($params['limit']) && isset($params['page'])) {
      $parameters[] = array('variable' => 'limit', 'value' => $params['limit']);
      $parameters[] = array(
        'variable' => 'offset',
        'value' => ($params['page'] - 1) * $params['limit']
      );
    }

    if (isset($params['loginuid']) && !empty($params['loginuid'])) {
      $parameters[] = array(
        'variable' => 'loginuid',
        'value' => $params['loginuid']
      );
    }

    if (isset($params['from']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['from'])) {
      $date_components = explode('/', $params['from']);
      $parameters[] = array(
        'variable' => 'minorderdate',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0]
      );
    }

    if (isset($params['to']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['to'])) {
      $date_components = explode('/', $params['to']);
      $parameters[] = array(
        'variable' => 'maxorderdate',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0]
      );
    }

    if (isset($params['customerorderno']) && !empty($params['customerorderno'])) {
      $parameters[] = array(
        'variable' => 'customerordernolike',
        'value' => '%' . $params['customerorderno'] . '%'
      );
    }

    if (isset($params['externalid']) && !empty($params['externalid'])) {
      $parameters[] = array(
        'variable' => 'externalid',
        'value' => $params['externalid']
      );
    }

    if (isset($params['status']) && !empty($params['status'])) {
      $parameters[] = array(
        'variable' => 'status',
        'value' => $params['status']
      );
    }

//    dpm ($parameters);

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    drupal_set_message('<pre>' . print_r($parameters,1). '</pre>');

    $total = isset($response['result']->ordersmeta->unlimtedcount) ? $response['result']->ordersmeta->unlimtedcount : 0;
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->orders) && count($response['result']->orders) > 0
    ) {
      foreach ($response['result']->orders as $order) {
        $items[] = (array) $order;
      }
    }
  }

//    drupal_set_message('<pre>' . print_r($total,1). '</pre>');
  return array($items, $total);
}


/**
 * General method to get orders from API
 */
function ei_get_orders($params = array()) {
  global $user;
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      global $user;
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $return = array(array(), 0);

  if ($accountno) {
    $params['accountno'] = $accountno;
    $return = ei_get_orders_non_acc_restricted($params);
  }

  return $return;
}


/**
 * As general orders retrieval method has restriction on account # to be mandatory
 *   I need method without restrictions.
 */
function ei_get_orders_non_acc_restricted($params = array()) {
  $items = array();
  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderList';
  $method = 'post';
  $parameters = array();

  if (isset($params['accountno']) && !empty($params['accountno'])) {
    $parameters[] = array(
      'variable' => 'accountno',
      'value' => $params['accountno']
    );
  }

  if (isset($params['enduserid']) && !empty($params['enduserid'])) {
    $parameters[] = array(
      'variable' => 'enduserid',
      'value' => $params['enduserid']
    );
  }

  if (isset($params['from']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['from'])) {
    $date_components = explode('/', $params['from']);
    $parameters[] = array(
      'variable' => 'minorderdate',
      'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0]
    );
  }
  if (isset($params['to']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['to'])) {
    $date_components = explode('/', $params['to']);
    $parameters[] = array(
      'variable' => 'maxorderdate',
      'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0] . ''
    );
  }

  if (isset($params['order_id']) && !empty($params['order_id'])) {
    $parameters[] = array(
      'variable' => 'orderid',
      'value' => $params['order_id']
    );
  }

  if (isset($params['orderready']) && !empty($params['orderready'])) {
    $parameters[] = array(
      'variable' => 'orderready',
      'value' => $params['orderready']
    );
  }

  if (isset($params['externalid']) && !empty($params['externalid'])) {
    $parameters[] = array(
      'variable' => 'externalid',
      'value' => $params['externalid']
    );
  }

  $response = ei_parse_url_request($server, $request, $method, $parameters);
  $total = isset($response['result']->ordersmeta->unlimtedcount) ? $response['result']->ordersmeta->unlimtedcount : 0;

  if (isset($response['status']) && $response['status'] == 'success' && $total != '0') {
    foreach ($response['result']->orders as $order) {
      $items[] = (array) $order;
    }
  }

  return array($items, $total);
}


function ei_get_products($params = array()) {
  $order_id = 0;
  if (isset($params['order_id'])) {
    $order_id = $params['order_id'];
  }


  $items = array();
  if ($order_id) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderEntryList';
    $method = 'post';
    $parameters = array(array('variable' => 'orderid', 'value' => $order_id));

    $response = ei_parse_url_request($server, $request, $method, $parameters);

    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->orderentries) && count($response['result']->orderentries) > 0
    ) {
      foreach ($response['result']->orderentries as $order) {
        $items[] = (array) $order;
      }
    }
  }

  return $items;
}

function ei_send_order($post_data) {
  $fields = array(
    'externalid',
    'isintegrated',
    'couriercompany',
    'trackingurl',
    'accountno',
    'loginuid',
    'isgstexempt',
    'orderdate',
    'ordertime',
    'deliveryadd1',
    'deliveryadd2',
    'deliveryadd3',
    'deliveryadd4',
    'deliveryadd5',
    'deliveryadd6',
    'deliverypostcode',
    'phone',
    'fax',
    'email',
    'entrycount',
    'itemcount',
    'subtotal',
    'freight',
    'gst',
    'total',
    'creditcardsurcharge',
    'ordersource',
    'totalweight',
    'totalcubic',
    'customerorderno',
    'deliveryinstructions',
    'contactname',
    'orderready',
    'paymentmethod',
    'paymentsuccessful',
    'dpskey',
    'status',
    'billingadd1',
    'billingadd2',
    'billingadd3',
    'billingadd4',
    'billingadd5',
    'billingadd6',
    'billingpostcode',
    'billingphone',
    'billingcontactname',
    'addressseqno',
    'partshipped',
    'staging',
    'staginginstructions',
    'source',
    'shippingzone',
    'shippingcity',
    'dispatchlocation',
    'dispatchbranchseqno',
    'deliverytype',
    'firstpereferenceno',
    'cartreference',
    'calibororder',
    'enduserid',
  );

  $data_to_send = array();
  foreach ($fields as $key) {
    if (isset($post_data[$key])) {
      $tmp['variable'] = $key;
      $tmp['value'] = $post_data[$key];
      $data_to_send[] = $tmp;
    }
  }
  if (!empty($data_to_send)) {

    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderInsert';
    $method = 'post';

    $response = ei_parse_url_request($server, $request, $method, $data_to_send);

    $order = array();
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->orders) && count($response['result']->orders) > 0
    ) {
      $order = (array) array_shift($response['result']->orders);
    }

    return isset($order['orderid']) ? $order['orderid'] : FALSE;
  }

  return FALSE;
}


function ei_send_order_items($post_data) {
  $fields = array(
    'orderid',
    'stockcode',
    'description',
    'position',
    'quantity',
    'peentryid',
    'unitcubic',
    'cubic',
    'unitweight',
    'weight',
    'unitprice',
    'price',
    'pereferenceno',
  );

  $data_to_send = array();
  foreach ($fields as $key) {
    if (isset($post_data[$key])) {
      $tmp['variable'] = $key;
      $tmp['value'] = $post_data[$key];
      $data_to_send[] = $tmp;
    }
  }

  if (!empty($data_to_send)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderEntryInsert';
    $method = 'post';

    ei_parse_url_request($server, $request, $method, $data_to_send);

    return TRUE;
  }

  return FALSE;
}


function ei_update_used_quantity($peentryid, $qty) {
  module_load_include('inc', 'exo_integration', 'includes/requests/pe_related');
  $peentry = ie_get_pe_entries(array('peentryid' => $peentryid));
  $new_qty = $qty;
  if ($peentry[1] && isset($peentry[0][0]['quantityused'])) {
    $new_qty += (int) $peentry[0][0]['quantityused'];
  }

  ei_update_pe_entries(array(
    'peentryid' => $peentryid,
    'quantityused' => $new_qty
  ), TRUE);
}


function ei_delete_order($orderid = NULL) {
  if (isset($orderid)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderDeleteComplete';
    $method = 'post';
    $parameters = array(array('variable' => 'orderid', 'value' => $orderid));

    ei_parse_url_request($server, $request, $method, $parameters);

    return TRUE;
  }

  return FALSE;
}


function ei_update_order($params = array()) {
  if (isset($params['orderid']) && !empty($params['orderid'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderUpdate';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'orderid',
        'value' => $params['orderid']
      )
    );

    if (isset($params['orderready'])) {
      $parameters[] = array(
        'variable' => 'orderready',
        'value' => $params['orderready']
      );
    }

    if (isset($params['orderdate'])) {
      $parameters[] = array(
        'variable' => 'orderdate',
        'value' => $params['orderdate']
      );
    }

    if (isset($params['ordertime'])) {
      $parameters[] = array(
        'variable' => 'ordertime',
        'value' => $params['ordertime']
      );
    }

    if (isset($params['dpskey'])) {
      $parameters[] = array(
        'variable' => 'dpskey',
        'value' => $params['dpskey']
      );
    }

    if (isset($params['paymentsuccessful'])) {
      $parameters[] = array(
        'variable' => 'paymentsuccessful',
        'value' => $params['paymentsuccessful']
      );
    }

    ei_parse_url_request($server, $request, $method, $parameters);

    return TRUE;
  }

  return FALSE;
}


function ei_get_back_order($params = array()) {
  global $user;
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      global $user;
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=backOrderList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );

    if (isset($params['sortby']) && isset($params['sortdir'])) {
      $parameters[] = array(
        'variable' => 'order',
        'value' => $params['sortby']
      );
      $parameters[] = array(
        'variable' => 'direction',
        'value' => $params['sortdir']
      );
    }

    if (isset($params['limit']) && isset($params['page'])) {
      $parameters[] = array('variable' => 'limit', 'value' => $params['limit']);
      $parameters[] = array(
        'variable' => 'offset',
        'value' => ($params['page'] - 1) * $params['limit']
      );
    }

    if (isset($params['seqno'])) {
      $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
    }

    if (isset($params['date'])) {
      $parameters[] = array('variable' => 'date', 'value' => $params['date']);
    }

    if (isset($params['filename'])) {
      $parameters[] = array(
        'variable' => 'filename',
        'value' => $params['filename']
      );
    }

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->backorders) ? count($response['result']->backorders) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->backorders as $backorder) {
        $items[] = (array) $backorder;
      }
    }
  }

  return array($items, $total);
}


function ei_get_order_creators($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderCreatorList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->logins) ? count($response['result']->logins) : 0;
    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->logins as $login) {
        $items[] = (array) $login;
      }
    }
  }

  return array($items, $total);
}


function ei_get_order_end_users($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderEndUserList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->endusers) ? count($response['result']->endusers) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->endusers as $endusers) {
        $items[] = $endusers->enduserid;
      }
    }
  }

  return array($items, $total);
}


function ei_get_order_statuses($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderStatusList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->statuses) ? count($response['result']->statuses) : 0;
    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->statuses as $status) {
        $items[] = $status->status;
      }
    }
  }

  return array($items, $total);

}

function ei_get_transaction($params = array()) {
  $items = array();
  $total = 0;

  if (isset($params['orderexternalid'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=orderTransactionList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'orderexternalid',
        'value' => $params['orderexternalid']
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->ordertransactions) ? count($response['result']->ordertransactions) : 0;
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->ordertransactions)
    ) {
      foreach ($response['result']->ordertransactions as $ordertransaction) {
        $items[] = (array) $ordertransaction;
      }
    }
  }

  return array($items, $total);
}


/**
 * Fetches courier tracking info from API
 */
function _ei_get_tracking_info($params = array()) {
  $items = array();

  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=trackingInfoList';
  $method = 'post';
  $parameters = array();

  if (isset($params['soid']) && is_numeric($params['soid'])) {
    $parameters[] = array(
      'variable' => 'soid',
      'value' => $params['soid']
    );
  }

  if (isset($params['changed'])) {
    $parameters[] = array(
      'variable' => 'changed',
      'value' => (bool) $params['changed']
    );
  }

  $response = ei_parse_url_request($server, $request, $method, $parameters);

  $total = isset($response['result']->trackinginfos) ? count($response['result']->trackinginfos) : 0;
  if (isset($response['status']) && $response['status'] == 'success' && $total) {
    foreach ($response['result']->trackinginfos as $trackinginfos) {
      $items[] = (array) $trackinginfos;
    }
  }

  return array($items, $total);
}


/**
 * Sets 'changed' flag to 0 in courier tracking info the API for specified seqno.
 */
function _ei_reset_tracking_info($seqno) {
  if (isset($seqno) && !empty($seqno)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=trackingInfoReset';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'seqno',
        'value' => $seqno
      )
    );

    ei_parse_url_request($server, $request, $method, $parameters);

    return TRUE;
  }

  return FALSE;
}