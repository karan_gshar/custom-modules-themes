<?php

module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');

function ei_check_peid($peid) {
  global $user;
//  $uid = 0;
//  if(isset($user->exo_uid)){
//    $uid = $user->exo_uid;
//  }else{
//    $account = user_load($user->uid);
//    if(isset($account->field_exo_uid['und'][0]['value']))
//      $uid = $account->field_exo_uid['und'][0]['value'];
//  }

//  $result = false;
//  if($uid){

  $account_no = 0;
  if (isset($user->account_no)) {
    $account_no = $user->account_no;
  }
  else {
    $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
    $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
  }

  $result = FALSE;
  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );
//    $parameters = array(array('variable' => 'loginuid',    'value' => $uid));
    $parameters[] = array('variable' => 'peid', 'value' => $peid);

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $result = isset($response['result']->pes[0]) ? (array) $response['result']->pes[0] : FALSE;
  }
  return $result;
}

function ei_check_peentryid($peentryid) {
  $entry_list = ie_get_pe_entries(array('peentryid' => $peentryid));
  if (isset($entry_list[0][0]['peid'])) {
    $list_check = ei_check_peid($entry_list[0][0]['peid']);
    if (
      isset($list_check['peid']) &&
      $list_check['peid'] &&
      (
        (
          isset($list_check['peready']) &&
          !$list_check['peready']
        ) ||
        $list_check['peready'] == NULL
      )
    ) {
      return TRUE;
    }
  }
  return FALSE;
}


function ei_remove_pe_list($peid) {
  global $user;
  $uid = 0;
  if (isset($user->exo_uid)) {
    $uid = $user->exo_uid;
  }
  else {
    $account = user_load($user->uid);
    if (isset($account->field_exo_uid['und'][0]['value'])) {
      $uid = $account->field_exo_uid['und'][0]['value'];
    }
  }

  $list_check = ei_check_peid($peid);
  if ($peid && $uid && isset($list_check['peready']) && !$list_check['peready']) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peDelete';
    $method = 'post';
    $parameters = array(array('variable' => 'peid', 'value' => $peid));

    ei_parse_url_request($server, $request, $method, $parameters);

    return TRUE;
  }
  return FALSE;
}


function ei_remove_pe_entries($peentryid) {
//    drupal_set_message('<pre>' . print_r($peentryid, 1) . '</pre>');
  if (ei_check_peentryid($peentryid)) {
    $entry = ie_get_pe_entries(array('peentryid' => $peentryid));
    $peid = $entry[0][0]['peid'];

    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peEntryDelete';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'peentryid',
        'value' => $peentryid
      )
    );

    ei_parse_url_request($server, $request, $method, $parameters);

    $entries = ie_get_pe_entries(array('peid' => $peid));
    $left_entries = array_pop($entries);
    ei_update_pe(array('peid' => $peid, 'entrycount' => $left_entries));
//    drupal_set_message('<pre>' . print_r($left_entries, 1) . '</pre>');

//    drupal_set_message('<pre>' . print_r($parameters, 1) . '</pre>');
    return TRUE;
  }
  return FALSE;
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_get_pe_list($params = array()) {
  global $user;

  $account_no = 0;
  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $items = array();
  $total = 0;
  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    if (isset($params['sortby']) && isset($params['sortdir'])) {
      $parameters[] = array(
        'variable' => 'order',
        'value' => $params['sortby']
      );
      $parameters[] = array(
        'variable' => 'direction',
        'value' => $params['sortdir']
      );
    }

    if (isset($params['limit']) && isset($params['page'])) {
      $parameters[] = array('variable' => 'limit', 'value' => $params['limit']);
      $parameters[] = array(
        'variable' => 'offset',
        'value' => ($params['page'] - 1) * $params['limit']
      );
    }

    if (isset($params['loginuid'])) {
      $parameters[] = array(
        'variable' => 'loginuid',
        'value' => $params['loginuid']
      );
    }

    if (isset($params['peid'])) {
      $parameters[] = array('variable' => 'peid', 'value' => $params['peid']);
    }

    if (isset($params['approved'])) {
      $parameters[] = array(
        'variable' => 'approved',
        'value' => $params['approved']
      );
    }

    if (isset($params['status']) && !empty($params['status'])) {
      $parameters[] = array(
        'variable' => 'status',
        'value' => $params['status']
      );
    }

    if (isset($params['type']) && !empty($params['type'])) {
      $parameters[] = array('variable' => 'type', 'value' => $params['type']);
    }

    if (isset($params['stockcode_search']) && !empty($params['stockcode_search'])) {
      $parameters[] = array(
        'variable' => 'keywords',
        'value' => $params['stockcode_search']
      );
    }

    if (isset($params['endusername']) && !empty($params['endusername'])) {
      $parameters[] = array(
        'variable' => 'clientname',
        'value' => $params['endusername']
      );
    }

    if (isset($params['penumber']) && !empty($params['penumber'])) {
      $parameters[] = array(
        'variable' => 'penumber',
        'value' => $params['penumber']
      );
    }

    if (isset($params['expire']) && !empty($params['expire'])) {
      $parameters[] = array(
        'variable' => 'maxdateexpires',
        'value' => $params['expire']
      );
      $parameters[] = array(
        'variable' => 'mindateexpires',
        'value' => date('Y-m-d')
      );
    }

    if (isset($params['from']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['from'])) {
      $date_components = explode('/', $params['from']);
      $parameters[] = array(
        'variable' => 'mindatecreated',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0]
      );
    }

    if (isset($params['to']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['to'])) {
      $date_components = explode('/', $params['to']);
      $parameters[] = array(
        'variable' => 'maxdatecreated',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0] . ''
      );
    }
//    dpm($parameters);
//    drupal_set_message('<pre>' . print_r($parameters, 1) . '</pre>');
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->pesmeta->unlimtedcount) ? $response['result']->pesmeta->unlimtedcount : 0;
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->pes) && count($response['result']->pes) > 0
    ) {
      foreach ($response['result']->pes as $pe_list) {
        $items[] = (array) $pe_list;
      }
    }
  }
  return array($items, $total);
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_get_brands_for_pe($params = array()) {
  $items = array();
  $total = 0;
  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=brandList';
  $method = 'post';

  if (isset($params['allowpe'])) {
    $parameters[] = array(
      'variable' => 'allowpe',
      'value' => $params['allowpe']
    );
  }

  if (isset($params['seqno'])) {
    $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
  }

  $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($parameters);

  $total = isset($response['result']->brands) ? count($response['result']->brands) : 0;

  if (isset($response['status']) && $response['status'] == 'success' && $total) {
    foreach ($response['result']->brands as $brand) {
      $items[$brand->seqno] = $brand->name;
    }
  }
  return array($items, $total);
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_get_brands($params = array()) {
  $items = array();
  $total = 0;
  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=brandList';
  $method = 'post';

  if (isset($params['allowpe'])) {
    $parameters[] = array(
      'variable' => 'allowpe',
      'value' => $params['allowpe']
    );
  }

  if (isset($params['seqno'])) {
    $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
  }

  $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($parameters);

  $total = isset($response['result']->brands) ? count($response['result']->brands) : 0;
  if (
    isset($response['status']) && $response['status'] == 'success' &&
    isset($response['result']->brands) && count($response['result']->brands) > 0
  ) {
    foreach ($response['result']->brands as $brand) {
      $items[] = (array) $brand;
    }
  }
  return array($items, $total);
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_create_new_pe($params = array()) {
  global $user;
  $exo_uid = 0;
  if (isset($params['exo_uid'])) {
    $exo_uid = $params['exo_uid'];
  }
  else {
    if (isset($user->exo_uid)) {
      $exo_uid = $user->exo_uid;
    }
    else {
      $account = user_load($user->uid);
      if (isset($account->field_exo_uid['und'][0]['value'])) {
        $exo_uid = $account->field_exo_uid['und'][0]['value'];
      }
    }
  }

  $account_no = 0;
  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  if ($exo_uid && $account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peInsert';
    $method = 'post';
    $parameters = array(
      array('variable' => 'loginuid', 'value' => $exo_uid),
      array('variable' => 'accountno', 'value' => $account_no),
      array('variable' => 'datecreated', 'value' => date('Y-m-d H:i:s')),
      array('variable' => 'status', 'value' => 'Not sent'),
      array('variable' => 'entrycount', 'value' => 0),
      array('variable' => 'peready', 'value' => 0),
      array('variable' => 'referenceno', 'value' => ''),
      array('variable' => 'conversionrate', 'value' => ''),
      array('variable' => 'approved', 'value' => 0),
      array('variable' => 'source', 'value' => 'web'),
      array('variable' => 'type', 'value' => 'General'),
      array('variable' => 'isintegrated', 'value' => 0),
    );

    if (isset($params['name'])) {
      $parameters[] = array('variable' => 'name', 'value' => $params['name']);
    }

    if (isset($params['brand'])) {
      $parameters[] = array(
        'variable' => 'brandseqno',
        'value' => $params['brand']
      );
    }


    $response = ei_parse_url_request($server, $request, $method, $parameters);
//      dpm($response);
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->pes[0]->peid)
    ) {
      return $response['result']->pes[0]->peid;
    }
  }
  else {
    drupal_set_message('You are not authorized for this action.', 'error');
  }
  return FALSE;
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_update_pe($params = array()) {
  if (isset($params['peid'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peUpdate';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'peid',
        'value' => $params['peid']
      )
    );

    if (isset($params['name'])) {
      $parameters[] = array('variable' => 'name', 'value' => $params['name']);
    }

    if (isset($params['brand'])) {
      $parameters[] = array(
        'variable' => 'brandseqno',
        'value' => $params['brand']
      );
    }

    if (isset($params['penumber'])) {
      $parameters[] = array(
        'variable' => 'penumber',
        'value' => $params['penumber']
      );
    }

    if (isset($params['status'])) {
      $parameters[] = array(
        'variable' => 'status',
        'value' => $params['status']
      );
    }

    if (isset($params['entrycount'])) {
      $parameters[] = array(
        'variable' => 'entrycount',
        'value' => $params['entrycount']
      );
    }

    if (isset($params['peready'])) {
      $parameters[] = array(
        'variable' => 'peready',
        'value' => $params['peready']
      );
    }

    if (isset($params['clientname'])) {
      $parameters[] = array(
        'variable' => 'clientname',
        'value' => $params['clientname']
      );
    }

    if (isset($params['referenceno'])) {
      $parameters[] = array(
        'variable' => 'referenceno',
        'value' => $params['referenceno']
      );
    }

    if (isset($params['conversionrate'])) {
      $parameters[] = array(
        'variable' => 'conversionrate',
        'value' => $params['conversionrate']
      );
    }

    if (isset($params['type'])) {
      $parameters[] = array('variable' => 'type', 'value' => $params['type']);
    }

    if (isset($params['isintegrated'])) {
      $parameters[] = array(
        'variable' => 'isintegrated',
        'value' => $params['isintegrated']
      );
    }
//    dpm($params);
//    dpm($parameters);
    ei_parse_url_request($server, $request, $method, $parameters);
    return TRUE;
  }
  return FALSE;
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_set_product_to_pe($params = array()) {
  global $user;
  $exo_uid = 0;
  if (isset($user->exo_uid)) {
    $exo_uid = $user->exo_uid;
  }
  else {
    if (isset($params['exo_uid'])) {
      $exo_uid = $params['exo_uid'];
    }
    else {
      $account = user_load($user->uid);
      if (isset($account->field_exo_uid['und'][0]['value'])) {
        $exo_uid = $account->field_exo_uid['und'][0]['value'];
      }
    }
  }

  $account_no = 0;
  if (isset($user->account_no)) {
    $account_no = $user->account_no;
  }
  else {
    if (isset($params['accountno'])) {
      $account_no = $params['accountno'];
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  if ($exo_uid && $account_no && isset($params['peid']) && isset($params['stockcode'])) {
    if (ei_check_peid($params['peid'])) {
      $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
      $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peEntryInsert';
      $method = 'post';
      $parameters = array(
        array('variable' => 'peid', 'value' => $params['peid']),
        array('variable' => 'stockcode', 'value' => $params['stockcode']),
        array(
          'variable' => 'approvedstockcode',
          'value' => $params['stockcode']
        ),
        array('variable' => 'approvedwithchanges', 'value' => 0),
        array('variable' => 'quantityused', 'value' => 0),
      );

      if (isset($params['quantity'])) {
        $parameters[] = array(
          'variable' => 'quantity',
          'value' => (int) round($params['quantity'], 0)
        );
        $parameters[] = array(
          'variable' => 'approvedquantity',
          'value' => (int) round($params['quantity'], 0)
        );
      }

      if (isset($params['listprice'])) {
        $parameters[] = array(
          'variable' => 'listprice',
          'value' => round($params['listprice']),
          2
        );
      }

      if (isset($params['discount'])) {
        $parameters[] = array(
          'variable' => 'discount',
          'value' => round($params['discount'], 0)
        );
      }

      if (isset($params['sellprice'])) {
        $parameters[] = array(
          'variable' => 'sellprice',
          'value' => round($params['sellprice']),
          2
        );
        $parameters[] = array(
          'variable' => 'approvedsellprice',
          'value' => round($params['sellprice']),
          2
        );
      }
      if (isset($params['margin'])) {
        $parameters[] = array(
          'variable' => 'margin',
          'value' => round($params['margin'])
        );
      }

      if (isset($params['buyprice'])) {
        $parameters[] = array(
          'variable' => 'buyprice',
          'value' => round($params['buyprice']),
          2
        );
        $parameters[] = array(
          'variable' => 'approvedbuyprice',
          'value' => round($params['buyprice']),
          2
        );
      }
      if (isset($params['position'])) {
        $parameters[] = array(
          'variable' => 'position',
          'value' => $params['position']
        );
      }

      if (isset($params['standardprice'])) {
        $parameters[] = array(
          'variable' => 'standardprice',
          'value' => round($params['standardprice']),
          2
        );
      }

      $response = ei_parse_url_request($server, $request, $method, $parameters);
//      dpm($response);

      if (
        isset($response['status']) && $response['status'] == 'success' &&
        isset($response['result']->peentries[0]->peentryid)
      ) {

        $list_entries = ie_get_pe_entries(array('peid' => $response['result']->peentries[0]->peid));
        ei_update_pe(array(
          'peid' => $response['result']->peentries[0]->peid,
          'entrycount' => $list_entries[1]
        ));

        return $response['result']->peentries[0]->peentryid;

      }
    }
    else {
      drupal_set_message('List is unaccessible. Try again later.', 'warning');
      return FALSE;
    }
  }
  return FALSE;
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @param  boolean optional parameter. Added just to skip validation for order confirmation, when needed to update used quantites.
 * @return array contain account info or empty if no result fetched
 */
function ei_update_pe_entries($params = array(), $no_check = FALSE) {
  if ((isset($params['peentryid']) && ei_check_peentryid($params['peentryid'])) || $no_check) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peEntryUpdate';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'peentryid',
        'value' => $params['peentryid']
      )
    );

    if (isset($params['quantity'])) {
      $parameters[] = array(
        'variable' => 'quantity',
        'value' => (int) round($params['quantity'], 0)
      );
    }

    if (isset($params['listprice'])) {
      $parameters[] = array(
        'variable' => 'listprice',
        'value' => number_format($params['listprice'], 2, '.', '')
      );
    }

    if (isset($params['discount'])) {
      $parameters[] = array(
        'variable' => 'discount',
        'value' => number_format($params['discount'], 2, '.', '')
      );
    }

    if (isset($params['sellprice']) && is_numeric($params['sellprice'])) {
      $parameters[] = array(
        'variable' => 'sellprice',
        'value' => number_format($params['sellprice'], 2, '.', '')
      );
    }

    if (isset($params['margin'])) {
      $parameters[] = array(
        'variable' => 'margin',
        'value' => number_format($params['margin'], 2, '.', '')
      );
    }

    if (isset($params['buyprice'])) {
      $parameters[] = array(
        'variable' => 'buyprice',
        'value' => number_format($params['buyprice'], 2, '.', '')
      );
    }

    if (isset($params['position'])) {
      $parameters[] = array(
        'variable' => 'position',
        'value' => $params['position']
      );
    }

    if (isset($params['quantityused'])) {
      $parameters[] = array(
        'variable' => 'quantityused',
        'value' => $params['quantityused']
      );
    }

    if (isset($params['standardprice'])) {
      $parameters[] = array(
        'variable' => 'standardprice',
        'value' => round($params['standardprice']),
        2
      );
    }
//    dpm($parameters);

    ei_parse_url_request($server, $request, $method, $parameters);

    return TRUE;
  }
  return FALSE;
}


function ie_get_pe_entries($params = array()) {
  $parameters = array();

  if (isset($params['peentryid'])) {
    $parameters[] = array(
      'variable' => 'peentryid',
      'value' => $params['peentryid']
    );
  }

  if (isset($params['peid'])) {
    $parameters[] = array('variable' => 'peid', 'value' => $params['peid']);
  }

  if (isset($params['stockcode'])) {
    $parameters[] = array(
      'variable' => 'stockcode',
      'value' => $params['stockcode']
    );
  }
//  dpm($parameters);

  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peEntryList';
  $method = 'post';

  $response = ei_parse_url_request($server, $request, $method, $parameters);
//  dpm($response);

  $items = array();
  $total = isset($response['result']->peentries) ? count($response['result']->peentries) : 0;

  if (isset($response['status']) && $response['status'] == 'success' && $total) {
    foreach ($response['result']->peentries as $peentry) {
      $items[] = (array) $peentry;
    }
  }
  return array($items, $total);
}


function ei_check_pe_with_changes_status($peid) {
  $peentries = ie_get_pe_entries(array('peid' => $peid));
  if ($peentries[1]) {
    foreach ($peentries[0] as $peentry) {
      if (isset($peentry['approvedwithchanges']) && $peentry['approvedwithchanges']) {
        return TRUE;
      }
    }
  }
  return FALSE;
}


/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, from, to, status, keyword
 * @return array contain account info or empty if no result fetched
 */
function ei_get_pe_statuses($params = array()) {
  global $user;
//  $exo_uid = 0;
//  if(isset($user->exo_uid)){
//    $exo_uid = $user->exo_uid;
//  }else{
//    if(isset($params['uid'])){
//      $exo_uid = $params['uid'];
//    }else{
//      $account = user_load($user->uid);
//      if(isset($account->field_exo_uid['und'][0]['value']))
//        $exo_uid = $account->field_exo_uid['und'][0]['value'];
//    }
//  }
//
//  $items = array();
//  $total = 0;
//  if($exo_uid){

  $account_no = 0;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;
  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peStatusList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );
//    $parameters = array(array('variable' => 'loginuid',    'value' => $exo_uid));

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->statuses) ? count($response['result']->statuses) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->statuses as $status) {
        $items[] = $status->status;
      }
    }
  }
  return array($items, $total);
}


function ei_get_pe_creators($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peCreatorList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

//    drupal_set_message('<pre>' . print_r($response, 1) . '</pre>');
    $total = isset($response['result']->logins) ? count($response['result']->logins) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->logins as $login) {
        $items[] = (array) $login;
      }
    }
  }
  return array($items, $total);
}


function ei_get_pe_types($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peTypeList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->types) ? count($response['result']->types) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->types as $type) {
        $items[] = $type->type;
      }
    }
  }
  return array($items, $total);
}


function ei_get_pe_stockcodes($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no && isset($params['stockcode'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peStockcodeList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );
    $parameters[] = array(
      'variable' => 'stockcode',
      'value' => $params['stockcode']
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->products) ? count($response['result']->products) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->products as $product) {
        $items[] = $product->stockcode;
      }
    }
  }
  return array($items, $total);
}


function ei_get_pe_mancodes($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;

  if ($account_no && isset($params['mancode'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peMancodeList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );
    $parameters[] = array(
      'variable' => 'mancode',
      'value' => $params['mancode']
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->products) ? count($response['result']->products) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->products as $product) {
        $items[] = $product->mancode;
      }
    }
  }
  return array($items, $total);
}


function ei_get_pe_end_users($params = array()) {
  global $user;

  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $items = array();
  $total = 0;
//  dpm($params);
  if ($account_no && isset($params['clientname'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=peClientNameList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );
    $parameters[] = array(
      'variable' => 'clientname',
      'value' => $params['clientname']
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);
    $total = isset($response['result']->clientnames) ? count($response['result']->clientnames) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->clientnames as $clientname) {
        $items[] = $clientname->clientname;
      }
    }
  }
  return array($items, $total);
}
