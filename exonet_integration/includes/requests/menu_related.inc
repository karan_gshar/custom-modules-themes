<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets price for products from exo system
 * @param  array Array of pararmetrs to pass. Have to pass 'nid' or 'stockcode' for product reference, may pass 'qty', otherwise falling back to 1.
 * @return string price rounded to 2 decimal digits or empty string if no result fetched
 */
function ei_get_alternative_categories($params = array()) {
  $alternatives = array();

  if (isset($params['catseqno'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=categoryAltChildrenList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'seqno',
        'value' => $params['catseqno']
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);

    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->altcategories) && !empty($response['result']->altcategories)
    ) {
      foreach ($response['result']->altcategories as $alt_category) {
        $alternatives[] = array(
          'catseqno' => $alt_category->groupno,
          'mastercatseqno' => $alt_category->mastercategory
        );
      }
    }
  }

  return $alternatives;
}