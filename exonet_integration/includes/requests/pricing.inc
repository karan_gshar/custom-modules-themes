<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets price for products from exo system
 * @param  array Array of pararmetrs to pass. Have to pass 'nid' or 'stockcode' for product reference, may pass 'qty', otherwise falling back to 1.
 * @return string price rounded to 2 decimal digits or empty string if no result fetched
 */
function ei_get_price($params = array()) {
  global $user;
  $output = '';

  if (($user->uid || isset($params['accountno'])) && (isset($params['nid']) || isset($params['stockcode']))) {

    $stockcode = '';
    if (isset($params['stockcode'])) {
      $stockcode = $params['stockcode'];
    }
    else {
      $product = node_load($params['nid']);
      $stockcode = isset($product->field_stock_code['und'][0]['value']) ? $product->field_stock_code['und'][0]['value'] : '';
    }

    if ($stockcode != '') {
      $accountno = 0;
      if (isset($params['accountno'])) {
        $accountno = $params['accountno'];
      }
      else {
        if (isset($user->account_no)) {
          $accountno = $user->account_no;
        }
        else {
          $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
          $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
        }
      }

      $quantity = isset($params['qty']) ? $params['qty'] : 1;
      $date = date('Y-m-d');

      if ($accountno) {
        $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
        if ($user->uid == 1) {
          $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=sellPriceTest';
        }
        else {
          $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=sellPrice';
        }

        $method = 'post';
        $parameters = array(
          array('variable' => 'accountno', 'value' => $accountno),
          array('variable' => 'stockcode', 'value' => $stockcode),
          array('variable' => 'quantity', 'value' => $quantity),
          array('variable' => 'date', 'value' => $date),
        );

        $cache_prices = variable_get('ei_cache_prices', FALSE);
        if ($cache_prices) {
          $data = &drupal_static('cached_prices');
        }
        else {
          $data = array();
        }

        if (isset($data[$accountno][$stockcode][$quantity][$date])) {
          $output = $data[$accountno][$stockcode][$quantity][$date];
        }
        else {
          $response = ei_parse_url_request($server, $request, $method, $parameters);

          if (isset($response['status']) && $response['status'] == 'success' &&
            isset($response['result']->sellprice) && is_numeric($response['result']->sellprice)
          ) {
            $output = number_format($response['result']->sellprice, 2, '.', '');

            if ($cache_prices) {
              $data[$accountno][$stockcode][$quantity][$date] = $output;
            }
          }
        }
      }
    }
    else {
      watchdog('exo_integration: get_price', 'Can\'t get stockcode. <br/><pre>' . print_r($params, 1) . '</pre>', NULL, WATCHDOG_WARNING);
    }
  }
  return $output;
}


function ei_get_list_price($params = array()) {
  $output = '';
  if ((user_is_logged_in() || isset($params['accountno'])) && (isset($params['nid']) || isset($params['stockcode']))) {
    global $user;

    $stockcode = '';
    if (isset($params['stockcode'])) {
      $stockcode = $params['stockcode'];
    }
    else {
      $product = node_load($params['nid']);
      $stockcode = isset($product->field_stock_code['und'][0]['value']) ? $product->field_stock_code['und'][0]['value'] : '';
    }

    if (!empty($stockcode)) {
      $accountno = 0;
      if (isset($params['accountno'])) {
        $accountno = $params['accountno'];
      }
      else {
        if (isset($user->account_no)) {
          $accountno = $user->account_no;
        }
        else {
          $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
          $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
        }
      }

      if ($accountno) {
        $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
        $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=productList';
        $method = 'post';
        $parameters = array(
          array(
            'variable' => 'stockcode',
            'value' => $stockcode
          )
        );

        $response = ei_parse_url_request($server, $request, $method, $parameters);
        //dpm($response);

        if (isset($response['status']) && $response['status'] == 'success' && isset($response['result']->products)) {
          $product = array_shift($response['result']->products);
          if (isset($product->sellprice6)) {
            $price = $product->sellprice6;
            if ((int) $price == 9999) {
              $output = 'POA';
            }
            elseif ((int) $price == 8888) {
              $output = 'POA';
            }
            elseif (is_numeric($price)) {
              $output = number_format($price, 2, '.', '');
            }
          }
        }
      }
    }
    else {
      watchdog('exo_integration: get_list_price', 'Can\'t get stockcode. <br/><pre>' . print_r($params, 1) . '</pre>', NULL, WATCHDOG_WARNING);
    }
  }
  return $output;
}


function ei_get_cached_prices($accountno = NULL) {
  $items = array();
  $total = 0;

  if (!isset($accountno)) {
    if (user_is_logged_in()) {
      global $user;
      $accountno = isset($user->account_no) ? $user->account_no : 0;
    }
  }

  if (isset($accountno) && !empty($accountno)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=priceCacheList';
    $method = 'post';
    $parameters = array(array('variable' => 'accountno', 'value' => $accountno));

    $response = ei_parse_url_request($server, $request, $method, $parameters);
    //dpm($response);

    $total = isset($response['result'] -> pricecaches) ? count($response['result'] -> pricecaches) : 0;
    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result'] -> pricecaches as $price_cache) {
        $items[] = (array) $price_cache;
      }
    }
  }

  return array($items, $total);
}