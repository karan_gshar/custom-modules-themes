<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets manager info responsible for currently logged in account
 * @param  array Manager seqno
 * @return array contain account info or empty if no result fetched
 */
function ei_get_account_manager_info($manager_seqno) {
  $items = array();

  if ($manager_seqno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=managerList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'seqno',
        'value' => $manager_seqno
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->managers) && count($response['result']->managers) > 0
    ) {
      $items = (array) array_shift($response['result']->managers);
    }

  }
//  $items = array();
  return $items;
}