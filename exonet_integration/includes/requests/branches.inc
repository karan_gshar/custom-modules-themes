<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets price for products from exo system
 * @param  array Array of pararmetrs to pass. Have to pass 'nid' or 'stockcode' for product reference, may pass 'qty', otherwise falling back to 1.
 * @return string price rounded to 2 decimal digits or empty string if no result fetched
 */
function ei_get_product_branch_info($params = array()) {
  $stockcode = '';
  if (isset($params['stockcode'])) {
    $stockcode = trim($params['stockcode']);
  }
  else {
    if (isset($params['nid'])) {
      $product = node_load($params['nid']);
      $stockcode = isset($product->field_stock_code['und'][0]['value']) ? $product->field_stock_code['und'][0]['value'] : '';
    }
  }

  $items = array();
  $total = 0;

  if (!empty($stockcode)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=productBranchList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'stockcode',
        'value' => $stockcode
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//        dpm($response);
    $total = isset($response['result']->productbranches) ? count($response['result']->productbranches) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->productbranches as $productbranch) {
        $items[] = (array) $productbranch;
      }
    }
  }
  else {
    watchdog('exo_integration: ' . __FUNCTION__, 'Can\'t get stockcode. <br/><pre>' . print_r($params, 1) . '</pre>', NULL, WATCHDOG_WARNING);
  }

  return array($items, $total);
}


function ei_get_branch_info($params = array()) {
  $items = array();
  $total = 0;

  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=branchList';
  $parameters = array();

  if (isset($params['seqno'])) {
    $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
  }

  $method = !empty($parameters) ? 'post' : 'get';

  $response = ei_parse_url_request($server, $request, $method, $parameters);
//        dpm($response);
  $total = isset($response['result']->branches) ? count($response['result']->branches) : 0;
  if (isset($response['status']) && $response['status'] == 'success' && $total) {
    foreach ($response['result']->branches as $branch) {
      $items[] = (array) $branch;
    }
  }

  return array($items, $total);
}