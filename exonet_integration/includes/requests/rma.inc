<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');

/**
 * Gets list of rma requests
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno, rmaid, rmano, sortby (with "sortdir" only), page (with "limit" only), from, to, status, keyword
 * @return numeric array contain rmas under index 0 and total amount of elements under index 1, like "array($items = array(), $total = 0)"
 */
function ei_get_rma_history($params = array()) {
  global $user;
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
//  dpm($params);
  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=rmaList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );

    if (isset($params['rma_id'])) {
      $parameters[] = array(
        'variable' => 'rmaid',
        'value' => $params['rma_id']
      );
    }

    if (isset($params['rmano'])) {
      $parameters[] = array('variable' => 'rmano', 'value' => $params['rmano']);
    }

    if (isset($params['sortby']) && isset($params['sortdir'])) {
      $parameters[] = array(
        'variable' => 'order',
        'value' => $params['sortby']
      );
      $parameters[] = array(
        'variable' => 'direction',
        'value' => $params['sortdir']
      );
    }

    if (isset($params['limit']) && isset($params['page'])) {
      $parameters[] = array('variable' => 'limit', 'value' => $params['limit']);
      $parameters[] = array(
        'variable' => 'offset',
        'value' => ($params['page'] - 1) * $params['limit']
      );
    }

    if (isset($params['from']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['from'])) {
      $date_components = explode('/', $params['from']);
      $parameters[] = array(
        'variable' => 'mindatesubmitted',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0]
      );
    }

    if (isset($params['to']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['to'])) {
      $date_components = explode('/', $params['to']);
      $parameters[] = array(
        'variable' => 'maxdatesubmitted',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0] . ''
      );
    }

    if (isset($params['status']) && !empty($params['status'])) {
      $parameters[] = array(
        'variable' => 'status',
        'value' => $params['status']
      );
    }

    if (isset($params['rmaid']) && !empty($params['rmaid'])) {
      $parameters[] = array('variable' => 'rmaid', 'value' => $params['rmaid']);
    }

    if (isset($params['rmano']) && !empty($params['rmano'])) {
      $parameters[] = array('variable' => 'rmano', 'value' => $params['rmano']);
    }

    if (isset($params['item']) && !empty($params['item'])) {
      $parameters[] = array('variable' => 'item', 'value' => $params['item']);
    }

    if (isset($params['fault']) && !empty($params['fault'])) {
      $parameters[] = array('variable' => 'fault', 'value' => $params['fault']);
    }

    if (isset($params['keyword']) && !empty($params['keyword'])) {
      $parameters[] = array(
        'variable' => 'keywords',
        'value' => $params['keyword']
      );
    }

//    dpm($parameters);
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->count) ? $response['result']->count : 0;

    if (isset($response['status']) && $response['status'] == 'success' && isset($response['result']->rmas) && $total) {
      foreach ($response['result']->rmas as $return) {
        $items[] = (array) $return;
      }
    }
  }
  return array($items, $total);
}


/**
 * Gets list of rma statuses
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user. Available parameters: uid, accountno
 * @return numeric array contain rma statuses under index 0 and total amount of elements under index 1, like "array($items = array(), $total = 0)"
 */
function ei_get_rma_statuses($params = array()) {
  global $user;
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=rmaStatusList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->statuses) ? count($response['result']->statuses) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->statuses as $status) {
        $items[] = $status->status;
      }
    }
  }
  return array($items, $total);
}


/**
 * Sets RMA
 * @param array $post_data - data, which needs to be set for RMA. Can pass anything, but only particular indexes would be selected. For complete list of indexes please refer to code.
 * @return numeric array contain complete rma data set under index 0 and sub index 0 ($rmas[0][0]) and total amount of elements under index 1 (basically 1 if success or 0 if fails), like "array($rmas[0] = array(...), $total = 1)"
 */
function ei_send_rma_request($post_data = array()) {
  $fields = array(
    'loginuid',
    'accountno',
    'type',
    'company',
    'contact',
    'phone',
    'mobile',
    'fax',
    'email',
    'orderno',
    'address1',
    'address2',
    'address3',
    'address4',
    'address5',
    'address6',
    'postcode',
    'rmacontact',
    'invoiceno',
    'modelno',
    'serialno',
    'productdesc',
    'faultdesc',
    'accessoriesincl',
    'servicecontract',
    'serviceno',
    'termsandconditions',
    'returnreceived',
    'returnprocessed',
    'quoteaccepted',
    'quoteaccepteddate',
    'quoteamount',
    'acceptedby',
    'status',
    'datesubmitted',
    'addressseqno',
  );

  $data_to_send = array();
  foreach ($fields as $key) {
    if (isset($post_data[$key])) {
      if ($key == 'productdesc' || $key == 'faultdesc' || $key == 'accessoriesincl') {
        $post_data[$key] = str_replace(array(
          "\n",
          "\r"
        ), ' ', $post_data[$key]);
      }
      $tmp['variable'] = $key;
      $tmp['value'] = $post_data[$key];
      $data_to_send[] = $tmp;
    }
  }
  $items = array();
  $total = 0;
  if (!empty($data_to_send)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=rmaInsert';
    $method = 'post';

    $response = ei_parse_url_request($server, $request, $method, $data_to_send);
//      dpm($response);
    $total = isset($response['result']->rmas) ? count($response['result']->rmas) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      $items[] = (array) array_shift($response['result']->rmas);
    }
  }
  return array($items, $total);
}


/**
 * Updates RMA record
 * @param  array rma_id is mandatory minimum to pass request. Currently supported keys for update: quoteaccepteddate, status, quoteaccepted, orderno, isprocessed
 * @return numeric array contain updated rma data set under index 0 and sub index 0 ($rmas[0][0]) and total amount of elements under index 1 (basically 1 if success or 0 if fails), like "array($rmas[0] = array(...), $total = 1)"
 */
function ei_update_rma($params = array()) {
  $items = array();
  $total = 0;
  if (isset($params['rma_id']) && ei_check_rma_access($params['rma_id'])) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=rmaUpdate';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'rmaid',
        'value' => $params['rma_id']
      )
    );

    if (isset($params['quoteaccepteddate'])) {
      $parameters[] = array(
        'variable' => 'quoteaccepteddate',
        'value' => $params['quoteaccepteddate']
      );
    }

    if (isset($params['status'])) {
      $parameters[] = array(
        'variable' => 'status',
        'value' => $params['status']
      );
    }

    if (isset($params['quoteaccepted'])) {
      $parameters[] = array(
        'variable' => 'quoteaccepted',
        'value' => $params['quoteaccepted']
      );
    }

    if (isset($params['orderno'])) {
      $parameters[] = array(
        'variable' => 'orderno',
        'value' => $params['orderno']
      );
    }

    if (isset($params['isprocessed'])) {
      $parameters[] = array(
        'variable' => 'isprocessed',
        'value' => $params['isprocessed']
      );
    }

    $result = ei_parse_url_request($server, $request, $method, $parameters);

    $total = isset($result->rmas) && !empty($result->rmas) ? count($result->rmas) : 0;

    if (isset($result['status']) && $result['status'] == 'success' && $total) {
      $items[] = (array) array_shift($result->rmas);
    }
  }
  return array($items, $total);
}


/**
 * Returns TRUE if user has access to rma and FALSE in contrary
 * @param int $rma_id RMA id :)
 * @return boolean
 */
function ei_check_rma_access($rma_id) {
  $rma = ei_get_rma_history(array('rma_id' => $rma_id));
  if ($rma[1]) {
    return TRUE;
  }
  return FALSE;
}


/**
 * Returns TRUE if user has access to rma quote and FALSE in contrary
 * @param int $rmano RMA number :)
 * @return boolean
 */
function ei_check_quote_file_permissions($rmano) {
  $rma = ei_get_rma_history(array('rmano' => $rmano));
  if ($rma[1]) {
    return TRUE;
  }
  return FALSE;
}