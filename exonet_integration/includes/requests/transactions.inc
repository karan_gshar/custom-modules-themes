<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets account history
 * @param  array optional parameter. Pass parameters to get more specific request. By default fetching result for currently logged in user.
 * @return array contain account info or empty if no result fetched
 */
function ei_get_account_history($params = array()) {
  global $user;
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
//  dpm($params);
  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=transactionList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );

    if (isset($params['current_only'])) {
      $parameters[] = array(
        'variable' => 'short',
        'value' => $params['current_only']
      );
    }

    if (isset($params['seqno']) && !empty($params['seqno'])) {
      $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
    }

    if (isset($params['sortby']) && isset($params['sortdir'])) {
      $parameters[] = array(
        'variable' => 'order',
        'value' => $params['sortby']
      );
      $parameters[] = array(
        'variable' => 'direction',
        'value' => $params['sortdir']
      );
    };

    if (isset($params['limit']) && isset($params['page'])) {
      $parameters[] = array('variable' => 'limit', 'value' => $params['limit']);
      $parameters[] = array(
        'variable' => 'offset',
        'value' => ($params['page'] - 1) * $params['limit']
      );
    };

    if (isset($params['invoiceno']) && !empty($params['invoiceno'])) {
      $parameters[] = array(
        'variable' => 'invoiceno',
        'value' => $params['invoiceno']
      );
    }

    if (isset($params['ref1']) && !empty($params['ref1'])) {
      $parameters[] = array('variable' => 'ref1', 'value' => $params['ref1']);
    }

    if (isset($params['ref2']) && !empty($params['ref2'])) {
      $parameters[] = array('variable' => 'ref2', 'value' => $params['ref2']);
    }

    if (isset($params['from']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['from'])) {
      $date_components = explode('/', $params['from']);
      $parameters[] = array(
        'variable' => 'mintransdate',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0]
      );
    }

    if (isset($params['to']) && preg_match('/\d{2}\/\d{2}\/\d{4}/is', $params['to'])) {
      $date_components = explode('/', $params['to']);
      $parameters[] = array(
        'variable' => 'maxtransdate',
        'value' => $date_components[2] . '-' . $date_components[1] . '-' . $date_components[0] . ''
      );
    }

    if (isset($params['status'])) {
      $parameters[] = array(
        'variable' => 'status',
        'value' => $params['status']
      );
    }


    if (isset($params['trans_type'])) {
      $parameters[] = array(
        'variable' => 'transtype',
        'value' => $params['trans_type']
      );
    }

//  drupal_set_message('<pre>' . print_r($parameters, 1) . '</pre>');
//    dpm($parameters);
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->transactionsmeta->unlimtedcount) ? $response['result']->transactionsmeta->unlimtedcount : 0;
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->transactions) && count($response['result']->transactions) > 0
    ) {
      foreach ($response['result']->transactions as $transaction) {
        $items[] = (array) $transaction;
      }
    }
  }
  return array($items, $total);
}


function ei_get_transaction_account_list() {
  global $user;
  $accountno = 0;
  if (isset($user->account_no)) {
    $accountno = $user->account_no;
  }
  else {
    $account = user_load($user->uid);
    $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
  }

  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=transactionCreatorList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );


//  drupal_set_message('<pre>' . print_r($parameters, 1) . '</pre>');
//    dpm($parameters);
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->accounts) ? $response['result']->accounts : 0;
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->accounts) && count($response['result']->accounts) > 0
    ) {
      foreach ($response['result']->accounts as $account) {
        $items[] = (array) $account;
      }
    }
  }
  return array($items, $total);
}