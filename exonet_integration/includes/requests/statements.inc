<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets statements information from exo system
 * @param  int optional parameter. Pass user id to get account info for that user, otherwise currently logged user id would be used.
 * @return array contain account info or empty if no result fetched
 */
function ei_get_current_status($uid = NULL) {
  global $user;
  if (!$uid) {
    $uid = $user->uid;
  }

  $statements_info = array();
  if ($uid) {
    $accountno = 0;
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      $account = user_load($user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }

    if ($accountno) {
      $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
      $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=statusList';
      $method = 'post';
      $parameters = array(
        array(
          'variable' => 'accountno',
          'value' => $accountno
        )
      );

      $response = ei_parse_url_request($server, $request, $method, $parameters);

      if (
        isset($response['status']) && $response['status'] == 'success' &&
        isset($response['result']->statuses) && count($response['result']->statuses) == 1
      ) {
        $statements_info = (array) array_shift($response['result']->statuses);
      }
    }
  }
  return $statements_info;
}


function ei_get_statements($params = array()) {
  $accountno = 0;
  if (isset($params['accountno'])) {
    $accountno = $params['accountno'];
  }
  else {
    global $user;
    if (isset($user->account_no)) {
      $accountno = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=statementList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );

    if (isset($params['current_only'])) {
      $parameters[] = array(
        'variable' => 'short',
        'value' => $params['current_only']
      );
    }

    if (isset($params['sortby']) && isset($params['sortdir'])) {
      $sortby = '';
      if ($params['sortby'] == 'statementdate_month') {
        $sortby = 'statementdate';
      }
      elseif ($params['sortby'] == 'statementdate_year') {
        $sortby = 'statementdate';
      };

      $parameters[] = array('variable' => 'order', 'value' => $sortby);
      $parameters[] = array(
        'variable' => 'direction',
        'value' => $params['sortdir']
      );
    };

    if (isset($params['limit']) && isset($params['page'])) {
      $parameters[] = array('variable' => 'limit', 'value' => $params['limit']);
      $parameters[] = array(
        'variable' => 'offset',
        'value' => ($params['page'] - 1) * $params['limit']
      );
    };

    if (isset($params['seqno'])) {
      $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
    }

    if (isset($params['statementdate_year'])) {
      $parameters[] = array(
        'variable' => 'statementyear',
        'value' => $params['statementdate_year']
      );
    }

    if (isset($params['statementdate_month'])) {
      $parameters[] = array(
        'variable' => 'statementmonth',
        'value' => $params['statementdate_month']
      );
    }

    if (isset($params['filename'])) {
      $parameters[] = array(
        'variable' => 'filename',
        'value' => $params['filename']
      );
    }

//    dpm($parameters);
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->statementsmeta->unlimtedcount) ? $response['result']->statementsmeta->unlimtedcount : count($response['result']->statements);
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->statements) && !empty($response['result']->statements)
    ) {
      foreach ($response['result']->statements as $statement) {
        $items[] = (array) $statement;
      }
    }
  }
  return array($items, $total);
}


function ei_get_statemetns_account_list() {
  global $user;
  $accountno = 0;
  if (isset($user->account_no)) {
    $accountno = $user->account_no;
  }
  else {
    $account = user_load($user->uid);
    $accountno = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
  }

  $items = array();
  $total = 0;
  if ($accountno) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=statementCreatorList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $accountno
      )
    );


//  drupal_set_message('<pre>' . print_r($parameters, 1) . '</pre>');
//    dpm($parameters);
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

    $total = isset($response['result']->accounts) ? $response['result']->accounts : 0;
    if (
      isset($response['status']) && $response['status'] == 'success' &&
      isset($response['result']->accounts) && count($response['result']->accounts) > 0
    ) {
      foreach ($response['result']->accounts as $account) {
        $items[] = (array) $account;
      }
    }
  }
  return array($items, $total);
}