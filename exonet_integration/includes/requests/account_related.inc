<?php
module_load_include('inc', 'exo_integration', 'includes/defaults');
module_load_include('inc', 'exo_integration', 'includes/url_parser');
/**
 * Gets account information from exo system
 * @param  int optional parameter. Pass user id to get account info for that user, otherwise currently logged user id would be used.
 * @return array contain account info or empty if no result fetched
 */
function ei_get_account_info($params = array()) {
  global $user;
  $account_no = 0;
  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $account_info = array();
  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=accountList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    $response = ei_parse_url_request($server, $request, $method, $parameters);

    $total = isset($response['result']->accounts) ? count($response['result']->accounts) : 0;
    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      $account_info = (array) array_shift($response['result']->accounts);
    }
  }

  return $account_info;
}


/**
 * Sets account information from drupal system and sends it to exo
 * @param  array parameters to send, should have particular indexes.
 */
function ei_set_account_info($post_data) {
  $fields = array(
    'accountno',
    'phone',
    'fax',
    'email',
    'deliveryadd1',
    'deliveryadd2',
    'deliveryadd3',
    'deliveryadd4',
    'deliveryadd5',
    'deliverypostcode',
    'deliveryadd6',
    'name',
    'postaladd1',
    'postaladd2',
    'postaladd3',
    'postalpostcode',
    'postaladd4'
  );
  $data_to_send = array();
  foreach ($fields as $key) {
    if (isset($post_data[$key])) {
      $tmp['variable'] = $key;
      $tmp['value'] = $post_data[$key];
      $data_to_send[] = $tmp;
    }
  }
  if (!empty($data_to_send)) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=accountDeltaReplace';
    $method = 'post';

    ei_parse_url_request($server, $request, $method, $data_to_send);
  }
}

/**
 * Sets account information from drupal system and sends it to exo
 * @param  array parameters to send, should have particular indexes.
 */
function ei_get_addresses($params = array()) {
  global $user;
  $account_no = 0;
  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }
  $total = 0;
  $items = array();
  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=addressList';
    $method = 'post';
    $parameters[] = array('variable' => 'direction', 'value' => 'asc');
    $parameters[] = array('variable' => 'order', 'value' => 'deliveryadd1');

    if (isset($params['seqno'])) {
      $parameters[] = array('variable' => 'seqno', 'value' => $params['seqno']);
    }
    else {
      $parameters = array(
        array(
          'variable' => 'accountno',
          'value' => $account_no
        )
      );
    }

    if (isset($params['source'])) {
      $parameters[] = array(
        'variable' => 'source',
        'value' => $params['source']
      );
    }

    $response = ei_parse_url_request($server, $request, $method, $parameters);

    $total = isset($response['result']->addresses) ? count($response['result']->addresses) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      $items = $response['result']->addresses;
    }
  }

  return array($items, $total);
}


/**
 * Sets account information from drupal system and sends it to exo
 * @param  array parameters to send, should have particular indexes.
 */
function ei_get_login_info($params = array()) {
  global $user;
  $loginuid = 0;

  if (isset($params['loginuid'])) {
    $loginuid = $params['loginuid'];
  }
  else {
    if (isset($user->exo_uid)) {
      $loginuid = $user->exo_uid;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $loginuid = isset($account->field_exo_uid['und'][0]['value']) ? $account->field_exo_uid['und'][0]['value'] : 0;
    }
  }

  $login_info = array();

  if ($loginuid) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=loginList';
    $method = 'post';
    $parameters = array(array('variable' => 'uid', 'value' => $loginuid));

    $response = ei_parse_url_request($server, $request, $method, $parameters);

    $total = isset($response['result']->logins) ? count($response['result']->logins) : 0;
    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      $login_info = (array) array_shift($response['result']->logins);
    }
  }

  return $login_info;
}


/**
 * Sets account information from drupal system and sends it to exo
 * @param  array parameters to send, should have particular indexes.
 */
function ei_get_logins($params = array()) {
  $total = 0;
  $items = array();

  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=loginList';
  $method = 'post';

  if (isset($params['loginuid'])) {
    $parameters = array(
      array(
        'variable' => 'uid',
        'value' => $params['loginuid']
      )
    );
  }

  if (isset($params['accountno'])) {
    $parameters[] = array(
      'variable' => 'accountno',
      'value' => $params['accountno']
    );
  }
//  dpm($parameters);
  $response = ei_parse_url_request($server, $request, $method, $parameters);
//  dpm($response);

  $total = isset($response['result']->logins) ? count($response['result']->logins) : 0;

  if (isset($response['status']) && $response['status'] == 'success' && $total) {
    foreach ($response['result']->logins as $login) {
      $items[] = (array) $login;
    }
  }

  return array($items, $total);
}


function ei_get_children_accounts($params = array()) {
  global $user;
  $account_no = 0;
  if (isset($params['accountno'])) {
    $account_no = $params['accountno'];
  }
  else {
    if (isset($user->account_no)) {
      $account_no = $user->account_no;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $account_no = isset($account->field_account_no['und'][0]['value']) ? $account->field_account_no['und'][0]['value'] : 0;
    }
  }

  $total = 0;
  $items = array();
  if ($account_no) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=accountRelationList';
    $method = 'post';
    $parameters = array(
      array(
        'variable' => 'accountno',
        'value' => $account_no
      )
    );

    //  dpm($parameters);
    $response = ei_parse_url_request($server, $request, $method, $parameters);
//    dpm($response);

//    drupal_set_message('<pre>' . print_r($response, 1) . '</pre>');
    $total = isset($response['result']->accountrelations) ? count($response['result']->accountrelations) : 0;

    if (isset($response['status']) && $response['status'] == 'success' && $total) {
      foreach ($response['result']->accountrelations as $children) {
        $items[] = (array) $children;
      }
    }
  }

  return array($items, $total);
}


/**
 * Updates login in NAPI
 * @param  array parameters to send, should have particular indexes.
 */
function ei_set_login_info($params = array()) {
  global $user;
  $loginuid = 0;

  if (isset($params['loginuid'])) {
    $loginuid = $params['loginuid'];
  }
  else {
    if (isset($user->exo_uid)) {
      $loginuid = $user->exo_uid;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $loginuid = isset($account->field_exo_uid['und'][0]['value']) ? $account->field_exo_uid['und'][0]['value'] : 0;
    }
  }

  if ($loginuid) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=loginUpdate';
    $method = 'post';
    $parameters = array(array('variable' => 'uid', 'value' => $loginuid));

    if (isset($params['marketverticals'])) {
      $parameters[] = array(
        'variable' => 'marketverticals',
        'value' => $params['marketverticals']
      );
    }

    if (isset($params['productgroups'])) {
      $parameters[] = array(
        'variable' => 'productgroups',
        'value' => $params['productgroups']
      );
    }

    if (isset($params['unsubscribe'])) {
      $parameters[] = array(
        'variable' => 'unsubscribe',
        'value' => $params['unsubscribe']
      );
    }

    $response = ei_parse_url_request($server, $request, $method, $parameters);
  }
}


function ei_get_marketing_verticals() {
  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=verticalList';
  $method = 'get';

  $response = ei_parse_url_request($server, $request, $method);

  $items = array();
  $success = isset($response['status']) && $response['status'] == 'success';
  if ($success && !empty($response['result']->verticals)) {
    foreach ($response['result']->verticals as $vertical) {
      $items[$vertical->name] = $vertical->displayname;
    }
  }

  return $items;
}


function ei_get_marketing_groups() {
  $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
  $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=groupList';
  $method = 'get';

  $response = ei_parse_url_request($server, $request, $method);

  $items = array();
  $success = isset($response['status']) && $response['status'] == 'success';
  if ($success && !empty($response['result']->groups)) {
    foreach ($response['result']->groups as $group) {
      $items[$group->name] = $group->displayname;
    }
  }

  return $items;
}


function ei_get_crm_fields($params = array()) {
  global $user;

  if (isset($params['loginuid'])) {
    $loginuid = $params['loginuid'];
  }
  else {
    if (isset($user->exo_uid)) {
      $loginuid = $user->exo_uid;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $loginuid = isset($account->field_exo_uid['und'][0]['value']) ? $account->field_exo_uid['und'][0]['value'] : 0;
    }
  }

  $defaults = &drupal_static(__FUNCTION__ . '_' . $loginuid);

  if (!isset($defaults)) {
    $defaults = FALSE;

    if ($loginuid) {
      $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
      $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=crmList';
      $method = 'post';
      $parameters = array(
        array(
          'variable' => 'loginuid',
          'value' => $loginuid
        )
      );

      $response = ei_parse_url_request($server, $request, $method, $parameters);

      $success = isset($response['status']) && $response['status'] == 'success';
      if ($success && !empty($response['result']->crms)) {
        $defaults = reset($response['result']->crms);
      }
    }

  }

  return $defaults;
}


function ei_replace_crm_fields($params = array()) {
  global $user;

  if (isset($params['loginuid'])) {
    $loginuid = $params['loginuid'];
  }
  else {
    if (isset($user->exo_uid)) {
      $loginuid = $user->exo_uid;
    }
    else {
      $account = user_load(isset($params['uid']) ? $params['uid'] : $user->uid);
      $loginuid = isset($account->field_exo_uid['und'][0]['value']) ? $account->field_exo_uid['und'][0]['value'] : 0;
    }
  }

  if ($loginuid) {
    $server = 'http://' . variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT);
    $request = variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT) . '?method=crmReplace';
    $method = 'post';
    $parameters = array(array('variable' => 'loginuid', 'value' => $loginuid));

    if (isset($params['marketverticals'])) {
      $parameters[] = array(
        'variable' => 'marketverticals',
        'value' => $params['marketverticals']
      );
    }

    if (isset($params['productgroups'])) {
      $parameters[] = array(
        'variable' => 'productgroups',
        'value' => $params['productgroups']
      );
    }

    if (isset($params['unsubscribe'])) {
      $parameters[] = array(
        'variable' => 'unsubscribe',
        'value' => $params['unsubscribe']
      );
    }

    $response = ei_parse_url_request($server, $request, $method, $parameters);
  }
}