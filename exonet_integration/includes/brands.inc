<?php

/**
 * Updates brands or creates it if not exists.
 * @param  array $brand_data contain data to populate brand node. Field "seqno" is mandatory minimum.
 */
function ei_update_brand(array $brand_data) {
  if (isset($brand_data['seqno'])) {
    $brand_seqno = $brand_data['seqno'];

    $seqno_result = ei_check_brand($brand_seqno);

    if (is_object($seqno_result)) {
      $brand = $seqno_result;

      if (!isset($brand->nid)) {
        $brand_name = substr(trim($brand_data['name']), 0, 255);
        $name_result = ei_check_brand_by_name($brand_name);

        if (is_object($seqno_result)) {
          $brand = $name_result;

          if (!isset($brand->nid)) {
            $brand->type = 'brands';
            node_object_prepare($brand);
          }
        }

        $brand->field_brand_seqno[LANGUAGE_NONE][0]['value'] = $brand_seqno;
        $brand->field_brand_seqno[LANGUAGE_NONE][0]['safe_value'] = $brand_seqno;
        $brand->field_brand_seqno[LANGUAGE_NONE][0]['format'] = NULL;
      }

      ei_update_brand_fields($brand, $brand_data);

      node_save($brand);
      syslog(LOG_NOTICE, 'exo_integration: Brand created with seqno: ' . $brand_seqno . ' Nid: ' . $brand->nid . ' Reference: sektor');
    }
    else {
      watchdog('exo_integration: Brand update', 'Can\'t update brand. Multiple brands found with same seqno: <pre>' . print_r($brand_seqno, 1) . '</pre>', NULL, WATCHDOG_ERROR);
    }
  }
  else {
    watchdog('exo_integration: Brand creation', 'No brand seqno set up: <pre>' . print_r($brand_data, 1) . '</pre>', NULL, WATCHDOG_ERROR);
  }
}


/**
 * Get brand based on seqno
 * @param $brand_seqno brand identifier in exo system
 * @return object|array return depends on:<br/>
 *  1) object if brand found<br/>
 *  2) empty object if no brand found<br/>
 *  3) array of nids if multiple brands found
 */
function ei_check_brand($brand_seqno) {
  $seqno_check = db_select('field_data_field_brand_seqno', 'seqno');
  $seqno_check->fields('seqno', array('entity_id'));
  $seqno_check->condition('seqno.entity_type', 'node');
  $seqno_check->condition('seqno.bundle', 'brands');
  $seqno_check->condition('seqno.field_brand_seqno_value', trim($brand_seqno));
  $seqno_result = $seqno_check->execute()->fetchAll();

  if (count($seqno_result) == 1) {
    $result = node_load($seqno_result[0]->entity_id);
  }
  else {
    if (count($seqno_result) == 0) {
      $result = new stdClass();
    }
    else {
      $result = $seqno_result;
      watchdog('exo_integration: Multiple brands found with same seqno: ' . print_r($brand_seqno, 1), NULL, WATCHDOG_ERROR);
    }
  }

  return $result;
}


function ei_check_brand_by_name($brand_name) {
  $name_check = db_select('node', 'n');
  $name_check->fields('n', array('nid'));
  $name_check->condition('n.type', 'brands');
  $name_check->condition('n.title', $brand_name, 'LIKE');
  $name_result = $name_check->execute()->fetchAll();

  if (count($name_result) == 1) {
    $result = node_load($name_result[0]->nid);
  }
  else {
    if (count($name_result) == 0) {
      $result = new stdClass();
    }
    else {
      $result = $name_result;
      watchdog('exo_integration: Multiple brands found with same name: ' . print_r($brand_name, 1), NULL, WATCHDOG_ERROR);
    }
  }

  return $result;
}


/**
 * Updates brand ct fields. Everything except content type system fields and brand seqno.
 * @param object $brand content type object to perform action on.
 * @param array $data data to populate object fields. Only particular fiedls would be selected.
 */
function ei_update_brand_fields($brand, array $data) {
  module_load_include('inc', 'exo_integration', 'includes/defaults');
  module_load_include('inc', 'exo_integration', 'includes/support_functions');
  $brand->uid = CREATOR_UID;
  $brand->language = LANGUAGE_NONE;
  $brand->status = 1;
  $brand->promote = 0;

// Title
  if (isset($data['name']) && $data['name'] != NULL && strlen($data['name'])) {
    $brand->title = substr($data['name'], 0, 255);
  }
  else {
    $brand->title = 'Undefined';
  }

// Description
  if (isset($data['description']) && $data['description'] != NULL && strlen($data['description'])) {
    $brand->body[LANGUAGE_NONE][0]['value'] = $data['description'];
    $brand->body[LANGUAGE_NONE][0]['safe_value'] = $data['description'];
    $brand->body[LANGUAGE_NONE][0]['format'] = 'full_html';
  }
  else {
    $brand->body[LANGUAGE_NONE][0]['value'] = '';
    $brand->body[LANGUAGE_NONE][0]['safe_value'] = '';
    $brand->body[LANGUAGE_NONE][0]['format'] = 'full_html';
  }

// Website
  if (isset($data['website']) && $data['website'] != NULL && strlen($data['website'])) {
    $brand->field_brand_website[LANGUAGE_NONE][0]['title'] = $brand->title;
    $brand->field_brand_website[LANGUAGE_NONE][0]['url'] = substr($data['website'], 0, 255);
  }
  else {
    $brand->field_brand_website[LANGUAGE_NONE][0]['title'] = '';
    $brand->field_brand_website[LANGUAGE_NONE][0]['url'] = '';
  }

// Image
  if (isset($data['image']) && !empty($data['image'])) {
    // Do Image stuff
    $filename_original = $data['image'];
    $folder_original = 'original_brand_logos';

    $filename_destination = $data['image'];
    $folder_destination = 'brands';

    $img = ei_save_single_file(CREATOR_UID, $filename_original, $folder_original, $filename_destination, $folder_destination);
    if ($img) {
      if (isset($brand->field_brand_logo[LANGUAGE_NONE]) && !empty($brand->field_brand_logo[LANGUAGE_NONE])) {
        foreach ($brand->field_brand_logo[LANGUAGE_NONE] as $index => $logo_image) {
          $file = file_load($logo_image['fid']);
          file_delete($file);
          unset($brand->field_brand_logo[LANGUAGE_NONE][$index]);
        }
      }

      $brand->field_brand_logo[LANGUAGE_NONE][] = array(
        'fid' => $img->fid,
        'uid' => $img->uid,
        'filename' => $img->filename,
        'uri' => $img->uri,
        'filemime' => $img->filemime,
        'filesize' => $img->filesize,
        'status' => $img->status,
        'display' => $img->status,
        'type' => 'image'
      );

      _ei_flush_image_cache('brand', $folder_destination, $filename_destination);
    }
  }

  // Create brand taxonomy
  $brand->field_brand_reference[LANGUAGE_NONE][0]['tid'] = ei_get_term_id('brands', $brand->title);
}


/**
 * Delete brand by exo seqno.
 * @param string|integer $seqno brand unique identifier.
 */
function ei_delete_brand($seqno) {
  $brand = ei_check_brand($seqno);

  if (is_object($brand)) {
    if (isset($brand->nid)) {
      // Need to delete node first, then taxonomy
      // If delete taxonomy first, then will throws a lot of errors in watchdog on node deletion (like array_flip problems)
      $tid = NULL;

      if (isset($brand->field_brand_reference[LANGUAGE_NONE][0]['tid']) && $brand->field_brand_reference[LANGUAGE_NONE][0]['tid'] != NULL) {
        $tid = $brand->field_brand_reference[LANGUAGE_NONE][0]['tid'];
      }

      node_delete($brand->nid);
      if ($tid) {
        module_load_include('inc', 'exo_integration', 'includes/taxonomy');
        ei_remove_term($tid, 'field_brand');
      }

      syslog(LOG_NOTICE, 'exo_integration: Brand deleted with seqno: ' . $seqno . ' Reference: sektor');
    }
    else {
      watchdog('exo_integration: Brand delete', 'No result found for seqno: ' . $seqno . '. Already deleted?', NULL, WATCHDOG_NOTICE);
    }
  }
}