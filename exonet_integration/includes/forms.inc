<?php


function exo_integration_module_settings() {

  // $module_path = drupal_get_path('module', 'exo_integration');

  $form['vertical_tab_holder'] = array('#type' => 'vertical_tabs');

  $form['vertical_tab_holder']['general'] = array(
    '#type' => 'fieldset',
    '#title' => 'General settings'
  );

  $form['vertical_tab_holder']['general'][EI_GLOBAL_SERVER_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Server url'),
    '#size' => 30,
    '#maxlength' => 128,
    '#attributes' => array('style' => 'width:auto;'),
    '#default_value' => variable_get(EI_GLOBAL_SERVER_URL, EI_SERVER_URL_DEFAULT),
    '#prefix' => '<div style="float:left">',
    '#suffix' => '</div>',
    '#field_prefix' => 'http://',
    '#field_suffix' => '/',
    '#required' => TRUE,
    '#weight' => 0,
  );

  $form['vertical_tab_holder']['general'][EI_GLOBAL_URL_REQUEST] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#size' => 30,
    '#maxlength' => 128,
    '#attributes' => array('style' => 'width:auto;'),
    '#default_value' => variable_get(EI_GLOBAL_URL_REQUEST, EI_URL_REQUEST_DEFAULT),
    '#prefix' => '<div style="float:left; padding:25px 0 0 3px">',
    '#suffix' => '</div>',
    '#field_suffix' => '?method=XXXXXXX...',
    '#weight' => 1,
  );

  $form['vertical_tab_holder']['general'][EI_SEND_SYSTEM_NOTIFICATIONS] = array(
    '#type' => 'checkbox',
    '#title' => t('Send notifications about starting/finishing processes.'),
    '#default_value' => variable_get(EI_SEND_SYSTEM_NOTIFICATIONS, EI_SEND_SYSTEM_NOTIFICATIONS_DEFAULT),
    '#prefix' => '<div style="clear:both">',
    '#suffix' => '</div>',
    '#weight' => 2,
  );

  $form['vertical_tab_holder']['general'][EI_GENERATE_URL_ALIAS] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate URL alias for products automatically.'),
    '#description' => 'If turned off, paths will be generated out of WebURLDesc field passed from the API. If in this case field is empty, no alias will be generated.',
    '#default_value' => variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT),
    '#weight' => 3,
  );

  $form['vertical_tab_holder']['general'][EI_RECORD_ORPHANED_DATA_CLEAN_UP] = array(
    '#type' => 'checkbox',
    '#title' => t('Record orphaned data clean up on the cron run for products.'),
    '#description' => 'If process been interrupted some of the fields might still contain information about node, which does not exists anymore. There is cron job set up to clean this data. <br/>If turned ON, it will record this data to watchdog on run.',
    '#default_value' => variable_get(EI_RECORD_ORPHANED_DATA_CLEAN_UP, EI_RECORD_ORPHANED_DATA_CLEAN_UP_DEFAULT),
    '#weight' => 4,
  );

  $form['vertical_tab_holder']['general'][EI_RECORD_STOCK_UNITS_UPDATES] = array(
    '#type' => 'checkbox',
    '#title' => t('Record stock units updates queries.'),
    '#description' => '',
    '#default_value' => variable_get(EI_RECORD_STOCK_UNITS_UPDATES, EI_RECORD_STOCK_UNITS_UPDATES_DEFAULT),
    '#weight' => 5,
  );

  $form['vertical_tab_holder']['allowed_ips'] = array(
    '#type' => 'fieldset',
    '#title' => 'Allowed IPs'
  );

  $counter = variable_get(EI_REMOTE_ADDRESS_COUNT, EI_REMOTE_ADDRESS_COUNT_DEFAULT);

  $form['vertical_tab_holder']['allowed_ips'][EI_REMOTE_ADDRESS_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('How many addresses you want to grant access to'),
    '#element_validate' => array('element_validate_integer'),
    '#size' => 4,
    '#maxlength' => 7,
    '#attributes' => array('style' => 'width:auto;'),
    '#default_value' => $counter,
    '#required' => TRUE,
    '#weight' => 0,
  );

  for ($i = ($counter - 1); $i >= 0; $i--) {
    $form['vertical_tab_holder']['allowed_ips'][constant("EI_REMOTE_ADDRESS_" . $i)] = array(
      '#type' => 'textfield',
      '#title' => t('IP address ' . ($i + 1)),
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array('style' => 'width:auto;'),
      '#default_value' => variable_get(constant("EI_REMOTE_ADDRESS_" . $i), EI_REMOTE_ADDRESS_DEFAULT),
      '#required' => TRUE,
      '#weight' => $i + 1,
    );
  }


  $form['vertical_tab_holder']['request_history'] = array(
    '#type' => 'fieldset',
    '#title' => 'Requests history'
  );

  $form['vertical_tab_holder']['request_history'][EI_RECORDS_TRACK_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('How many requests you want to keep in history'),
    '#element_validate' => array('element_validate_integer'),
    '#size' => 4,
    '#maxlength' => 7,
    '#attributes' => array('style' => 'width:auto;'),
    '#default_value' => variable_get(EI_RECORDS_TRACK_COUNT, EI_RECORDS_TRACK_COUNT_DEFAULT),
    '#required' => TRUE,
    '#weight' => 0,
  );

  $form['vertical_tab_holder']['request_history'][EI_RECORDS_ITEMS_PER_PAGE] = array(
    '#type' => 'textfield',
    '#title' => t('How many items to show in history per page'),
    '#element_validate' => array('element_validate_integer'),
    '#size' => 4,
    '#maxlength' => 7,
    '#attributes' => array('style' => 'width:auto;'),
    '#default_value' => variable_get(EI_RECORDS_ITEMS_PER_PAGE, EI_RECORDS_ITEMS_PER_PAGE_DEFAULT),
    '#required' => TRUE,
    '#weight' => 0,
  );


  $form['vertical_tab_holder']['caching'] = array(
    '#type' => 'fieldset',
    '#title' => 'Caching'
  );

  $form['vertical_tab_holder']['caching']['ei_cache_prices'] = array(
    '#type' => 'checkbox',
    '#title' => 'Cache price request',
    '#default_value' => variable_get('ei_cache_prices', FALSE),
    '#weight' => 0,
  );

  return $form;
}


function exo_integration_module_history() {
//  drupal_set_message('<pre>' . print_r(node_load(2), 1) . '</pre>');
  $module_path = drupal_get_path('module', 'exo_integration');

  drupal_add_js($module_path . '/datatables/js/jquery.dataTables.min.js');
  drupal_add_js($module_path . '/datatables/js/TableTools.min.js');
  drupal_add_js($module_path . '/js/ei_script.js');

  drupal_add_css($module_path . '/datatables/css/jquery-ui-1.9.1.custom.min.css');
  drupal_add_css($module_path . '/datatables/css/TableTools.css');

  $counter = variable_get(EI_RECORDS_TRACK_CURRENT, EI_RECORDS_TRACK_CURRENT_DEFAULT);
  $last_deleted = variable_get(EI_RECORDS_TRACK_LAST_DELETED, EI_RECORDS_TRACK_LAST_DELETED_DEFAULT);
  $total_to_show = variable_get(EI_RECORDS_ITEMS_PER_PAGE, EI_RECORDS_ITEMS_PER_PAGE_DEFAULT);

  $elements = exo_integration_get_elements($last_deleted + 1, $counter);
  $table_elements = exo_integration_get_pager($elements, $total_to_show);


  $form['history_table_top_pager'] = array(
    '#markup' => $table_elements['pager'],
    '#weight' => 0,
  );

  $form['history_table_top_chooser'] = array(
    '#markup' => $table_elements['order_chooser'],
    '#weight' => 1,
  );

  $form['history_table_top_elements'] = array(
    '#markup' => $table_elements['current_elements'],
    '#weight' => 2,
  );

  $form['history_table'] = array(
    '#markup' => $table_elements['table'],
    '#weight' => 3,
  );

  $form['history_table_bottom_elements'] = array(
    '#markup' => $table_elements['current_elements'],
    '#weight' => 4,
  );

  $form['history_table_bottom_chooser'] = array(
    '#markup' => $table_elements['order_chooser'],
    '#weight' => 5,
  );

  $form['history_table_bottom_pager'] = array(
    '#markup' => $table_elements['pager'],
    '#weight' => 6,
  );
  return $form;
}

function exo_integration_get_elements($first_element, $last_element) {
  $elements = array();
  for ($i = $first_element; $i < $last_element; $i++) {
    $data = variable_get('ei_records_track_record_' . $i, NULL);
    if ($data != NULL) {
      $data_array = unserialize($data);
      $submitted = (isset($data_array['timestamp'])) ? date('d/m/Y H:i:s', $data_array['timestamp']) : NULL;
      unset($data_array['timestamp']);
      $elements[]['data'] = $data_array;
      $elements[count($elements) - 1]['entry_no'] = $i;
      $elements[count($elements) - 1]['timestamp'] = $submitted;
    }
  }
  return $elements;
}


function exo_integration_get_pager($elements, $items_per_page, $show_order_chooser = TRUE, $show_current_elements = TRUE) {
  $table = '';
  $pager = '';
  $order_chooser = '';
  $current_elements = '';

  if (!empty($elements)) {
    $first_element = 1;
    $last_element = 1;
    $order = '';

    $page = isset($_GET['page']) ? htmlspecialchars($_GET['page']) : 1;

    if (count($elements) > $items_per_page) {
      $first_element = ($page * $items_per_page) - $items_per_page;
      $last_element = (($page * $items_per_page) > count($elements)) ? count($elements) : ($page * $items_per_page);
    }
    else {
      $first_element = 0;
      $last_element = count($elements);
    }

    if ($show_current_elements) {
      $current_elements = '<span id="current_elements" class="clearfix"><span class="title">Showing:</span><span class="description">' . ($first_element + 1) . ' to ' . $last_element . ' out of ' . count($elements) . '</span></span>';
    }


    if ($show_order_chooser) {
      $order = isset($_GET['order']) ? htmlspecialchars($_GET['order']) : 'asc';
      if (strtolower($order) == 'desc') {
        $elements = array_reverse($elements);
      }
      $order_chooser = exo_integration_get_order_chooser($order);
    }

    $pager = exo_integration_get_pager_markup(ceil(count($elements) / $items_per_page), $page);


    $page_elements = array();
    for ($i = $first_element; $i < $last_element; $i++) {
      $page_elements[] = $elements[$i];
    }

    $table = exo_integration_get_markup($page_elements, $order);
  }

  $return_array = array(
    'table' => $table,
    'pager' => $pager
  );
  if ($show_order_chooser) {
    $return_array['order_chooser'] = $order_chooser;
  }
  if ($show_current_elements) {
    $return_array['current_elements'] = $current_elements;
  }

  return $return_array;
}

function exo_integration_get_pager_markup($total_pages, $current_page) {
  $pager_buttons = array();
  $max_pages_to_show = 7;

  if ($total_pages > 1) {
    $url_components = drupal_get_query_parameters();
    _ei_filter_url_query($url_components);

    $link_format = t('<a href="@path?!query" class="pager_link">!title</a>', array('@path' => current_path()));

    // If going outside of the borders
    if ($current_page > $total_pages) {
      $current_page = $total_pages;
    }
    elseif ($current_page < 1) {
      $current_page = 1;
    }

    // Determine what page numbers to show in the pager if we went outside of max allowed pages (see var above)
    $start = 1;
    $end = $total_pages;
    if ($total_pages > $max_pages_to_show) {
      $start = $current_page - floor($max_pages_to_show / 2);
      $end = $current_page + floor($max_pages_to_show / 2);
    }

    if ($start < 1) {
      $start = 1;
    }
    if ($end > $total_pages) {
      $end = $total_pages;
    }

    // Should we show "Previous" buttons?
    if ($total_pages > 1 && $current_page != 1) {
      $url_components['page'] = 1;
      $pager_buttons[] = t($link_format, array(
        '!title' => '<< First page',
        '!query' => _ei_get_query($url_components)
      ));

      $url_components['page'] = $current_page - 1;
      $pager_buttons[] = t($link_format, array(
        '!title' => '< Previous page',
        '!query' => _ei_get_query($url_components)
      ));
    }

    // Just decoration for usability
    if ($start > 1) {
      $pager_buttons[] = '...';
    }

    // Liks to pages
    for ($i = $start; $i <= $end; $i++) {
      $url_components['page'] = $i;
      $pager_buttons[] = ($i != $current_page) ?
        t($link_format, array(
          '!title' => $i,
          '!query' => _ei_get_query($url_components)
        )) :
        '<span class="pager_link current not_link">' . $i . '</span>';
    }

    // Just decoration for usability
    if ($end < $total_pages) {
      $pager_buttons[] = '...';
    }

    // Should we show "Next" buttons?
    if ($total_pages > 1 && $current_page != $total_pages) {
      $url_components['page'] = $current_page + 1;
      $pager_buttons[] = t($link_format, array(
        '!title' => 'Next page >',
        '!query' => _ei_get_query($url_components)
      ));

      $url_components['page'] = $total_pages;
      $pager_buttons[] = t($link_format, array(
        '!title' => 'Last page >>',
        '!query' => _ei_get_query($url_components)
      ));
    }
  }

  // Theming list
  return theme_item_list(array(
    'items' => $pager_buttons,
    'title' => '',
    'type' => 'ul',
    'attributes' => array(
      'id' => 'custom_pager',
      'class' => array('clearfix')
    ),
  ));
}


function _ei_get_query($url_components) {
  return filter_xss_bad_protocol(check_plain(http_build_query($url_components)));
}


function _ei_filter_url_query(&$url_components) {
  if (!empty($url_components)) {
    foreach ($url_components as $index => &$value) {
      if (!is_array($value) && empty($value) || in_array($index, array(
          'form_build_id',
          'form_token',
          'form_id'
        ))
      ) {
        unset($url_components[$index]);
      }
      elseif (is_array($value)) {
        _ei_filter_url_query($value);

        if (empty($value)) {
          unset($url_components[$index]);
        }
      }
    }
  }
}


function exo_integration_get_order_chooser($order) {
  $markup = '';
  if (strtolower($order) == 'asc' || strtolower($order) == 'desc') {
    $chooser_buttons = array();
    $url_data = parse_url($_SERVER['REQUEST_URI']);
    $path = isset($url_data['path']) ? $url_data['path'] : '/';

    $url_components = array();
    parse_str(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $url_components);

    $chooser_buttons[] = '<span class="title not_link">Sorting order:</span>';

    switch (strtolower($order)) {
      case 'asc':
        $chooser_buttons[] = '<span>ASC</span>';
        $url_components['order'] = 'desc';
        $chooser_buttons[] = '<a href="' . $path . '?' . http_build_query($url_components) . '">DESC</a>';
        break;
      case 'desc':
        $url_components['order'] = 'asc';
        $chooser_buttons[] = '<a href="' . $path . '?' . http_build_query($url_components) . '">ASC</a>';
        $chooser_buttons[] = '<span>DESC</span>';
        break;
    }

    $markup = '<ul id="order_chooser" class="clearfix"><li>' . implode('</li><li>', $chooser_buttons) . '</li></ul>';
  }
  return $markup;
}

function exo_integration_get_markup($elements, $order = '') {
  $table = '<table id="request_history_table" class="datatable" ' . (($order !== NULL && strlen($order) > 0) ? 'sort_order="' . strtolower($order) . '"' : '') . '>
                            <thead>
                              <tr>
                                <th>Entry#</th>
                                <th>Record Index</th>
                                <th>Date</th>
                                <th>Data</th>
                              </tr>
                            </thead>
                            <tbody>';
  $tbody = '';
  if (!empty($elements)) {
    foreach ($elements as $index => $data) {
      $tbody .= '<tr>
                    <td>entry_#' . ($index + 1) . '</td>
                    <td>' . $data['entry_no'] . '</td>
                    <td>' . (($data['timestamp'] != NULL) ? $data['timestamp'] : 'Unspecified') . '</td>
                    <td><textarea class="history_data_entry" readonly="readonly">' . print_r($data['data'], 1) . '</textarea></td>
                 </tr>';
    }
  }
  $table .= (($tbody != '') ? $tbody : 'No records recieved yet') . '</tbody></table>';
  return $table;
}

function exo_integration_module_url_request($form, $form_state) {
  module_load_include('inc', 'exo_integration', 'includes/defaults');

  $form['main_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => 'Send a Request',
    '#weight' => 1
  );

  $form['main_wrapper'][EI_SERVER_URL] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#description' => t('E.g.:<br/>1) [username]:[password]@example.com<br/>2) localhost<br/>No trailing slash.'),
    '#field_prefix' => 'http://',
    '#field_suffix' => '&nbsp;/&nbsp;&nbsp;',
    '#maxlength' => 128,
    '#size' => 32,
    '#default_value' => variable_get(EI_SERVER_URL, EI_SERVER_URL_DEFAULT),
    '#required' => TRUE,
    '#weight' => -1,
  );

  $form['main_wrapper'][EI_URL_REQUEST] = array(
    '#type' => 'textfield',
    '#title' => t('Request URL'),
    '#description' => t('Write request url and submit it. If request is valid you should be able to see result.'),
    '#maxlength' => 512,
    '#default_value' => variable_get(EI_URL_REQUEST, EI_URL_REQUEST_DEFAULT),
    '#required' => TRUE,
    '#weight' => 0,
  );

  $form['main_wrapper']['method_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => 'Data request options',
    '#weight' => 1
  );

  $method = isset($_SESSION['exo_integration']['post_data']['method_selection']) ? $_SESSION['exo_integration']['post_data']['method_selection'] : 'get';

  $form['main_wrapper']['method_wrapper']['method_selection'] = array(
    '#type' => 'radios',
    '#title' => 'Method',
    '#options' => array(
      'get' => '$_GET',
      'post' => '$_POST'
    ),
    '#default_value' => $method,
    '#weight' => 0
  );


  //unset($_SESSION['exo_integration']['post_data']['parameters']);

  //drupal_set_message('<pre>' . print_r(arg(), 1) . '</pre>');

  //if(arg(1) != 'ajax'){
  if (isset($_SESSION['exo_integration']['post_data']['parameters']) && !empty($_SESSION['exo_integration']['post_data']['parameters'])) {
    $index = max(array_keys($_SESSION['exo_integration']['post_data']['parameters']));
    $array = ei_get_post_data_fields($_SESSION['exo_integration']['post_data']['parameters']);
  }
  else {
    $array = ei_get_post_data_fields();
  }
  $form['main_wrapper']['method_wrapper']['post_data_wrapper'] = array_shift($array);
  //}

  $form['main_wrapper']['method_wrapper']['add_element'] = array(
    '#type' => 'button',
    '#value' => t('Add element'),
    '#name' => t('element_submit'),
    '#ajax' => array(
      'callback' => 'ei_get_post_add_fields_ajax',
      'wrapper' => 'post_data_wrapper > fieldset.form-wrapper > div.fieldset-wrapper',
      'method' => 'append',
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="method_selection"]' => array('value' => t('post')),
      ),
    ),
    '#weight' => 1000000
  );


  $form['main_wrapper']['ei_submit_url_request'] = array(
    '#type' => 'submit',
    '#name' => t('form_submit'),
    '#value' => t('Submit request'),
    '#submit' => array('exo_integration_module_submit'),
    '#weight' => 2
  );


  $form['main_wrapper']['ei_result_field_fieldset'] = array(
    '#type' => 'fieldset',
    '#weight' => 3,
    '#title' => 'Result',
  );

  $output = 'No result to display.';
  if (isset($_SESSION['exo_integration']['result'])) {
    $output = $_SESSION['exo_integration']['result'];
    unset($_SESSION['exo_integration']['result']);
  };

  $form['main_wrapper']['ei_result_field_fieldset']['ei_result_field'] = array(
    '#prefix' => '<div id="request_result">',
    '#markup' => $output,
    '#suffix' => '</div>',
  );

  return $form;
}


function ei_get_post_data_fields($values_array = array(
  0 => array(
    'variable' => '',
    'value' => ''
  )
)) {
  $sub_form = array();

  $sub_form['post_data_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => 'List of post data',
    '#weight' => 1,
    '#prefix' => '<div id="post_data_wrapper">',
    '#suffix' => '</div>',
    '#states' => array(
      'visible' => array(
        ':input[name="method_selection"]' => array('value' => t('post')),
      ),
    )
  );

  foreach ($values_array as $index => $data) {
    foreach (ei_get_post_add_fields_non_ajax($index, $data['variable'], $data['value']) as $form_key => $form_elements) {
      $sub_form['post_data_wrapper'][$form_key] = $form_elements;
    }
  }

  return $sub_form;
}


function ei_get_post_add_fields_non_ajax($fields_counter, $variable_value = '', $data_value = '') {

  $sub_form = get_fields($fields_counter, $variable_value, $data_value);

  return $sub_form;
}


function ei_get_post_add_fields_ajax() {
  $fields_counter = (isset($_SESSION['exo_integration']['post_data']['parameters']) && !empty($_SESSION['exo_integration']['post_data']['parameters'])) ?
    max(array_keys($_SESSION['exo_integration']['post_data']['parameters'])) + 1 :
    0;

  $sub_form = get_fields($fields_counter, '', '');

  return $sub_form;
}


function get_fields($index, $variable_value = '', $data_value = '') {
  $_SESSION['exo_integration']['post_data']['parameters'][$index]['variable'] = $variable_value;
  $_SESSION['exo_integration']['post_data']['parameters'][$index]['value'] = $data_value;


  //drupal_set_message('<pre>New field: ' . $fields_counter. ' Var: ' . $variable_value . ' Val: ' . $data_value . '</pre>');
  $sub_form['data_wrapper_' . $index] = array(
    '#type' => 'fieldset',
    '#title' => 'Element ' . ($index + 1),
    '#weight' => $index
  );

  $sub_form['data_wrapper_' . $index]['variable_' . $index] = array(
    '#title' => t('Variable'),
    '#type' => 'textfield',
    '#name' => 'variable_' . $index,
    '#field_suffix' => '=',
    '#default_value' => $variable_value,
  );

  $sub_form['data_wrapper_' . $index]['value_' . $index] = array(
    '#title' => t('Value'),
    '#type' => 'textarea',
    '#name' => 'value_' . $index,
    '#default_value' => $data_value,
  );

  $sub_form['data_wrapper_' . $index]['delete_' . $index] = array(
    '#title' => t('Delete element ' . ($index + 1)),
    '#description' => t('Element would be deleted on after form submit'),
    '#type' => 'checkbox',
    '#attributes' => array('id' => 'edit-delete-' . $index),
    '#value' => t('Delete element'),
    '#name' => t('delete_' . $index),
  );


  return $sub_form;
}


// Supposed to be ajax callback, but I haven't finished it
//function ei_delete_element(){
//  $sub_form = array();
//  if(isset($_POST['_triggering_element_name'])){
//    $element_components = explode('_', $_POST['_triggering_element_name']);
//    if(isset($element_components[0]) && isset($element_components[1]) && $element_components[0] == 'delete'){
//
//      $element = $element_components[1];
//      //drupal_set_message('<pre>Delete: ' . $element . ' ' . print_r($_SESSION['exo_integration']['post_data'], 1) . '</pre>');
//      unset($_SESSION['exo_integration']['post_data']['parameters'][$element]);
//      //drupal_set_message('<pre>' . print_r($_SESSION['exo_integration']['post_data'], 1) . '</pre>');
//      $array = ei_get_post_data_fields($_SESSION['exo_integration']['post_data']['parameters']);
//
//      //drupal_set_message('<pre>' . print_r($array, 1) . '</pre>');
//
//      $sub_form = array_shift($array);
//    }
//  }
//  //drupal_set_message('<pre>' . print_r(drupal_render($sub_form), 1) . '</pre>');
//  //return $sub_form;
//  //return drupal_render($sub_form);
//  //return asd;
//}


function exo_integration_module_submit($form, $form_state) {
  module_load_include('inc', 'exo_integration', 'includes/defaults');
  module_load_include('inc', 'exo_integration', 'includes/requests/pricing');

  if (!empty($_POST)) {
    unset($_SESSION['exo_integration']['post_data']['parameters']);
    $parameters = array();
    $method = '';

    foreach ($_POST as $key => $value) {
      $key_components = explode('_', $key);
      if (
        isset($key_components[0]) &&
        isset($key_components[1]) &&
        (
          $key_components[0] == 'variable' ||
          $key_components[0] == 'value'
        ) &&
        (
          !isset($_POST['delete_' . $key_components[1]]) ||
          !$_POST['delete_' . $key_components[1]]
        )
      ) {
        $parameters[$key_components[1]][$key_components[0]] = $value;
      }

      if ($key == 'method_selection') {
        $method = $value;
      }
    }

    if (!empty($parameters)) {
      $_SESSION['exo_integration']['post_data']['parameters'] = $parameters;
    }
    $_SESSION['exo_integration']['post_data']['method_selection'] = $method;
  }

  $server_url = $_POST[EI_SERVER_URL];
  variable_set(EI_SERVER_URL, $server_url);

  $request = $_POST[EI_URL_REQUEST];
  variable_set(EI_URL_REQUEST, $request);

  $request_result = ei_parse_url_request('http://' . $server_url, $request, $method, $parameters);


  $output = '';
  switch ($request_result['status']) {
    case 'critical':
    case 'error':
      if (isset($request_result['message'])) {
        if (isset($request_result['target'])) {
          form_set_error($request_result['target'], $request_result['message']);
        }
        else {
          drupal_set_message($request_result['message'], 'error');
        }
      }
    // no break tag .... It's not missing, I didn't insert it on purpose
    case 'success':
      if (isset($request_result['result'])) {
        $output .= check_plain(print_r($request_result['result'], 1));
      }
      break;
    default:
      drupal_set_message('Unknown result fetched', 'error');
      break;
  }

  if ($output != '') {
    $_SESSION['exo_integration']['result'] = '<pre>' . $output . '</pre>';
  }
}


