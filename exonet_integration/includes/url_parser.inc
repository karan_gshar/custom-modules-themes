<?php
/**
 *  Trying to get result from requested url by passing different methods and extra parameters if applicable.
 * @param  string $server server url.<br/>Eg: http://[username]:[password]@example.com or http://localhost
 * @param  string $request request string.<br/>Everything after domain reference.
 * @param  string $method either "post" or "get"
 * @param  array $parameters if $method == 'post', then list passing parameters. Each entry should be an array, with 'variable' and 'value' keys.
 * @return array  'status'    => success|error|critical,<br/>
 *                 ['result'    => if success, then contains result. May contain in case of error as well],<br/>
 *                 ['message'    => if error or critical, contains message to display],<br/>
 *                 ['target'    => if critical, may contain form element reference, which needs to be reviewed, in regards to form constructed in /exo_integration/includes/form.inc.]
 */
function ei_parse_url_request($server, $request, $method, $parameters = array()) {
  $result = array();
  $context = NULL;
  $auth_details = '';

  $url_data = parse_url($server);

  if (isset($url_data['user']) && isset($url_data['pass'])) {
    $auth_details = $url_data['user'] . ':' . $url_data['pass'];
    $context = stream_context_create(array(
      'http' => array(
        'header' => "Authorization: Basic " . base64_encode($auth_details) . "\r\n" .
          "Source: Drupal"
      )
    ));
  }
  else {
    $context = stream_context_create(array(
      'http' => array(
        'header' => "Source: Drupal"
      )
    ));
  }

  $final_server_url = (isset($url_data['scheme']) ? $url_data['scheme'] . '://' : 'http://') . $url_data['host'];

  $url_data = parse_url($request);

  $url_components = array();
  parse_str(parse_url($request, PHP_URL_QUERY), $url_components);

  switch ($method) {
    case 'get':
      if (!empty($url_components)) {
        $request_url = $final_server_url . '/' . (isset($url_data['path']) ? $url_data['path'] : '') . '?' . http_build_query($url_components);
        $requested_data = file_get_contents($request_url, FALSE, $context);

        if ($data = json_decode($requested_data)) {
          $result['status'] = 'success';
          $result['result'] = $data;
        }
        else {
          $result['status'] = 'error';
          $result['result'] = $requested_data;
          $result['message'] = 'No valid JSON object can be retrieved';
        }

      }
      else {
        $result['status'] = 'critical';
        $result['target'] = 'ei_url_request';
        $result['message'] = 'For GET request no valid request string found in url';
      }
      break;
    case 'post':
      $url = $final_server_url . '/' . (isset($url_data['path']) ? $url_data['path'] : '') . (!empty($url_components) ? '?' . http_build_query($url_components) : '');

      $post_request_array = array();
      if (!empty($parameters)) {
        foreach ($parameters as $param_data) {
          if (isset($param_data['variable']) && !empty($param_data['variable']) && isset($param_data['value'])) {
            $post_request_array[$param_data['variable']] = $param_data['value'];
          }
        }
      }

      if (!empty($post_request_array)) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_request_array));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Source: Drupal',
        ));

        if ($auth_details != '') {
          curl_setopt($ch, CURLOPT_USERPWD, $auth_details);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        }

        $requested_data = curl_exec($ch);

        if ($data = json_decode($requested_data)) {
          $result['status'] = 'success';
          $result['result'] = $data;
        }
        else {
          $result['status'] = 'error';
          $result['result'] = $requested_data;
          $result['message'] = 'No valid JSON object can be retrieved';
        }
      }
      else {
        $result['status'] = 'critical';
        $result['message'] = 'No valid POST data can be constructed. Check post fields for proper syntax. No variables and values should be empty.';
      }
      break;
    default:
      $result['status'] = 'critical';
      $result['target'] = 'method_selection';
      $result['message'] = 'Unknown request method';
      break;
  }
  return $result;
}