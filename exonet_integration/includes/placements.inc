<?php

function ei_update_placement($placement_data, $remove_old_placements = TRUE) {
  module_load_include('inc', 'exo_integration', 'includes/product');

  $stock_code_is_set = isset($placement_data['stockcode']) && $placement_data['stockcode'];

  if ($stock_code_is_set) {
    $product = ei_check_product($placement_data['stockcode']);

    if (isset($product->field_categories)) {
      if ($remove_old_placements) {
        ei_delete_all_placement($placement_data['stockcode']);
      }

      $category_seqno_is_set = isset($placement_data['categoryseqno']) && $placement_data['categoryseqno'];
      if ($category_seqno_is_set) {
        module_load_include('inc', 'exo_integration', 'includes/taxonomy');

        $tid = ei_check_term($placement_data['categoryseqno'], 'categories', 'update placements');

        if ($tid) {
          // Need to check if this category has not been assigned to product yet to avoid duplicates
          $is_set_already = FALSE;

          if (isset($product->field_categories[LANGUAGE_NONE]) && $product->field_categories[LANGUAGE_NONE]) {
            foreach ($product->field_categories[LANGUAGE_NONE] as $cat) {
              if (isset($cat['tid']) && $cat['tid'] == $tid) {
                $is_set_already = TRUE;
                break;
              }
            }
          }

          if (!$is_set_already) {
            syslog(LOG_NOTICE, 'exo_integration: Created/updated placement: ' . $placement_data['stockcode'] . ' Seqno: ' . $placement_data['categoryseqno'] . ' Reference: sektor');
            $product->field_categories['und'][]['tid'] = $tid;

            if (!variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT)) {
              // Turn off automatic path generation
              $product->path['pathauto'] = FALSE;
              $product->pathauto_perform_alias = FALSE;
            }
            node_save($product);
          }
        }
        else {
          watchdog('exo_integration: Can\'t create/update placements', 'Category cannot be found <br/>Data: <pre>' . print_r($placement_data, 1) . '</pre>', NULL, WATCHDOG_WARNING);
        }
      }
      else {
        watchdog('exo_integration: Can\'t create/update placements', 'No stockcode or category seqno set up <br/>Data: <pre>' . print_r($placement_data, 1) . '</pre>', NULL, WATCHDOG_WARNING);
      }
    }
    else {
      if (is_object($product)) {
        watchdog('exo_integration: Can\'t create/update placements', 'Product cannot be found <br/>Data: <pre>' . print_r($placement_data, 1) . '</pre>', NULL, WATCHDOG_WARNING);
      }
      else {
        watchdog('exo_integration: Can\'t create/update placements', 'Multiple products found with the same stockcode <br/>Data: <pre>' . print_r($placement_data, 1) . '</pre>', NULL, WATCHDOG_WARNING);
      }
    }
  }
}


function ei_delete_placement($placement_data) {
  module_load_include('inc', 'exo_integration', 'includes/taxonomy');
  module_load_include('inc', 'exo_integration', 'includes/product');

  $stockcode_is_set = isset($placement_data['stockcode']) && $placement_data['stockcode'];
  $categoryseqno_is_set = isset($placement_data['categoryseqno']) && $placement_data['categoryseqno'];
  $categorytid_is_set = isset($placement_data['categorytid']) && $placement_data['categorytid'];

  if ($stockcode_is_set && ($categoryseqno_is_set || $categorytid_is_set)) {
    $tid = NULL;
    if ($categoryseqno_is_set) {
      $tid = ei_check_term($placement_data['categoryseqno'], 'categories', 'delete placements');
    }
    else {
      $tid = $placement_data['categorytid'];
    }

    $product = ei_check_product($placement_data['stockcode']);

    if ($tid && $product->nid) {
      $categories = isset($product->field_categories[LANGUAGE_NONE]) ? $product->field_categories[LANGUAGE_NONE] : array();
      if (!empty($categories)) {
        foreach ($categories as $index => $cat) {
          if (isset($cat['tid']) && $cat['tid'] == $tid) {
            $index_to_delete = $index;
            break;
          }
        }
      }

      // Might be = 0 because of index
      if (isset($index_to_delete)) {
        ei_remove_term($product->field_categories[LANGUAGE_NONE][$index_to_delete]['tid'], 'field_categories', $product->nid, FALSE);
        unset($product->field_categories[LANGUAGE_NONE][$index_to_delete]);

        if (empty($product->field_categories[LANGUAGE_NONE])) {
          $product->field_categories = array();
        }

        if (!variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT)) {
          // Turn off automatic path generation
          $product->path['pathauto'] = FALSE;
          $product->pathauto_perform_alias = FALSE;
        }
        node_save($product);
      }
    }
    else {
      if (!$tid) {
        watchdog('exo_integration: Can\'t delete placements', 'Category cannot be found <br/>Data: !data', array('!data' => '<pre>' . print_r($placement_data, 1) . '</pre>'), WATCHDOG_WARNING);
      }
      elseif (is_object($product)) {
        watchdog('exo_integration: Can\'t delete placements', 'Product cannot be found <br/>Data: !data', array('!data' => '<pre>' . print_r($placement_data, 1) . '</pre>'), WATCHDOG_WARNING);
      }
      else {
        watchdog('exo_integration: Can\'t delete placements', 'Multiple products found with the same stockcode <br/>Data: !data', array('!data' => '<pre>' . print_r($placement_data, 1) . '</pre>'), WATCHDOG_WARNING);
      }
    }
  }
  else {
    watchdog('exo_integration: Can\'t create/update placements', 'No stockcode or category seqno set up <br/>Data: !data', array('!data' => '<pre>' . print_r($placement_data, 1) . '</pre>'), WATCHDOG_WARNING);
  }
}


function ei_delete_all_placement($stockcode) {
  module_load_include('inc', 'exo_integration', 'includes/product');
  $product = ei_check_product($stockcode);

  if (isset($product->field_categories[LANGUAGE_NONE])) {
    if (!empty($product->field_categories[LANGUAGE_NONE])) {
      module_load_include('inc', 'exo_integration', 'includes/taxonomy');
      foreach ($product->field_categories[LANGUAGE_NONE] as $category) {
        ei_remove_term($category['tid'], 'field_categories', $product->nid, FALSE);
      }
    }

    $product->field_categories = NULL;

    if (!variable_get(EI_GENERATE_URL_ALIAS, EI_GENERATE_URL_ALIAS_DEFAULT)) {
      // Turn off automatic path generation
      $product->path['pathauto'] = FALSE;
      $product->pathauto_perform_alias = FALSE;
    }
    node_save($product);
  }
  else {
    if (is_object($product)) {
      if (!isset($product->nid)) {
        watchdog('exo_integration: Can\'t delete placements', 'Product cannot be found <br/>Stockcode: <pre>' . print_r($stockcode, 1) . '</pre>', NULL, WATCHDOG_WARNING);
      }
    }
    else {
      watchdog('exo_integration: Can\'t delete placements', 'Multiple products found with the same stockcode <br/>Stockcode: <pre>' . print_r($stockcode, 1) . '</pre>', NULL, WATCHDOG_WARNING);
    }
  }
}


function ei_remove_all_placements_for_product($product) {
  if (isset($product->field_stock_code[LANGUAGE_NONE][0]['value'])) {
    ei_delete_all_placement($product->field_stock_code[LANGUAGE_NONE][0]['value']);
  }
}