<?php

function ei_process_data($data_array) {
  module_load_include('inc', 'exo_integration', 'includes/mails');
  $process_id = uniqid();

  // My custom timestamp
  if (isset($data_array['timestamp'])) {
    $submitted = $data_array['timestamp'];
    unset($data_array['timestamp']);
  }

  _ei_log_process('start', array('id' => $process_id, 'data' => $data_array));

  if (!empty($data_array)) {
    /* Dave's config set up in .htaccess
      php_value default_socket_timeout 7200
      php_value max_execution_time 7200
      php_value max_input_time 7200
      php_value max_input_vars 100000
      php_value memory_limit 2G
      php_value post_max_size 100M
     */

    if (variable_get(EI_SEND_SYSTEM_NOTIFICATIONS, EI_SEND_SYSTEM_NOTIFICATIONS_DEFAULT)) {
      $n_params = array(
        'action' => 'Started',
        'type' => implode(', ', array_keys($data_array)),
        'process_id' => $process_id,
      );
      ei_send_email_notification('exo_notify_status', $n_params);
    }

    $parent_data = array();
    $related_products = array();
    foreach ($data_array as $primary_key => $subset) {
      try {
        // Just to save last request ... Just in case ...
        // I also track all requests with module functions (can view them here: /admin/exo_integration/history)
        $folder = 'exo_integration';
        $dir = 'public://' . $folder;
        $filename = 'request_' . date('YmdHis') . '_' . $primary_key . '.txt';

        if (file_prepare_directory($dir)) {
          $filepath = drupal_realpath($dir . '/' . $filename);
          file_put_contents($filepath, print_r($data_array, TRUE));
        }

        $link = l($filename, variable_get('file_public_path', conf_path() . '/files') . '/' . $folder . '/' . $filename, array('attributes' => array('target' => '_blank')));
        _ei_log_process('execute', array(
          'key' => $primary_key,
          'count' => count($subset),
          'file' => $link
        ));


        switch ($primary_key) {


          /*********  Categories **********/
          case 'categorytrans':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/taxonomy');
              $vocabulary = 'categories';

              foreach ($subset as $subset_data_index => $subset_data) {
                switch ($subset_data['action']) {
                  case 'replaced':
                  case 'updated':
                    //update category
                    if (isset($subset_data['category'])) {
                      $tmp = ei_update_category($subset_data['category'], $vocabulary);
                      if (isset($tmp)) {
                        $parent_data['categories'][] = $tmp;
                      }
                    }
                    else {
                      watchdog('exo_integration: No data passed for category', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'], NULL, WATCHDOG_WARNING);
                    }

                    if (isset($subset_data['placements']) && !empty($subset_data['placements'])) {
                      module_load_include('inc', 'exo_integration', 'includes/placements');
                      foreach ($subset_data['placements'] as $placement_index => $placement_data) {
                        ei_update_placement($placement_data, FALSE);
                      }
                      //                      ei_update_placement($placement_data, !(bool)$placement_index);
                      //                  }else{
                      //                    watchdog('exo_integration: No data passed for placements', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'], null, WATCHDOG_WARNING);
                    }
                    break;
                  case 'deleted':
                    //delete category
                    if (isset($subset_data['seqno'])) {
                      ei_delete_term($subset_data['seqno'], $vocabulary);
                    }
                    else {
                      watchdog('exo_integration: Delete', 'Can\'t delete category (no seqno set up): <pre>' . print_r($subset_data, 1) . '</pre>', NULL, WATCHDOG_NOTICE);
                    }
                    break;
                  default:
                    watchdog('exo_integration: Unknown action', 'Primary index: ' . $primary_key . '<br/>Action: ' . $subset_data['action'], NULL, WATCHDOG_WARNING);
                    break;
                }
              }
            }
            break;


          /*********  Products **********/
          case 'producttrans':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/product');

              foreach ($subset as $subset_data_index => $subset_data) {
                switch ($subset_data['action']) {
                  case 'replaced':
                  case 'updated':
                    syslog(LOG_DEBUG, 'Product upload: ' . $subset_data['stockcode'] . ' Reference: sektor_import');
                    // update/create product
                    if (isset($subset_data['product'])) {
                      $related = ei_update_product($subset_data['product']);
                      if (!empty($related)) {
                        $related_products[$subset_data['stockcode']] = $related;
                      }

                      if (isset($subset_data['placements']) && !empty($subset_data['placements'])) {
                        module_load_include('inc', 'exo_integration', 'includes/placements');
                        $delete_placements = TRUE;
                        foreach ($subset_data['placements'] as $placement_index => $placement_data) {
                          ei_update_placement($placement_data, $delete_placements);
                          $delete_placements = FALSE;
                          //                    }else{
                          //                      watchdog('exo_integration: No data passed for placements', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'], null, WATCHDOG_WARNING);
                        }
                      }
                    }
                    else {
                      watchdog('exo_integration: No data passed for product', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'] . ' <br/>Index: ' . $subset_data_index, NULL, WATCHDOG_WARNING);
                    }
                    break;
                  case 'deleted':
                    // delete product
                    if (isset($subset_data['stockcode'])) {
                      ei_delete_product($subset_data['stockcode']);
                    }
                    else {
                      watchdog('exo_integration: Delete', 'Can\'t delete product (no stockcode set up): <pre>' . print_r($subset_data, 1) . '</pre>', NULL, WATCHDOG_NOTICE);
                    }
                    break;
                  default:
                    watchdog('exo_integration: Unknown action', 'Primary index: ' . $primary_key . '<br/>Action: ' . $subset_data['action'], NULL, WATCHDOG_WARNING);
                    break;
                }
              }

              _ei_flush_image_styles('products');
            }
            break;


          /*********  Product URL **********/
          case 'producturls':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/product');

              foreach ($subset as $subset_data_index => $subset_data) {
                $stockcode_is_set = isset($subset_data['stockcode']) && !empty($subset_data['stockcode']);
                $weburldesc_is_set = isset($subset_data['weburldesc']) && !empty($subset_data['weburldesc']);
                if ($stockcode_is_set && $weburldesc_is_set) {
                  $product = ei_check_product($subset_data['stockcode']);

                  if (isset($product->nid) && $product->nid) {
                    ei_update_product_alias($product, $subset_data);
                  }
                  else {
                    watchdog('exo_integration: Product URL Update', 'Cannot find product with stockcode: @stockode <br/>Data: @data', array(
                      '@stockcode' => $subset_data['stockcode'],
                      '@data' => '<pre>' . print_r($subset_data, 1) . '</pre>'
                    ), WATCHDOG_WARNING);
                  }
                }
              }
            }
            break;


          /*********  Product images **********/
          case 'products':
            if (/*!empty($subset) && */
              isset($data_array['filename']) && !empty($data_array['filename'])
            ) {
              //            watchdog('exo_integration: debug', 'Data: !data', array('!data' => print_r($data_array,1)), WATCHDOG_DEBUG);
              //            module_load_include('inc', 'exo_integration', 'includes/product');
              //            ei_update_image_for_products($data_array['filename'], $subset);
              module_load_include('inc', 'exo_integration', 'includes/support_functions');
              _ei_update_file($data_array['filename'], 'product');
              _ei_flush_image_styles('products');
            }
            break;


          /*********  Categories to Products **********/
          case 'placementtrans':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/placements');

              foreach ($subset as $subset_data_index => $subset_data) {
                switch ($subset_data['action']) {
                  case 'replaced':
                  case 'updated':
                    // update/create placements
                    if (isset($subset_data['allplacements']) && !empty($subset_data['allplacements'])) {
                      foreach ($subset_data['allplacements'] as $placement_index => $placement_data) {
                        //                      ei_update_placement($placement_data, !(bool)$placement_index);
                        ei_update_placement($placement_data, FALSE);
                      }
                    }
                    else {
                      watchdog('exo_integration: No data passed for placements', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'] . ' <br/>Index: ' . $subset_data_index, NULL, WATCHDOG_WARNING);
                    }
                    break;
                  case 'deleted':
                    // delete placements

                    // Just leave it for later in case they will use multiple categories per product. Currently only one allowed.
                    //									if(isset($subset_data['placementtop'])){
                    //										ei_delete_placement($subset_data['placementtop']);
                    //									}else{
                    //										watchdog('exo_integration: Delete', 'Can\'t delete placement (no data set up): <pre>' . print_r($subset_data, 1) . '</pre>', null, WATCHDOG_NOTICE);
                    //									}

                    //									if(isset($subset_data['stockcode'])){
                    //										ei_delete_all_placement($subset_data['stockcode']);
                    //                  }else{
                    //										watchdog('exo_integration: Delete', 'Can\'t delete placements (no stockcode set up): <pre>' . print_r($subset_data, 1) . '</pre>', null, WATCHDOG_NOTICE);
                    //									}

                    if (isset($subset_data['stockcode'])) {
                      if (isset($subset_data['categoryseqno'])) {
                        ei_delete_placement($subset_data);
                      }
                      else {
                        watchdog('exo_integration: Delete', 'Can\'t delete placements (no categoryseqno set up): <pre>' . print_r($subset_data, 1) . '</pre>', NULL, WATCHDOG_NOTICE);
                      }
                    }
                    else {
                      watchdog('exo_integration: Delete', 'Can\'t delete placements (no stockcode set up): <pre>' . print_r($subset_data, 1) . '</pre>', NULL, WATCHDOG_NOTICE);
                    }
                    break;
                  default:
                    watchdog('exo_integration: Unknown action', 'Primary index: ' . $primary_key . '<br/>Action: ' . $subset_data['action'], NULL, WATCHDOG_WARNING);
                    break;
                }
              }
            }
            break;


          /*********  Logins **********/
          case 'logintrans':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/logins');

              foreach ($subset as $subset_data_index => $subset_data) {
                switch ($subset_data['action']) {
                  case 'replaced':
                  case 'updated':
                    // update/create logins
                    if (isset($subset_data['login'])) {
                      ei_update_logins($subset_data['login']);
                    }
                    else {
                      watchdog('exo_integration: No data passed for logins', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'] . ' <br/>Index: ' . $subset_data_index, NULL, WATCHDOG_WARNING);
                    }
                    break;
                  case 'deleted':
                    // delete logins
                    if (isset($subset_data['uid'])) {
                      ei_delete_login($subset_data['uid']);
                    }
                    else {
                      watchdog('exo_integration: Delete', 'Can\'t delete login (no uid set up): <pre>' . print_r($subset_data, 1) . '</pre>', NULL, WATCHDOG_NOTICE);
                    }
                    break;
                  default:
                    watchdog('exo_integration: Unknown action', 'Primary index: ' . $primary_key . '<br/>Action: ' . $subset_data['action'], NULL, WATCHDOG_WARNING);
                    break;
                }
              }
            }
            break;


          /*********  Brands **********/
          case 'brandtrans':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/brands');

              foreach ($subset as $subset_data_index => $subset_data) {
                switch ($subset_data['action']) {
                  case 'replaced':
                  case 'updated':
                    // update/create brands
                    if (isset($subset_data['brand'])) {
                      ei_update_brand($subset_data['brand']);
                    }
                    else {
                      watchdog('exo_integration: No data passed for brands', 'Primary index: ' . $primary_key . ' <br/>Action: ' . $subset_data['action'] . ' <br/>Index: ' . $subset_data_index, NULL, WATCHDOG_WARNING);
                    }
                    break;
                  case 'deleted':
                    // delete brands
                    if (isset($subset_data['seqno'])) {
                      ei_delete_brand($subset_data['seqno']);
                    }
                    else {
                      watchdog('exo_integration: Delete', 'Can\'t delete brand (no seqno set up): <pre>' . print_r($subset_data, 1) . '</pre>', NULL, WATCHDOG_NOTICE);
                    }
                    break;
                  default:
                    watchdog('exo_integration: Unknown action', 'Primary index: ' . $primary_key . '<br/>Action: ' . $subset_data['action'], NULL, WATCHDOG_WARNING);
                    break;
                }
              }

              _ei_flush_image_styles('brands');
            }
            break;


          /*********  Brands images **********/
          case 'brands':
            if (/*!empty($subset) && */
              isset($data_array['filename']) && !empty($data_array['filename'])
            ) {
              //            watchdog('exo_integration: debug', 'Data: !data', array('!data' => print_r($data_array,1)), WATCHDOG_DEBUG);
              //            module_load_include('inc', 'exo_integration', 'includes/product');
              //            ei_update_image_for_products($data_array['filename'], $subset);
              module_load_include('inc', 'exo_integration', 'includes/support_functions');
              _ei_update_file($data_array['filename'], 'brand');

              _ei_flush_image_styles('brands');
            }
            break;


          /*********  RMA Status Change Notifications **********/
          case 'rmas':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/mails');

              foreach ($subset as $subset_data_index => $subset_data) {
                if (isset($subset_data['new']['status']) && !empty($subset_data['new']['status'])) {
                  $accepted_statuses = array(
                    //                                            'submitted',
                    'item received',
                    'quote',
                    'waiting on parts',
                    'work completed',
                    //                                            'waiting on tech to be assigned',
                    //                                            'in progress',
                  );

                  $accepted = in_array(strtolower($subset_data['new']['status']), $accepted_statuses) ? TRUE : FALSE;

                  if ($accepted/* && strtolower($subset_data['new']['source']) != 'exo'*/) {
                    if (isset($subset_data['old']) && !empty($subset_data['old'])) {
                      $subset_data['new']['old'] = $subset_data['old'];
                    }
                    ei_send_email_notification('rma_notification_changed', $subset_data['new']);
                    ei_send_email_notification('rma_notification_changed_client', $subset_data['new']);
                  }
                }
              }
            }
            break;


          /*********  PE Status Change Notifications **********/
          case 'pes':
            if (!empty($subset)) {
              module_load_include('inc', 'exo_integration', 'includes/mails');

              foreach ($subset as $subset_data_index => $subset_data) {
                if (isset($subset_data['new']['status']) && !empty($subset_data['new']['status'])) {
                  $accepted_statuses = array(
                    'web' => array(
                      'pe_notification_changed' => array(
                        'received',
                      ),
                      'pe_notification_changed_client' => array(
                        'received',
                        'submitted pending approval',
                        'approved',
                        'rejected',
                        'expired',
                      ),
                    ),
                    'exo' => array(
                      'pe_notification_changed_client' => array(
                        'approved',
                        'rejected',
                        'expired',
                      ),
                    ),
                  );

                  foreach ($accepted_statuses as $source => $type_of_notification) {
                    if (strtolower($subset_data['new']['source']) == $source) {
                      foreach ($type_of_notification as $type => $statuses) {
                        foreach ($statuses as $status) {
                          if (strtolower($subset_data['new']['status']) == strtolower($status)) {
                            if (isset($subset_data['old']) && !empty($subset_data['old']) && !isset($subset_data['new']['old'])) {
                              $subset_data['new']['old'] = $subset_data['old'];
                            }
                            ei_send_email_notification($type, $subset_data['new']);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            break;


          case 'filename':
            // Just to remove extra error notifications from watchdog
            break;

          default:
            watchdog('exo_integration: Unknown primary index', 'Primary index: ' . $primary_key . ' Data: <pre>' . print_r($subset, 1) . '</pre>', NULL, WATCHDOG_WARNING);
            break;
        }
      } catch (Exception $e) {
        watchdog('exo_integration: Data Processing', 'Error occurred: !error <br/>Processing: !process <br/>Data: !data', array(
          '!error' => $e->getMessage(),
          '!process' => $primary_key,
          '!data' => '<pre>' . print_r($subset, 1) . '</pre>'
        ), WATCHDOG_CRITICAL);
      }
    }

    try {
      // Nest categories
      if (!empty($parent_data)) {
        foreach ($parent_data as $vacabulary => $term_parent) {
          foreach ($term_parent as $term_data) {
            if (isset($term_data['children_id']) && isset($term_data['parent_id'])) {
              ei_update_term_parents($term_data['children_id'], $term_data['parent_id'], $vacabulary);
            }
          }
        }
      }

      // Assign related products
      if (!empty($related_products)) {
        foreach ($related_products as $parent => $related_items) {
          ei_update_related_products($parent, $related_items);
        }
      }
      //      watchdog('exo_integration: debug', 'Related products: <pre>' . print_r($related_products, 1) . '</pre>', null, WATCHDOG_WARNING);

      // Clear menu cache
      module_load_include('module', 'custom_menu', 'custom_menu');
      cm_clear_cache('main-menu');

      if (variable_get(EI_SEND_SYSTEM_NOTIFICATIONS, EI_SEND_SYSTEM_NOTIFICATIONS_DEFAULT)) {
        $n_params = array(
          'action' => 'Finished',
          'type' => implode(', ', array_keys($data_array)),
          'process_id' => $process_id,
        );
        ei_send_email_notification('exo_notify_status', $n_params);
      }
    } catch (Exception $e) {
      watchdog('exo_integration: Data Processing', 'Error occurred: !error <br/>Data: !data', array(
        '!error' => $e->getMessage(),
        '!data' => '<pre>' . print_r($data_array, 1) . '</pre>'
      ), WATCHDOG_CRITICAL);
    }
  }

  _ei_log_process('finish', array('id' => $process_id, 'data' => $data_array));
}


/**
 * Logs process in the db based on type and data passed
 */
function _ei_log_process($type, $data) {
  switch ($type) {
    case 'start':
      $params = array(
        '!time' => date('d/m/Y H:i:s'),
        '!id' => $data['id'],
        '!processes' => implode(', ', array_keys($data['data'])),
      );
      watchdog('exo_integration: Timings', 'Start <br/>!time <br/>Id: <br/>!id <br/>Processes: <br/>!processes', $params, WATCHDOG_INFO);
      break;

    case 'finish':
      $params = array(
        '!time' => date('d/m/Y H:i:s'),
        '!id' => $data['id'],
        '!processes' => implode(', ', array_keys($data['data'])),
      );
      watchdog('exo_integration: Timings', 'Finished <br/>!time <br/>Id: <br/>!id <br/>Processes: <br/>!processes', $params, WATCHDOG_INFO);
      break;

    case 'execute':
      $params = array(
        '!key' => $data['key'],
        '!count' => $data['count'],
        '!file' => $data['file'],
      );
      watchdog('exo_integration: Timings ', 'Executing <br/>!key <br/>Entries count: <br/>!count <br/>File reference: <br/>!file', $params, WATCHDOG_INFO);
      break;
  }
}


function _ei_flush_image_styles($type) {
  switch ($type) {
    case 'brands':
      /*   image_style_flush('brandlogo-tall');
         image_style_flush('brandlogo-wide');
         image_style_flush('brand_logo');
         image_style_flush('brand_logo_description_block');
         image_style_flush('brand_logo_featured_block');
         image_style_flush('brand_logo_gallery');
         image_style_flush('brand_logo_product_page');*/
      break;

    case 'products':
      /*   image_style_flush('product-image');
         image_style_flush('product-image-slider');
         image_style_flush('product_featured_block');
         image_style_flush('product_image_in_cart');
         image_style_flush('product_image_in_pe');
         image_style_flush('product_list_image_thumbnail');
         image_style_flush('product_page_large_view');*/
      break;
  }
}